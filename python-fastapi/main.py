from fastapi import FastAPI
import asyncio
import aiomysql
from database import engine, SessionLocal, Base, metadata_obj, champion_rate
from sqlalchemy import select

app = FastAPI()

loop = asyncio.get_event_loop()


@app.get("/champ-rate/{champ_id}/{lane}")
async def root(champ_id: int, lane):




    pool = await aiomysql.create_pool(host='mysql', port=3306, user='root', password='qwer1234', db='match_info', loop=loop)

    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            # valid champ id check
            view_query = 'SELECT DISTINCT id FROM count_info'
            await cur.execute(view_query)
            sql_result = await cur.fetchall()
            champ_id_list = []
            for champ_id_tuple in sql_result:
                champ_id_list.append(champ_id_tuple[0])


            if champ_id not in champ_id_list:
                return {"Not a valid champion ID.": 1, "champ_id":champ_id, "len": len(champ_id_list)}

            # valid lane check
            lanes = ['TOP', 'MIDDLE', 'JUNGLE', 'SUPPORT', 'ADCARRY']
            if lane not in lanes:
                return "Not a valid lane."


            view_query = 'SELECT * FROM champion_rate WHERE (id=%s AND lane=%s);'
            await cur.execute(view_query, (champ_id, lane))
            description = cur.description
            field_name = [i[0] for i in description]

            sql_result = await cur.fetchall()
            ret = {}
            for i in range(len(field_name)):
                ret[field_name[i]] = str(sql_result[0][i])

    pool.close()
    return ret


import numpy as np
import random

# item == document['match_timeline']


def lane_predictor_wk(item):
    participant_ids = list(range(10))
    lanes = [None, None, None, None, None, None, None, None, None, None]
    teams = [None, None, None, None, None, None, None, None, None, None]
    # step 1. predict jungles  by jungle minions kiiled logs
    jungle_kills_blue = []
    jungle_kills_red = []

    for pid in participant_ids:
        logs = []
        for i in range(5):
            logs.append(item['frames'][i]['participantFrames']
                        [str(pid+1)]['jungleMinionsKilled'])
        kills = np.mean(logs[:5])
        if pid < 5:
            jungle_kills_blue.append((kills, pid))
        else:
            jungle_kills_red.append((kills, pid))

    jungle_kills_blue = sorted(jungle_kills_blue, reverse=True)
    lanes[jungle_kills_blue[0][1]] = 'JUNGLE'

    jungle_kills_red = sorted(jungle_kills_red, reverse=True)
    lanes[jungle_kills_red[0][1]] = 'JUNGLE'

    # step 2. predict teams and lanes by position logs
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] is None, zip(participant_ids, lanes)):
        # logs = item['position_logs'][str(pid+1)]
        logs = []
        for i in range(2, 9):
            v = item['frames'][i]['participantFrames'][str(pid+1)]
            if 'position' in v:
                position = (v['position']['x'], v['position']['y'])
            else:
                position = (-1, -1)
            logs.append(position)
        avg_position = np.mean(logs[:7], axis=0)
        if pid < 5:
            x = avg_position[0]
            blues.append((x, pid))
        else:
            x = avg_position[0]
            reds.append((x, pid))

    s = sorted(blues)
    # print(s)
    lanes[s[0][1]] = 'TOP'
    lanes[s[1][1]] = 'MIDDLE'
    lanes[s[2][1]] = 'BOTTOM'
    lanes[s[3][1]] = 'BOTTOM'

    s = sorted(reds)
    # print(s)
    lanes[s[0][1]] = 'TOP'
    lanes[s[1][1]] = 'MIDDLE'
    lanes[s[2][1]] = 'BOTTOM'
    lanes[s[3][1]] = 'BOTTOM'

    # step 3. discriminate adcarry and support
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] == 'BOTTOM', zip(participant_ids, lanes)):
        logs = []
        length = 30 if len(item['frames']) >= 30 else len(item['frames'])
        for i in range(length):
            logs.append(item['frames'][i]['participantFrames']
                        [str(pid+1)]['minionsKilled'])
        kills = np.mean(logs[:30])
        if pid < 5:
            blues.append((kills, pid))
        else:
            reds.append((kills, pid))

    s = sorted(blues)
    lanes[s[0][1]] = 'SUPPORT'
    lanes[s[1][1]] = 'ADCARRY'

    s = sorted(reds)
    lanes[s[0][1]] = 'SUPPORT'
    lanes[s[1][1]] = 'ADCARRY'

    lane_dict = dict()
    for pid, lane in zip(participant_ids, lanes):
        lane_dict[str(pid+1)] = lane

    return lane_dict

def lane_predictor_harry(item):
    participant_ids = list(range(10))
    lanes_given = [None, None, None, None, None, None, None, None, None, None]
    lanes_predicted = [None, None, None, None,
                       None, None, None, None, None, None]
    lanes = [None, None, None, None, None, None, None, None, None, None]
    lanes_ignore = [None, None, None, None, None, None, None, None, None, None]
    # step 1. check the given lines
    given_blue = []
    given_red = []
    for pid in participant_ids:

        logs = []
        for i in range(0, 14):
            v = item['frames'][i]['participantFrames'][str(pid+1)]
            if 'position' in v:
                position = (v['position']['x'], v['position']['y'])
            else:
                position = (-1, -1)
            logs.append(position)
        given = np.sum(logs[0])
        if pid < 5:
            given_blue.append((given, pid))
        else:
            given_red.append((given, pid))

    s = sorted(given_blue)
    # print(s)
    lanes_given[s[0][1]] = 'MIDDLE'
    lanes_given[s[1][1]] = 'ADCARRY'
    lanes_given[s[2][1]] = 'JUNGLE'
    lanes_given[s[3][1]] = 'SUPPORT'
    lanes_given[s[4][1]] = 'TOP'

    s = sorted(given_red)
    # print(s)
    lanes_given[s[0][1]] = 'MIDDLE'
    lanes_given[s[1][1]] = 'ADCARRY'
    lanes_given[s[2][1]] = 'JUNGLE'
    lanes_given[s[3][1]] = 'SUPPORT'
    lanes_given[s[4][1]] = 'TOP'

    # step 2. predict jungles  by jungle minions kiiled logs (rigid)
    jungle_kills_blue = []
    jungle_kills_red = []
    for pid in participant_ids:
        logs = []
        for i in range(5):
            logs.append(item['frames'][i]['participantFrames']
                        [str(pid+1)]['jungleMinionsKilled'])
        kills = np.mean(logs[:5])
        if pid < 5:
            jungle_kills_blue.append((kills, pid))
        else:
            jungle_kills_red.append((kills, pid))

    jungle_kills_blue = sorted(jungle_kills_blue, reverse=True)
    lanes_predicted[jungle_kills_blue[0][1]] = 'JUNGLE'

    jungle_kills_red = sorted(jungle_kills_red, reverse=True)
    lanes_predicted[jungle_kills_red[0][1]] = 'JUNGLE'

    # step 3. predict teams and lanes by position logs (modified to an axis)
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] is None, zip(participant_ids, lanes_predicted)):
        logs = []
        for i in range(0, 5):
            v = item['frames'][i]['participantFrames'][str(pid+1)]
            if 'position' in v:
                position = (v['position']['x'], v['position']['y'])
            else:
                position = (-1, -1)
            logs.append(position)
        avg_position = np.mean(logs[2:5], axis=0)
        if pid < 5:
            x = avg_position[1]/avg_position[0]
            blues.append((x, pid))
        else:
            x = (15000-avg_position[0])/(15000-avg_position[1])
            reds.append((x, pid))

    s = sorted(blues, reverse=True)
    # print(s)
    if s[0][0] > 1:
        lanes_predicted[s[0][1]] = 'TOP'
    if (s[1][0] > 0.3) and (s[1][0] < 4):
        lanes_predicted[s[1][1]] = 'MIDDLE'
    if (s[2][0] < 1) and (s[2][0] > 0):
        lanes_predicted[s[2][1]] = 'BOTTOM'
    if (s[3][0] < 1) and (s[3][0] > 0):
        lanes_predicted[s[3][1]] = 'BOTTOM'

    s = sorted(reds, reverse=True)
    # print(s)
    if s[0][0] > 1:
        lanes_predicted[s[0][1]] = 'TOP'
    if (s[1][0] > 0.3) and (s[1][0] < 4):
        lanes_predicted[s[1][1]] = 'MIDDLE'
    if (s[2][0] < 1) and (s[2][0] > 0):
        lanes_predicted[s[2][1]] = 'BOTTOM'
    if (s[3][0] < 1) and (s[3][0] > 0):
        lanes_predicted[s[3][1]] = 'BOTTOM'

    # step 4. discriminate adcarry and support (rigid)
    bottom_blues = []
    bottom_reds = []
    for pid, _ in filter(lambda x: x[1] == 'BOTTOM', zip(participant_ids, lanes_predicted)):
        logs = []
        length = 30 if len(item['frames']) >= 30 else len(item['frames'])
        for i in range(length):
            logs.append(item['frames'][i]['participantFrames']
                        [str(pid+1)]['minionsKilled'])
        kills = np.mean(logs[:30])
        if pid < 5:
            bottom_blues.append((kills, pid))
        else:
            bottom_reds.append((kills, pid))

    s = sorted(bottom_blues)
    if len(s) == 2:
        lanes_predicted[s[0][1]] = 'SUPPORT'
        lanes_predicted[s[1][1]] = 'ADCARRY'
    else:
        []
    s = sorted(bottom_reds)
    if len(s) == 2:
        lanes_predicted[s[0][1]] = 'SUPPORT'
        lanes_predicted[s[1][1]] = 'ADCARRY'
    else:
        []

    # step 5. compare the given and predicted lanes
    for pid in participant_ids:
        if lanes_predicted[pid] == lanes_given[pid]:
            lanes[pid] = lanes_predicted[pid]
        elif lanes_predicted[pid] == None or lanes_predicted[pid] == 'BOTTOM':
            lanes[pid] = lanes_given[pid]+'*'
        else:
            lanes[pid] = lanes_predicted[pid]

    # ignore troll games
#    for pid in participant_ids:
#        if lanes_predicted[pid]==lanes_given[pid]:
#            lanes[pid]=lanes_predicted[pid]
#        elif lanes_predicted[pid]==None or lanes_predicted[pid]=='BOTTOM':
#            lanes=lanes_ignore
#            break
#        else:
#            lanes[pid]=lanes_predicted[pid]

    lane_dict = dict()
    for pid, lane in zip(participant_ids, lanes):
        lane_dict[str(pid+1)] = lane

    return lane_dict


def predict_lane(item):
    if len(item['frames']) <= 16:
        return None
    lane_pred1 = lane_predictor_wk(item)
    lane_pred2 = lane_predictor_harry(item)
    if lane_pred1 == lane_pred2:
        return lane_pred1
    else:
        return None
def sort_key(event):
    return event['timestamp']


import motor.motor_asyncio
client = motor.motor_asyncio.AsyncIOMotorClient('mongodb', 27017)

@app.get('/match-by-id/{match_id}')
async def show_match_info(match_id):
    db = client.match_info_db
    match_info = db.lane_predict_conform
    ref_docu = await match_info.find_one({'gameId': match_id})
    if ref_docu is None:
        return 'Not a valid match_id'

    lane_dict = predict_lane(ref_docu['match_timeline'])

    docu = {}

    docu["participants"] = []
    docu["teams"] = []
#     docu['game_duration'] = i['info']['gameDuration']
    for frame in ref_docu["match_timeline"]['frames']:
        for event in frame['events']:
            if event['type'] == 'GAME_END':
                docu['game_duration'] = event['timestamp']
    docu['game_id'] = ref_docu['gameId']
    docu['frame_interval'] = ref_docu['match_timeline']['frameInterval']
    match_data = ref_docu['info']
    for player in match_data['participants']:
        temp_docu = {}
        temp_docu['control_wards'] = player['visionWardsBoughtInGame']
        temp_docu['wards_placed'] = player['wardsPlaced']
        temp_docu['wards_killed'] = player['wardsKilled']

        temp_docu['total_damage_dealt_to_champions'] = player['totalDamageDealtToChampions']
        temp_docu['total_damage_dealt'] = player['totalDamageDealt']

        temp_docu['kills'] = player['kills']
        temp_docu['deaths'] = player['deaths']
        temp_docu['assists'] = player['assists']

        temp_docu['summoner_name'] = player['summonerName']
        temp_docu['champion_level'] = player['champLevel']
        temp_docu['champion_id'] = player['championId']
        temp_docu['champion_name'] = player['championName']
        temp_docu['participant_id'] = player['participantId']
        temp_docu['lane'] = lane_dict[str(temp_docu['participant_id'])]

        cs = player['totalMinionsKilled'] + player['neutralMinionsKilled']
        cs_per_min = cs / (docu['game_duration'] / 60000)
        temp_docu['cs'] = cs
        temp_docu['cs_per_min'] = cs_per_min

        temp_docu['itme0'] = player['item0']
        temp_docu['itme1'] = player['item1']
        temp_docu['itme2'] = player['item2']
        temp_docu['itme3'] = player['item3']
        temp_docu['itme4'] = player['item4']
        temp_docu['itme5'] = player['item5']
        temp_docu['itme6'] = player['item6']

        temp_docu['team_id'] = player['teamId']
        temp_docu['win'] = player['win']

        temp_docu['gold_earned'] = player['goldEarned']

        docu['participants'].append(temp_docu)
    for team in match_data['teams']:
        temp_docu = {}
        temp_docu['team_id'] = team['teamId']
        temp_docu['baron_kill'] = team['objectives']['baron']['kills']
        temp_docu['dragon_kill'] = team['objectives']['dragon']['kills']
        temp_docu['tower_kill'] = team['objectives']['tower']['kills']

        gold_sum, kill_sum = 0, 0
        if team['teamId'] == 100:
            for idx in range(0,5):
                gold_sum += docu['participants'][idx]['gold_earned']
                kill_sum += docu['participants'][idx]['kills']
        else:
            for idx in range(5,10):
                gold_sum += docu['participants'][idx]['gold_earned']
                kill_sum += docu['participants'][idx]['kills']

        temp_docu['total_gold'] = gold_sum
        temp_docu['total_gold'] = kill_sum

        docu['teams'].append(temp_docu)



    docu['blue_gold_minus_red_gold'] = []
#     print(i['match_timeline'])
    for frame in ref_docu['match_timeline']['frames']:
        dict_to_append = {}
        dict_to_append['timestamp'] = frame['timestamp']
        blue_gold = 0
        red_gold = 0
        for pid, data in frame['participantFrames'].items():
            if int(pid) <= 5:
                blue_gold += data['currentGold']
            else:
                red_gold += data['currentGold']
        dict_to_append['difference'] = blue_gold - red_gold
        docu['blue_gold_minus_red_gold'].append(dict_to_append)


    valid_events = ['WARD_KILLED', 'WARD_PLACED', 'BUILDING_KILL', 'ELITE_MONSTER_KILL', 'CHAMPION_KILL']
    events = []
    for frame in ref_docu['match_timeline']['frames']:
        for event in frame['events']:
            if event['type'] in valid_events:
                events.append(event)
    events.sort(key=sort_key)

    first_blood_flag, first_tower_flag = False, False
    for event in events:
        if event['type'] == 'CHAMPION_KILL':
            if first_blood_flag == False:
                first_blood_flag = True
                event['kill_first_blood'] = True
            else:
                event['kill_first_blood'] = False
        elif event['type'] == 'BUILDING_KILL':
            if first_tower_flag == False:
                first_tower_flag = True
                event['first_tower_destroyed'] = True
            else:
                event['first_tower_destroyed'] = False
    docu['events'] = events
    return docu
