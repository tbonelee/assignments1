## Description

지역과 소환사명을 입력하면 해당 소환사의 최근 20경기 gameId를 출력하는 스크립트입니다.

## Prerequisite

실행을 위해서는 python3와 [Riot-Watcher 라이브러리](https://github.com/pseudonym117/Riot-Watcher)를 설치해야 합니다.

## Usage

라이브러리를 설치하였다면 `run.py`를 수정.

```python
lol_watcher = Lolwatcher('API key 입력')

my_region = '지역명 입력'

summonerName = '소환사명 입력'
```

`winning_percent.py`에서도 API key 수정

```python
lol_watcher = Lolwatcher('API key 입력')
```

설정 완료 후 다음 커맨드를 사용하여 실행:

```bash
python3 run.py
```
