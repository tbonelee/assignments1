import json



with open("./champion.json", 'r') as json_file:
	python_obj = json.load(json_file)


# 스트링 타입의 championName을 key로 정수형의 championId를 value로 하는 딕셔너리 
champNameToId = {}
for name, object in python_obj["data"].items():
    	champNameToId[name] = int(object["key"])

# 정수형의 championId를 key로 스트링 타입의 championName을 value로 하는 딕셔너리 
champIdToName = {}
for name, id in champNameToId.items():
	champIdToName[id] = name

if __name__ == "__main__":
	print("champNameToId =")
	print(champNameToId)
	print("champIdToName =")
	print(champIdToName)
