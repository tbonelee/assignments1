from riotwatcher.Handlers.ThrowOnErrorHandler import ApiError
import champion_id
from riotwatcher import LolWatcher

class WinningPercent:
	"""소환사의 챔피언별 승리 횟수와 총 승리 횟수를 기록하는 오브젝트
	"""

	def __init__(self):
		self.numOfGames = 0
		self.numOfTotalWins = 0
		self.numOfGamesByChampionId = {}
		self.numOfWinsByChampionId = {}

	def recordToWin(self, championId):
		self.numOfGames = self.numOfGames + 1
		self.numOfTotalWins = self.numOfTotalWins + 1
		if championId in self.numOfWinsByChampionId:
			self.numOfWinsByChampionId[championId] = self.numOfWinsByChampionId[championId] + 1
		else:
			self.numOfWinsByChampionId[championId] = 1
		if championId in self.numOfGamesByChampionId:
			self.numOfGamesByChampionId[championId] = self.numOfGamesByChampionId[championId] + 1
		else:
			self.numOfGamesByChampionId[championId] = 1

	def recordToLose(self, championId):
		self.numOfGames = self.numOfGames + 1
		if championId in self.numOfGamesByChampionId:
			self.numOfGamesByChampionId[championId] = self.numOfGamesByChampionId[championId] + 1
		else:
			self.numOfGamesByChampionId[championId] = 1

	def getTotalWinPercent(self):
		return self.numOfTotalWins / self.numOfGames

	def getWinPercentByChampionId(self, championId: int):
		# print("here")
		if championId in self.numOfGamesByChampionId:
			if championId in self.numOfWinsByChampionId:
				return self.numOfWinsByChampionId[championId] / self.numOfGamesByChampionId[championId]
			else:
				return 0
		else:
			return -1
	def getWinPercentByChampionName(self, championName: str):
		return champion_id.champNameToId[championName]

lol_watcher = LolWatcher('RGAPI-2b9ed84f-f085-46f1-818b-0b3e20d8c4ba')



def trueMeansWin(gameId: int, championId: int, my_region: str):
	while True:
		try:
			data = lol_watcher.match_v4.by_id(my_region, gameId)
		except ApiError as e:
			if e.response.status_code == 504:
				print(e)
				print(" But we'll try agian..")
			else:
				raise e
		else:
			break
	temp = data['participants']
	isInTeam = False
	for _ in range(len(temp)):
		if temp[_]['championId'] == championId:
			win = temp[_]['stats']['win']
			isInTeam = True
			break
	if isInTeam == False:
		raise Exception("Player not in match")
	return win


if __name__ == "__main__":
	print(trueMeansWin(5428590663, 34, 'kr'))
