from re import match
from riotwatcher import LolWatcher, ApiError
import json
import champion_id
import winning_percent

# API 키를 넣어서 객체 생성
lol_watcher = LolWatcher('RGAPI-2b9ed84f-f085-46f1-818b-0b3e20d8c4ba')


# 지역 입력 받기
# cf) 소환사를 찾을 지역
# 	['br1', 'eun1', 'euw1', 'jp1', 'kr', 'la1', 'la2', 'na1', 'oc1', 'ru', 'tr1']
my_region = 'kr'

# 소환사명
summonerName = 'Hide on bush'

# 소환사 정보는 user에 딕셔너리로 반환
try:
	user = lol_watcher.summoner.by_name(my_region, summonerName)
except ApiError as e:
	if e.response.status_code == 429:
		print('We should retry in {} seconds.'.format(e.headers['Retry-After']))
		print('this retry-after is handled by default by the RiotWatcher library')
		print('future requests wait until the retry-after time passes')
		exit()
	elif e.response.status_code == 404:
		print("Summoner name not found")
		exit()
	else:
		print("")
		raise

# 해당 소환사의 가장 최근 경기를 최대 1000 개까지 matchlist에 받아온다
gameIdWithChamp = {}
print("=" * 10 + "gameId load start" + "=" * 10)
for i in range (0, 10):
	matchList = lol_watcher.match_v4.matchlist_by_account(my_region, user['accountId'], begin_index=(i*100)+0, end_index=(i*100)+100)
	for elem in matchList['matches']:
		# gameIdList.append(elem['gameId'])
		gameIdWithChamp[elem['gameId']] = elem['champion']
print("=" * 10 + "gameId load finished" + "=" * 10)

# 챔피언별 승패와 경기수, 총 승패와 경기수 담는 객체 생성
Hide_on_bush = winning_percent.WinningPercent()

print("=" * 10 + "checking winning records start" + "=" * 10)
for gameId, championId in gameIdWithChamp.items():
	if winning_percent.trueMeansWin(gameId, championId, my_region):
		Hide_on_bush.recordToWin(championId)
	else:
		Hide_on_bush.recordToLose(championId)
print("=" * 10 + "checking winning records finished" + "=" * 10)

# 딕셔너리로 기록
print("=" * 10 + "making json file start" + "=" * 10)
results_obj = {}
results_obj["total"] = {"winningPercentage":Hide_on_bush.getTotalWinPercent(), "numberOfGames":Hide_on_bush.numOfGames}
results_obj["champions"] = {}
for championId in Hide_on_bush.numOfGamesByChampionId:
	championName = champion_id.champIdToName[championId]
	results_obj["champions"][championName] = {"id":championId, "winningPercentage":Hide_on_bush.getWinPercentByChampionId(championId), "numberOfGames":Hide_on_bush.numOfGamesByChampionId[championId]}

# 파일로 출력
results_json = open("Results.json", "w")
print(results_obj)
json.dump(results_obj, results_json)
results_json.close()
print("=" * 10 + "making json file finished" + "=" * 10)
