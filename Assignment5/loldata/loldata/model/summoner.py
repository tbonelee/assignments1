import logging
import logging.config
from typing import Match
import numpy as np
from statistics import mean
from collections import defaultdict
import time
from ..error import NotSpecifiedSummonerError, NoSummonerError
from ..utils import convert_to_dict_mongodb
from ..config import LogConfig
from ..lol.tier import Tier
from ..lol.mmr import MMR
from ..lol.champion import Champion
from ..db_handler.mongodb_handler import MongoDbHandler
from ..lol.version import Version
from ..algorithm.lane_predictor import predict_lane
from .match_feature_extractor import MatchFeatureExtractor
from .match_summary_feature_extractor import MatchSummaryFeatureExtractor

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("model.summoner")
version = Version()
champion = Champion()
mmr = MMR()
match_feature_extractor = MatchFeatureExtractor()
match_summary_feature_extractor = MatchSummaryFeatureExtractor()

SEASON = '11'
SEASON_ID = 14
season_dict = {13: '10',
               14: '11'}


def get_summoner_id_list(tier_set=None, shuffle=True, limit=None):
    mds = MongoDbHandler('riot', 'summoner_summary')
    summoner_id_list = []
    condition = {"$or": [{"tier": tier, "rank": rank} for tier, rank in tier_set]}
    items = mds.find_item(condition, projection={'summoner_id': 1, 'tier': 1, 'rank': 1})
    if limit is not None:
        items = items.limit(limit)
    for i, item in enumerate(items):
        key = (item['tier'], item['rank'])
        if key in tier_set:
            summoner_id_list.append(item['summoner_id'])
        if i % 1000 == 0:
            logger.info(f'{i}-th summoner is filtered.')
    if shuffle:
        np.random.shuffle(summoner_id_list)
    return summoner_id_list


class Summoner(object):

    def __init__(self, summoner_id=None, summoner_name=None,
                 mds=None, mdg=None, mongodb_dict=None):
        self.my_region = 'kr'
        self.mds = mds
        if mds is None:
            self.mds = MongoDbHandler('riot', 'summoner_summary')
        self.mdg = mdg
        if mdg is None:
            self.mdg = MongoDbHandler('riot', 'game_summary')
        self.mongodb_dict = mongodb_dict
        if mongodb_dict is None:
            self.mongodb_dict = version.get_mongodb_dict()

        if summoner_id is None and summoner_name is None:
            raise NotSpecifiedSummonerError('please give a summoner_id or a summoner_name.')
        if summoner_name is not None:
            summoner_info = self.mds.find_one_item({'summoner_name': summoner_name})
        if summoner_id is not None:
            summoner_info = self.mds.find_one_item({'summoner_id': summoner_id})
        if summoner_info is None:
            raise NoSummonerError('No summoners in our database.')

        self.summoner_info = summoner_info
        self.summoner_id = summoner_info['summoner_id']
        self.summoner_name = summoner_info['summoner_name']
        self.account_id = summoner_info['account_id']
        self.update_additional_info()
        self.analyzed_data = defaultdict(list)
        self.champion_lane_analyzed_data = defaultdict(lambda: defaultdict(list))
        self.match_summary_list = []
        self.match_list = []

    def get_match_summary_list(self, begin_time=None, end_time=None, save=True):
        match_summary_list = []
        for i, game_id in enumerate(self.summoner_info['seasons']['11']['solo_rank']['game_id_list']):
            match_summary = self.mdg.find_one_item({'game_id': game_id})
            if begin_time is not None and match_summary['creation'] < begin_time:
                continue
            if end_time is not None and match_summary['creation'] > end_time:
                continue
            match_summary_list.append(match_summary)
        if save:
            self.match_summary_list = match_summary_list
        return match_summary_list

    def get_match_list(self, begin_time=None, end_time=None, save=True):
        match_list = []
        match_summary_list = self.get_match_summary_list(begin_time, end_time)
        for match_summary in match_summary_list:
            game_id = match_summary['game_id']
            version_name = match_summary['version']
            match = self.mongodb_dict[version_name].find_one_item({'game_id': game_id})
            match_list.append(match)
        if save:
            self.match_list = match_list
        return match_list

    def update_additional_info(self):

        most_lane_info, second_most_lane_info = self.get_most_lane()
        most_champion_info_arr = self.get_most_champion()
        win, consecutive_num = self.get_consecutive_tag()

        self.summoner_info['lanes'] = [most_lane_info, second_most_lane_info]
        self.summoner_info['champions'] = most_champion_info_arr
        self.summoner_info['tags'] = {'consecutive': ('win' if win == 1 else 'lose', consecutive_num)}

    def get_consecutive_tag(self):
        recent_games = self.summoner_info['recent_games']
        last_win = recent_games[0][3]
        consecutive_num = 1
        for i in range(1, len(recent_games)):
            does_win = recent_games[i][3]
            if last_win == does_win:
                consecutive_num += 1
                last_win = does_win
            else:
                break
        return last_win, consecutive_num

    def get_most_champion(self, top_n=10):
        data = self.summoner_info['seasons'][SEASON]['solo_rank']['champions']
        champion_info_arr = []
        total_count = 0
        for champion_id, vs in data.items():
            count = vs['counts']
            losses = vs['losses']
            wins = vs['wins']
            total_count += count
            champion_info_arr.append((int(champion_id), count, wins, losses))
        champion_info_arr = sorted(champion_info_arr, key=lambda x: x[1], reverse=True)

        def _get_champion_info_dict(champion_info_tuple, total_count):
            champion_id, count, wins, losses = champion_info_tuple
            champion_ratio = count / total_count if total_count != 0 else 0
            winrate = wins / count if count != 0 else 0
            champion_info_dict = {
                'champion_id': champion_id,
                'winrate': round(winrate * 100),
                'champion_ratio': round(champion_ratio * 100),
                'count': count,
                'wins': wins,
                'losses': losses,
            }
            return champion_info_dict
        ret_arr = []
        for i in range(min(len(champion_info_arr), top_n)):
            champion_info_dict = _get_champion_info_dict(champion_info_arr[i], total_count)
            ret_arr.append(champion_info_dict)
        return ret_arr

    def get_most_lane(self):
        lane_dict = self.summoner_info['seasons'][SEASON]['solo_rank']['lanes']
        lane_info_arr = []
        total_count = 0
        for lane_name, vs in lane_dict.items():
            count = vs['counts']
            losses = vs['losses']
            wins = vs['wins']
            total_count += count
            lane_info_arr.append((lane_name, count, wins, losses))
        lane_info_arr = sorted(lane_info_arr, key=lambda x: x[1], reverse=True)

        def _get_lane_info_dict(lane_info_tuple, total_count):
            lane, count, wins, losses = lane_info_tuple
            lane_ratio = count / total_count if total_count != 0 else 0
            winrate = wins / count if count != 0 else 0
            lane_info_dict = {
                'lane': lane,
                'winrate': round(winrate * 100),
                'lane_ratio': round(lane_ratio * 100),
                'count': count,
                'wins': wins,
                'losses': losses,
            }
            return lane_info_dict

        most_lane_info_dict = _get_lane_info_dict(lane_info_arr[0], total_count)
        if len(lane_info_arr) >= 2:
            second_most_lane_info_dict = _get_lane_info_dict(lane_info_arr[1], total_count)
        else:
            second_most_lane_info_dict = {}
        return most_lane_info_dict, second_most_lane_info_dict

    def analyze_summoner_data(self):
        """
        match feature 및 match summary feature 를 추출하여 내부 변수로 저장하는 함수.
        실행하기 전에 self.get_match_list() 와 self.get_match_summary_list() 를 실행시켜야 함.
        """
        for ms in self.match_summary_list:
            tmp_analyzed_data, tmp_champion_lane_analyzed_data = match_summary_feature_extractor.extract_match_summary_feature(self.mds, ms, self)
            for k, v in tmp_analyzed_data.items():
                self.analyzed_data[k].append(v)

            for key, vs in tmp_champion_lane_analyzed_data.items():
                for k, v in vs.items():
                    self.champion_lane_analyzed_data[key][k].append(v)

        for m in self.match_list:
            lane_pred = predict_lane(m)
            if lane_pred is not None:
                try:
                    tmp_analyzed_data, tmp_champion_lane_analyzed_data = match_feature_extractor.extract_match_feature(self.mds, m, self, lane_pred)
                    for k, v in tmp_analyzed_data.items():
                        self.analyzed_data[k].append(v)

                    for key, vs in tmp_champion_lane_analyzed_data.items():
                        for k, v in vs.items():
                            self.champion_lane_analyzed_data[key][k].append(v)
                except IndexError as e:
                    print(e)
                except TypeError as e:
                    print(e)
