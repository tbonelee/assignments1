from collections import defaultdict
from ..lol.champion import Champion
from ..lol.mmr import MMR
champion = Champion()
mmr = MMR()


class MatchFeatureExtractor(object):

    def extract_match_feature(self, mds, match, summoner, lane_pred):
        analyzed_data = dict()
        champion_lane_analyzed_data = defaultdict(lambda: dict())

        my_champion, my_lane, opponent_champion, diff_dict = self.get_diff_after_lane_time(match, summoner.summoner_id, lane_pred, lane_time=13)
        key = (champion.champion_id_name_dict[my_champion], my_lane)
        for k, v in diff_dict.items():
            analyzed_data[k] = v
            champion_lane_analyzed_data[key][k] = v

        analyzed_data['opponent_mmr'] = mmr.convert_tier_to_point(match['tier'], 'I', 0)
        champion_lane_analyzed_data[key]['opponent_mmr'] = mmr.convert_tier_to_point(match['tier'], 'I', 0)
        return analyzed_data, champion_lane_analyzed_data

    def get_diff_after_lane_time(self, match_item, summoner_id, lane_pred, lane_time=13):
        for w in ['win', 'lose']:
            for p in match_item['teams'][w]['participants']:
                pid = str(p['stats']['participantId'])
                sid = p['summonerId']
                if sid == summoner_id:
                    my_win = w
                    my_champion = p['championId']
                    my_lane = lane_pred[pid]
                    my_gold_at_n_min = match_item['total_gold_logs'][pid][lane_time]
                    my_xp_at_n_min = match_item['xp_logs'][pid][lane_time]
                    if my_lane == 'JUNGLE':
                        my_cs_at_n_min = match_item['jungle_minions_killed_logs'][pid][lane_time]
                    else:
                        my_cs_at_n_min = match_item['minions_killed_logs'][pid][lane_time]
                    break

        for p in match_item['teams']['lose' if my_win == 'win' else 'win']['participants']:
            pid = str(p['stats']['participantId'])
            sid = p['summonerId']
            opponent_lane = lane_pred[pid]
            if opponent_lane == my_lane:
                opponent_win = 'lose' if my_win == 'win' else 'win'
                opponent_champion = p['championId']
                opponent_gold_at_n_min = match_item['total_gold_logs'][pid][lane_time]
                opponent_xp_at_n_min = match_item['xp_logs'][pid][lane_time]
                if opponent_lane == 'JUNGLE':
                    opponent_cs_at_n_min = match_item['jungle_minions_killed_logs'][pid][lane_time]
                else:
                    opponent_cs_at_n_min = match_item['minions_killed_logs'][pid][lane_time]
                break

        gold_diff_after_lane_time = my_gold_at_n_min - opponent_gold_at_n_min
        xp_diff_after_lane_time = my_xp_at_n_min - opponent_xp_at_n_min
        cs_diff_after_lane_time = my_cs_at_n_min - opponent_cs_at_n_min
        ret_dict = dict()
        ret_dict['gold_diff_after_lane_time'] = gold_diff_after_lane_time
        ret_dict['xp_diff_after_lane_time'] = xp_diff_after_lane_time
        ret_dict['cs_diff_after_lane_time'] = cs_diff_after_lane_time

        return my_champion, my_lane, opponent_champion, ret_dict
