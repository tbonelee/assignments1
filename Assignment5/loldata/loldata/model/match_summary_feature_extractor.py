from collections import defaultdict
from statistics import mean
from ..lol.mmr import MMR
from ..lol.champion import Champion

mmr = MMR()
champion = Champion()


class MatchSummaryFeature(object):
    @classmethod
    def get(cls, match_summary):
        raise NotImplementedError


class Duration(MatchSummaryFeature):
    @classmethod
    def get(cls, match_summary):
        return match_summary['duration'] / 60


class MatchSummarySummonerFeature(object):
    @classmethod
    def get(cls, match_summary, summuner_id):
        raise NotImplementedError

    @staticmethod
    def get_participant_info(match_summary, summoner_id):
        return match_summary['participants'][summoner_id]['info']


class Win(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return 1 if cls.get_participant_info(match_summary, summoner_id)['win'] else 0


class Dealt(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['dealt']


class DealtPerMin(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        duration = Duration.get(match_summary)
        dealt = Dealt.get(match_summary, summoner_id)
        return dealt / duration


class DealtPerGold(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        duration = Duration.get(match_summary)
        gold = Gold.get(match_summary, summoner_id)
        return gold / duration


class Gold(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['gold_earned']


class Kills(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['kills']


class Deaths(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['deaths']


class Assists(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['assists']


class KDA(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        kills = Kills.get(match_summary, summoner_id)
        assists = Assists.get(match_summary, summoner_id)
        deaths = Deaths.get(match_summary, summoner_id)
        kda = min((kills + assists) / deaths, 10) if deaths != 0 else 10
        return kda


class CSPerMin(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['cs_per_min']


class PercentRelatedKills(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['percent_related_kills']


class WardPlaced(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['ward_placed']


class GoldEarnedPerMin(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        duration = Duration.get(match_summary)
        gold = Gold.get(match_summary, summoner_id)
        return gold / duration


class MyTeam(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['team']


class OpponentTeam(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        my_team = MyTeam.get(match_summary, summoner_id)
        opponent_team = 'blue' if my_team == 'red' else 'red'
        return opponent_team


class AvgOpponentMMR(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, mds, match_summary, default_summoner):
        opponent_team = OpponentTeam.get(match_summary, default_summoner.summoner_id)
        avg_opponent_mmr = mean([cls.get_avg_opponent_mmr(mds, sid, default_summoner) for sid in match_summary['team'][opponent_team]['summoner_id_list']])
        return avg_opponent_mmr

    @classmethod
    def get_avg_opponent_mmr(cls, mds, summoner_id, default_summoner):
        summoner_info = mds.find_one_item({'summoner_id': summoner_id})
        if summoner_info['tier'] == '':
            point = mmr.calculate_mmr(default_summoner.summoner_info)
        else:
            point = mmr.calculate_mmr(summoner_info)
        return point


class ChampionName(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        champion_id = cls.get_participant_info(match_summary, summoner_id)['champion_id']
        champion_name = champion.champion_id_name_dict[champion_id]
        return champion_name


class ChampionLane(MatchSummarySummonerFeature):
    @classmethod
    def get(cls, match_summary, summoner_id):
        return cls.get_participant_info(match_summary, summoner_id)['lane']


class MatchSummaryFeatureExtractor(object):

    def extract_match_summary_feature(self, mds, match_summary, summoner):
        analyzed_data = defaultdict()
        champion_lane_analyzed_data = defaultdict(lambda: dict())

        summoner_id = summoner.summoner_id
        win = Win.get(match_summary, summoner_id)
        kda = KDA.get(match_summary, summoner_id)
        percent_related_kills = PercentRelatedKills.get(match_summary, summoner_id)
        cs_per_min = CSPerMin.get(match_summary, summoner_id)
        dealt_per_min = DealtPerMin.get(match_summary, summoner_id)
        dealt_per_gold = DealtPerGold.get(match_summary, summoner_id)
        ward_placed = WardPlaced.get(match_summary, summoner_id)
        gold_earned_per_min = GoldEarnedPerMin.get(match_summary, summoner_id)
        # avg_opponent_mmr = AvgOpponentMMR.get(mds, match_summary, summoner)

        analyzed_data['winrate'] = win
        analyzed_data['kda'] = kda
        analyzed_data['percent_related_kills'] = percent_related_kills
        analyzed_data['cs_per_min'] = cs_per_min
        analyzed_data['dealt_per_min'] = dealt_per_min
        analyzed_data['dealt_per_gold'] = dealt_per_gold
        analyzed_data['ward_placed'] = ward_placed
        analyzed_data['gold_earned_per_min'] = gold_earned_per_min
        # analyzed_data['avg_opponent_mmr'] = avg_opponent_mmr

        key = (ChampionName.get(match_summary, summoner_id),
               ChampionLane.get(match_summary, summoner_id))

        champion_lane_analyzed_data[key]['winrate'] = win
        champion_lane_analyzed_data[key]['kda'] = kda
        champion_lane_analyzed_data[key]['percent_related_kills'] = percent_related_kills
        champion_lane_analyzed_data[key]['cs_per_min'] = cs_per_min
        champion_lane_analyzed_data[key]['dealt_per_min'] = dealt_per_min
        champion_lane_analyzed_data[key]['dealt_per_gold'] = dealt_per_gold
        champion_lane_analyzed_data[key]['ward_placed'] = ward_placed
        champion_lane_analyzed_data[key]['gold_earned_per_min'] = gold_earned_per_min
        # champion_lane_analyzed_data[key]['avg_opponent_mmr'] = avg_opponent_mmr

        return analyzed_data, champion_lane_analyzed_data
