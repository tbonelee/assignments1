"""
Async Mongodb handler
"""
import os
from motor.motor_asyncio import AsyncIOMotorClient as MongoClient
from ..config import MongoDBConfig

class AsyncMongoDbHandler():

    def __init__(self, db_name=None, collection_name=None):
        mongo_config = MongoDBConfig()
        self.db_config = mongo_config.config_dict

        self._client = MongoClient("mongodb://{user}:{password}@{local_ip}:{port}".format(**self.db_config))


        if db_name is not None:
            self._db = self._client[db_name]
            if collection_name is not None:
                self._collection = self._db[collection_name]
            else:
                raise Exception("Need to collection name")
        else:
            raise Exception("Need to db name")

    def set_db(self, db_name=None, collection_name=None):
        if db_name is None:
            raise Exception("Need to dbname name")

        self._db = self._client[db_name]
        if collection_name is not None:
            self._collection = self._db[collection_name]


    def set_collection(self, collection_name=None):
        if collection_name is None:
            raise Exception("Need to dbname name")
        self._collection = self._db[collection_name]

    def get_current_db_name(self):
        return self._db.name

    def get_current_collection_name(self):
        return self._collection.name

    async def insert_item(self, data, db=None, collection=None):
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        await self._collection.insert_one(data)

    async def insert_items(self, datas, db=None, collection=None):
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        await self._collection.insert_many(datas)

    async def find_item(self, condition=None, db=None, collection=None, projection=None):
        if condition is None:
            condition = {}
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return await self._collection.find(condition, projection=projection, no_cursor_timeout=True)

    async def find_one_item(self, condition=None, db=None, collection=None, projection=None):
        if condition is None:
            condition = {}
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return await self._collection.find_one(condition, projection=projection)

    async def delete_item(self, condition=None, db=None, collection=None):
        if condition is None:
            raise Exception("Need to condition")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        await self._collection.delete_many(condition)

    async def update_item(self, condition=None, update_value=None, db=None, collection=None):
        if condition is None:
            raise Exception("Need to condition")
        if update_value is None:
            raise Exception("Need to update value")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return await self._collection.update_many(filter=condition, update=update_value)

    async def aggregate(self, pipeline=None, db=None, collection=None):
        if pipeline is None:
            raise Exception("Need to pipeline")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return await self._collection.aggregate(pipeline)
