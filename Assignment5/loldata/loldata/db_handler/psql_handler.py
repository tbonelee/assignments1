import psycopg2
from ..config import PSQLConfig


class PSQLHandler(object):

    def __init__(self, db_name='lolps_sync', connect=False, **kwargs):
        self.__mysql = None
        psql_config = PSQLConfig(db_name)
        self.config = psql_config.config_dict

        for key in kwargs:
            if key in self.config:
                self.config[key] = kwargs[key]
        if connect:
            self.connect()

    def connect(self):
        if self.__mysql is None:
            cnx = psycopg2.connect(user=self.config['user'], password=self.config['password'],
                                    host=self.config['host'], database=self.config['database'],
                                    port=self.config['port'])
            self.__mysql = cnx
        return self.__mysql

    def get_param_str(self, ids):
        pstr = '(' + '%s, ' * len(ids.split(','))
        pstr = pstr[:-2] + ')'
        return pstr

    def insert(self, table, ids, params):
        ins = "INSERT INTO " + table + ' ' + ids + ' ' + "VALUES " + self.get_param_str(ids)
        self.query(ins, params)

    def insert_ignore(self, table, ids, conflict_ids, params):
        ins = "INSERT INTO " + table + ' ' + ids + ' ' + "VALUES " + self.get_param_str(ids) + " ON CONFLICT " + conflict_ids + " DO NOTHING"
        self.query(ins, params)

    def update(self, table, set_values, condition):
        ins = "UPDATE " + table + ' SET ' + set_values + " WHERE " + condition
        self.query(ins, None)

    def delete(self, table, condition=None):
        query = "DELETE FROM " + table
        if condition is not None:
            query += " " + "WHERE " + condition
        self.query(query)

    def set_cursor(self):
        self.connection = self.connect()
        if self.connection == False:
            print("PSQL not connected")
            return False
        self.cur = self.connection.cursor()

    def commit(self):
        try:
            self.connection.commit()
        except Exception as err:
            print("An error occured: {}".format(err))

    def query(self, query, param=None):
        try:
            self.cur.execute(query, param)
        except Exception as err:
            print("An error occured: {}".format(err))
