"""
Mongodb handler
"""
import os
from pymongo import MongoClient
from pymongo.cursor import CursorType
from ..config import MongoDBConfig

class MongoDbHandler:

    def __init__(self, db_name=None, collection_name=None):
        mongo_config = MongoDBConfig()
        self.db_config = mongo_config.config_dict


        self._client = MongoClient("mongodb://{user}:{password}@{local_ip}:{port}".format(**self.db_config))


        if db_name is not None:
            self._db = self._client[db_name]
            if collection_name is not None:
                self._collection = self._db[collection_name]
            else:
                raise Exception("Need to collection name")
        else:
            raise Exception("Need to db name")

    def set_db(self, db_name=None, collection_name=None):
        if db_name is None:
            raise Exception("Need to dbname name")

        self._db = self._client[db_name]
        if collection_name is not None:
            self._collection = self._db[collection_name]


    def set_collection(self, collection_name=None):
        if collection_name is None:
            raise Exception("Need to dbname name")
        self._collection = self._db[collection_name]

    def get_current_db_name(self):
        return self._db.name

    def get_current_collection_name(self):
        return self._collection.name

    def insert_item(self, data, db=None, collection=None):
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.insert_one(data).inserted_id

    def insert_items(self, datas, db=None, collection=None):
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.insert_many(datas).inserted_ids

    def find_item(self, condition=None, db=None, collection=None, projection=None):
        if condition is None:
            condition = {}
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.find(condition, projection=projection)

    def find_one_item(self, condition=None, db=None, collection=None, projection=None):
        if condition is None:
            condition = {}
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.find_one(condition, projection=projection)

    def delete_item(self, condition=None, db=None, collection=None):
        if condition is None:
            raise Exception("Need to condition")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.delete_many(condition)

    def update_item(self, condition=None, update_value=None, db=None, collection=None):
        if condition is None:
            raise Exception("Need to condition")
        if update_value is None:
            raise Exception("Need to update value")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.update_many(filter=condition, update=update_value)

    def aggregate(self, pipeline=None, db=None, collection=None):
        if pipeline is None:
            raise Exception("Need to pipeline")
        if db is not None:
            self._db = self._client[db]
        if collection is not None:
            self._collection = self._db[collection]
        return self._collection.aggregate(pipeline)
