
class InvalidMatchDataError(Exception):
    pass

class InvalidSummonerDataError(Exception):
    pass

class NotSpecifiedSummonerError(Exception):
    pass

class NoSummonerError(Exception):
    pass