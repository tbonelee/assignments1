from .tier import Tier


class MMR(object):
    def __init__(self):
        self.weight_of_winrate = 3000

    def __call__(self, tier, rank, lp):
        return self.convert_tier_to_point(tier, rank, lp)

    def convert_tier_to_point(self, tier, rank, lp):
        return Tier.tier_point_dict[tier] + Tier.rank_point_dict[rank] + lp

    def calculate_mmr(self, summoner_item, season='11'):
        point = self.convert_tier_to_point(summoner_item['tier'], summoner_item['rank'], summoner_item['league_points'])
        wins = summoner_item['seasons'][season]['solo_rank']['wins']
        losses = summoner_item['seasons'][season]['solo_rank']['losses']
        mmr = point + self.convert_winrate_to_point(wins / (wins + losses))
        return mmr

    def convert_winrate_to_point(self, winrate):
        point = (0.5 - winrate) * self.weight_of_winrate
        return point
