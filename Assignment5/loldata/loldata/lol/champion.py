from ..db_handler.psql_handler import PSQLHandler


class Champion(object):

    def __init__(self):
        self.initialize()

    def initialize(self):
        sql = PSQLHandler(db_name='lolps_sync', connect=False)
        sql.set_cursor()
        sql.query('SELECT champion_id, champion_name_kr FROM champion_info')
        items = sql.cur.fetchall()
        self.champion_id_name_dict = dict()
        for champion_id, champion_name in items:
            self.champion_id_name_dict[champion_id] = champion_name
        self.champion_name_id_dict = {v: k for k, v in self.champion_id_name_dict.items()}
        sql.cur.close()
