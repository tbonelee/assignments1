from ..db_handler.psql_handler import PSQLHandler
from ..db_handler.mongodb_handler import MongoDbHandler


class Version(object):

    def __init__(self):
        self.initialize()

    def initialize(self):
        sql = PSQLHandler(db_name='lolps_sync', connect=False)
        sql.set_cursor()
        sql.query('SELECT version_id, description FROM version_info')
        items = sql.cur.fetchall()
        self.version_id_name_dict = dict()
        for version_id, description in items:
            self.version_id_name_dict[version_id] = description
        self.version_name_id_dict = {v: k for k, v in self.version_id_name_dict.items()}
        sql.cur.close()

    def get_last_version_name(self):
        __, last_version_name = sorted(self.version_id_name_dict.items())[-1]
        return last_version_name

    def get_last_version_id(self):
        last_version_id, __ = sorted(self.version_id_name_dict.items())[-1]
        return last_version_id

    def get_last_n_version_name_list(self, n=4):
        version_name_list = []
        for version_id, version_name in sorted(self.version_id_name_dict.items(), reverse=True)[:n]:
            version_name_list.append(version_name)
        return version_name_list

    def get_mongodb_dict(self):
        last_version_name = self.get_last_version_name()
        version_mongodb_dict = dict()
        season, last_patch = last_version_name.split('.')
        for i in range(1, int(last_patch) + 1):
            mgd = MongoDbHandler('riot', f'game_data_v{season}.{i}')
            version_mongodb_dict[f'{season}.{i}'] = mgd
        return version_mongodb_dict
