
class Tier(object):

    tier_point_dict = {
        'IRON': 0,
        'BRONZE': 500,
        'SILVER': 1000,
        'GOLD': 1500,
        'PLATINUM': 2000,
        'DIAMOND': 2500,
        'MASTER': 2700,
        'GRANDMASTER': 2700,
        'CHALLENGER': 2700
    }

    rank_point_dict = {
        'IV': 0,
        'III': 100,
        'II': 200,
        'I': 300
    }

    tier_enum_dict = {
        '': -1,
        'IRON': 0,
        'BRONZE': 1,
        'SILVER': 2,
        'GOLD': 3,
        'PLATINUM': 4,
        'DIAMOND': 5,
        'MASTER': 6,
        'GRANDMASTER': 7,
        'CHALLENGER': 8,
    }

    rank_enum_dict = {
        '': -1,
        'IV': 0,
        'III': 1,
        'II': 2,
        'I': 3
    }

    bronze = {
        ('BRONZE', 'IV'),
        ('BRONZE', 'III'),
        ('BRONZE', 'II'),
        ('BRONZE', 'I'),
    }

    silver = {
        ('SILVER', 'IV'),
        ('SILVER', 'III'),
        ('SILVER', 'II'),
        ('SILVER', 'I'),
    }

    gold = {
        ('GOLD', 'IV'),
        ('GOLD', 'III'),
        ('GOLD', 'II'),
        ('GOLD', 'I'),
    }

    platinum = {
        ('PLATINUM', 'IV'),
        ('PLATINUM', 'III'),
        ('PLATINUM', 'II'),
        ('PLATINUM', 'I'),
    }

    diamond = {
        ('DIAMOND', 'IV'),
        ('DIAMOND', 'III'),
        ('DIAMOND', 'II'),
        ('DIAMOND', 'I'),
    }

    master = {
        ('MASTER', 'I'),
    }

    grandmaster = {
        ('GRANDMASTER', 'I'),
    }

    challenger = {
        ('CHALLENGER', 'I')
    }

    low_elo = set.union(bronze, silver, gold)
    high_elo = set.union(platinum, diamond, master, grandmaster, challenger)
    very_high_elo = {
        ('DIAMOND', 'II'),
        ('DIAMOND', 'I'),
        ('MASTER', 'I'),
        ('GRANDMASTER', 'I'),
        ('CHALLENGER', 'I')
    }
