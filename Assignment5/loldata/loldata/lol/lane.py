from enum import IntEnum


class Lane(IntEnum):

    TOP = 0
    JUNGLE = 1
    MIDDLE = 2
    ADCARRY = 3
    SUPPORT = 4
