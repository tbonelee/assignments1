from riotwatcher import LolWatcher
from ..utils import get_now
from ..db_handler.psql_handler import PSQLHandler
from ..config import RiotConfig

class Item(object):
    
    def __init__(self):
        self.item_id_name_dict = dict()
        self.item_name_id_dict = dict()
        self.core_item_id_name_dict = dict()
        self.core_item_id_price_dict = dict()
        self.initialize()
        
        
    def initialize(self):        
        sql = PSQLHandler(db_name='lolps_sync', connect=False)
        sql.set_cursor()
        sql.query('SELECT item_id, item_name_kr, price, is_core FROM item_info')
        items = sql.cur.fetchall()
        for item_id, item_name_kr, price, is_core in items:
            self.item_id_name_dict[item_id] = item_name_kr
            self.item_name_id_dict[item_name_kr] = item_id
            if is_core == 1:
                self.core_item_id_name_dict[item_id] = item_name_kr
                self.core_item_id_price_dict[item_id] = price
        sql.cur.close()


    def update_item_info_db(self):
        def __get_item_id_name_dict(locale='ko_KR'):
            my_region = riot_config.my_region

            dd = watcher.data_dragon
            versions = dd.versions_for_region(my_region)['n']
            item_version = versions['item']
            items = dd.items(item_version[:13], locale=locale)
            item_id_name_dict = dict()

            for key, item in items['data'].items():
                item_name = item['name']
                item_id = int(key)
                item_id_name_dict[item_id] = item_name
            return item_id_name_dict

        def __get_item_id_price_dict(locale='ko_KR'):
            my_region = riot_config.my_region

            dd = watcher.data_dragon
            versions = dd.versions_for_region(my_region)['n']
            item_version = versions['item']
            items = dd.items(item_version[:13], locale=locale)
            item_id_name_dict = dict()

            for key, item in items['data'].items():
                item_price = item['gold']['total']
                item_id = int(key)
                item_id_name_dict[item_id] = item_price
            return item_id_name_dict

        def __update_item_info_dict(sql, item_id_name_dict, item_id_price_dict):
            sql.set_cursor()
            for item_id, item_name in item_id_name_dict.items():
                price = item_id_price_dict[item_id]
                sql.query('UPDATE item_info SET item_name_kr=%s, price=%s, updated_at=%s WHERE item_id=%s',(item_name, price, get_now(), item_id))
            sql.commit()

        riot_config = RiotConfig()
        watcher = LolWatcher(api_key=riot_config.api_key)
        
        item_id_name_dict = __get_item_id_name_dict()
        item_id_price_dict = __get_item_id_price_dict()
        sql_lolps = PSQLHandler(db_name='lolps', connect=False)
        sql_lolps_sync = PSQLHandler(db_name='lolps_sync', connect=False)
        sql_psweb = PSQLHandler(db_name='psweb', connect=False)

        __update_item_info_dict(sql_lolps, item_id_name_dict, item_id_price_dict)
        __update_item_info_dict(sql_lolps_sync, item_id_name_dict, item_id_price_dict)
        __update_item_info_dict(sql_psweb, item_id_name_dict, item_id_price_dict)
