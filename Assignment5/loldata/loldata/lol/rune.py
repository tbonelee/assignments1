from ..db_handler.psql_handler import PSQLHandler


class Rune(object):

    def __init__(self):
        self.initialize()

    def initialize(self):
        sql = PSQLHandler(db_name='lolps_sync', connect=False)
        sql.set_cursor()
        sql.query('SELECT rune_id, ruen_name_kr FROM rune_info')
        items = sql.cur.fetchall()
        self.rune_id_name_dict = dict()
        for rune_id, rune_name in items:
            self.rune_id_name_dict[rune_id] = rune_name
        self.rune_name_id_dict = {v: k for k, v in self.rune_id_name_dict.items()}
        sql.cur.close()
