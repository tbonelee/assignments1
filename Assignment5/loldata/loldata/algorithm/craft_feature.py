import logging
import logging.config
import numpy as np
from collections import defaultdict

from ..config import LogConfig
from ..db_handler.mongodb_handler import MongoDbHandler
from ..lol.version import Version
from ..lol.champion import Champion

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
feature_logger = logging.getLogger('craft_feature')
version = Version()
champion = Champion()

class CraftFeature(object):

    def __init__(self, candidate_summoner_id_list=None, 
    cleansed_features=None):
        self.season_name = '11'
        self.max_kda = 10
        self.game_num_per_summoner = 50
        self.print_log_iter = 1000
        self.craft_mmr_offset = 2000
        self.max_feature_num_per_champion_lane = 20000
        self.min_match_num_per_champion_lane = 2000
        self.pick_rate_threshold = 0.2
        self.match_num_threshold = 50
        self.docg = MongoDbHandler(db_name='riot', collection_name='game_summary')
        self.docs = MongoDbHandler(db_name='riot', collection_name='summoner_summary')
        self.version_mongodb_dict = version.get_mongodb_dict()

        self.feature_names = ['kills', 'deaths', 'assists', 'largestMultiKill', 'longestTimeSpentLiving', 'doubleKills', 'tripleKills',
                  'quadraKills', 'pentaKills', 'totalDamageDealt', 'magicDamageDealt', 'physicalDamageDealtToChampions',
                  'trueDamageDealtToChampions', 'totalHeal', 'totalUnitsHealed', 'visionScore', 'timeCCingOthers', 'totalDamageTaken',
                  'magicalDamageTaken', 'physicalDamageTaken', 'trueDamageTaken', 'goldEarned', 'goldSpent', 'champLevel',
                   'wardsKilled', 'damageDealtToObjectives', 'neutralMinionsKilled', 'wardsPlaced', 'totalMinionsKilled',
                  'visionWardsBoughtInGame', 'turretKills', 'neutralMinionsKilledEnemyJungle', 'kda']

        if cleansed_features is not None:
            self.cleansed_features = cleansed_features
        elif candidate_summoner_id_list is not None:
            self.aggregated_features = self.aggregate_features(candidate_summoner_id_list)
            self.cleansed_features = self.cleanse_features(self.aggregated_features)
        
        self.calculate_gaussian_params(self.cleansed_features)
        self.calculate_gaussian_params_per_champion_lane(self.cleansed_features)
        self.feature_names_dict = self.get_feature_names_dict(self.cleansed_features)


    def get_feature_names_dict(self, cleansed_features):
        feature_names_for_add = {'TOP':['kda'],
                                'JUNGLE':['kda'],
                                'MIDDLE':['kda'],
                                'ADCARRY':['kda', 'totalMinionsKilled', 'totalDamageDealt'],
                                'SUPPORT':['kda', 'visionScore', 'visionWardsBoughtInGame', 'wardsPlaced', 'assists']}
        feature_names_for_remove = {'TOP':['deaths'],
                                    'JUNGLE':['deaths'],
                                    'MIDDLE':['deaths'],
                                    'ADCARRY':['deaths'],
                                    'SUPPORT':['deaths']}
        feature_names_dict = dict()
        for k, vs in cleansed_features.items():
            champion_name, lane = k
            mu_features_of_champion_lane = self.mu_of_features_dict[k]

            diff = (mu_features_of_champion_lane - self.mu_of_features) / self.std_of_features
            indices = np.where(diff > 0.5)[0].tolist()
            for feature_name in feature_names_for_remove[lane]:
                idx = self.feature_names.index(feature_name)
                if idx in indices:
                    indices.remove(idx)
            for feature_name in feature_names_for_add[lane]:
                idx = self.feature_names.index(feature_name)
                if idx not in indices:
                    indices.append(idx)
            indices = np.array(indices)
            feature_names_dict[k] = indices
        return feature_names_dict


    def calculate_gaussian_params(self, champion_lane_feature_dict):
        avg_feature_list = []
        std_feature_list = []
        for k, vs in champion_lane_feature_dict.items():
            avg_feature_list.append(np.mean(vs, 0))
            std_feature_list.append(np.std(vs, 0))
        self.mu_of_features = np.mean(avg_feature_list, 0)
        self.std_of_features = np.mean(std_feature_list, 0)


    def calculate_gaussian_params_per_champion_lane(self, champion_lane_feature_dict):
        mu_dict = dict()
        std_dict = dict()
        for k, vs in champion_lane_feature_dict.items():
            mu_dict[k] = np.mean(vs, 0)
            std_dict[k] = np.std(vs, 0)
        self.mu_of_features_dict = mu_dict
        self.std_of_features_dict = std_dict


    def aggregate_features(self, candidate_summoner_id_list):
        champion_lane_feature_dict = defaultdict(list)
        processed_game_id_set = set()
        for i, csi in enumerate(candidate_summoner_id_list):
            summoner_item = self.docs.find_one_item({'summoner_id':csi})
            game_id_list = summoner_item['seasons'][self.season_name]['solo_rank']['game_id_list']
            game_id_list = sorted(game_id_list, reverse=True) # 최신 경기 기준
            for game_id in game_id_list[:self.game_num_per_summoner]:
                if game_id in processed_game_id_set:
                    continue
                self.process_game(game_id, champion_lane_feature_dict)
                processed_game_id_set.add(game_id)

            if i % self.print_log_iter == 0:
                feature_logger.info("%d-th summoner is aggregated."%i)

        return champion_lane_feature_dict


    def process_game(self, game_id, champion_lane_feature_dict):
        game_summary_item = self.docg.find_one_item({'game_id':game_id})
        version = game_summary_item['version']
        champion_lane_dict = self.get_champion_lane_dict(game_summary_item)
        game_item = self.version_mongodb_dict[version].find_one_item({'game_id':game_id})

        for w in ['win', 'lose']:
            for info_dict in game_item['teams'][w]['participants']:
                champion_id = info_dict['championId']
                champion_name = champion.champion_id_name_dict[champion_id]
                lane = champion_lane_dict[champion_id]
                info_dict['duration'] = game_item['game_duration']
                if len(champion_lane_feature_dict[(champion_name, lane)]) < self.max_feature_num_per_champion_lane:
                    features = self.extract_features(info_dict)
                    champion_lane_feature_dict[(champion_name, lane)].append(features)


    def get_champion_lane_dict(self, game_summary):
        champion_lane_dict = dict()
        for k, vs in game_summary['participants'].items():
            champion_lane_dict[vs['info']['champion_id']] = vs['info']['lane']
        return champion_lane_dict


    def extract_features(self, info_dict):
        def _calculate_kda(info_dict):
            if info_dict['stats']['deaths'] != 0:
                kda = min(self.max_kda, (info_dict['stats']['kills'] + info_dict['stats']['assists']) / info_dict['stats']['deaths'])
            else:
                kda = self.max_kda
            return kda

        duration = info_dict['duration']
        features = []
        for feature_name in self.feature_names:
            if feature_name in info_dict['stats']:
                feature = info_dict['stats'][feature_name] / duration
                features.append(feature)
            elif feature_name == 'kda':
                feature = _calculate_kda(info_dict)
                features.append(feature)
            else:
                raise IndexError
        return np.array(features)


    def cleanse_features(self, champion_lane_feature_dict):
        cleansed_champion_lane_feature_dict = dict()
        for k, vs in champion_lane_feature_dict.items():
            if len(vs) > self.min_match_num_per_champion_lane:
                cleansed_champion_lane_feature_dict[k] = np.array(vs)
        return cleansed_champion_lane_feature_dict


