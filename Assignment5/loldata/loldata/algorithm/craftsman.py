import logging
import asyncio
import numpy as np
from collections import defaultdict

from ..db_handler.mongodb_handler import MongoDbHandler
from ..db_handler.psql_handler import PSQLHandler

from ..utils import get_now
from multiprocessing import Process, Manager
from .craft_feature import CraftFeature

from ..lol.version import Version
from ..lol.mmr import MMR
from ..lol.lane import Lane
from ..lol.tier import Tier
from ..lol.champion import Champion
from ..config import LogConfig
from ..model.summoner import get_summoner_id_list

log_config = LogConfig()
craftsman_logger = logging.getLogger('craftsman')
version = Version()
mmr = MMR()
champion = Champion()


def get_candidate_summoner_id_list(docs):
    candidate_summoner_id_list = []
    high_elo_tier_list = [('DIAMOND', 'I'), ('DIAMOND', 'II'), ('MASTER', 'I'), ('GRANDMASTER', 'I'), ('CHALLENGER', 'I')]

    for i, item in enumerate(docs.find_item(projection={'summoner_id': 1, 'tier': 1, 'rank': 1})):
        key = (item['tier'], item['rank'])
        if key in high_elo_tier_list:
            candidate_summoner_id_list.append(item['summoner_id'])
    np.random.shuffle(candidate_summoner_id_list)
    return candidate_summoner_id_list


def get_my_win(game_item, summoner_id):
    for team, vs in game_item['team'].items():
        for s in vs['summoner_id_list']:
            if s == summoner_id:
                my_team = team
                return 'win' if game_item['team'][my_team]['win'] == True else 'lose'


def get_team_avg_mmr(docs, game_summary_item):
    mmr_list = []
    for team in ['red', 'blue']:
        for sid in game_summary_item['team'][team]['summoner_id_list']:
            mmr_score = get_mmr_of_summoner_id(docs, sid)
            mmr_list.append(mmr_score)
    return np.mean(mmr_list)


def get_mmr_of_summoner_id(docs, summoner_id):
    summoner_item = docs.find_one_item({'summoner_id': summoner_id})
    mmr_score = mmr.calculate_mmr(summoner_item)
    return mmr_score


def get_champion_lane_dict(game_summary):
    champion_lane_dict = dict()
    for k, vs in game_summary['participants'].items():
        champion_lane_dict[vs['info']['champion_id']] = vs['info']['lane']
    return champion_lane_dict


def collect_craft(start, end, candidate_summoner_id_list, feature, craft_score_list):
    docs = MongoDbHandler(db_name='riot', collection_name=collection_name_doc_summoner)
    docg = MongoDbHandler(db_name='riot', collection_name=collection_name_doc_game)
    version_mongodb_dict = version.get_mongodb_dict()
    target_version_list = version.get_last_n_version_name_list(5)

    for i, summoner_id in enumerate(candidate_summoner_id_list[start:end]):
        summoner_features_dict = defaultdict(list)
        summoner_avg_mmr_dict = defaultdict(list)
        summoner_item = docs.find_one_item({'summoner_id': summoner_id})
        summoner_name = summoner_item['summoner_name']
        game_id_list = summoner_item['seasons'][SEASON_NAME]['solo_rank']['game_id_list']
        game_id_list = sorted(game_id_list, reverse=True)
        for game_id in game_id_list:
            game_summary_item = docg.find_one_item({'game_id': game_id})
            version_name = game_summary_item['version']
            if version_name not in target_version_list:
                break

            champion_lane_dict = get_champion_lane_dict(game_summary_item)
            game_item = version_mongodb_dict[version_name].find_one_item({'game_id': game_id})
            w = get_my_win(game_summary_item, summoner_id)
            for info_dict in game_item['teams'][w]['participants']:
                if info_dict['summonerId'] == summoner_id:
                    champion_id = info_dict['championId']
                    champion_name = champion.champion_id_name_dict[champion_id]
                    lane = champion_lane_dict[champion_id]
                    info_dict['duration'] = game_item['game_duration']
                    if (champion_name, lane) in feature.feature_names_dict:
                        features = feature.extract_features(info_dict)
                        try:
                            avg_mmr = get_team_avg_mmr(docs, game_summary_item)
                            summoner_avg_mmr_dict[(champion_name, lane)].append(avg_mmr)
                            summoner_features_dict[(champion_name, lane)].append(features)
                        except KeyError as e:
                            pass
                        except Exception as e:
                            craftsman_logger.exception(e)
                        finally:
                            break
            
        extract_craft_score(feature, summoner_features_dict, summoner_avg_mmr_dict, summoner_id, summoner_name, craft_score_list)

        if i % 100 == 0:
            craftsman_logger.info("%d-th craftsman is collected." % i)


def extract_craft_score(feature, summoner_features_dict, summoner_avg_mmr_dict, summoner_id, summoner_name, craft_score_list):
    total_count = sum([len(vs) for vs in summoner_features_dict.values()])
    for k, vs in summoner_features_dict.items():
        if len(vs) > max(total_count * feature.pick_rate_threshold, feature.match_num_threshold):
            mu_of_features = np.mean(vs, 0)
            diff = (mu_of_features - feature.mu_of_features_dict[k]) / (feature.std_of_features_dict[k] + 1e-10)
            feature_score = np.mean(diff[feature.feature_names_dict[k]])
            mmr_score = np.mean(summoner_avg_mmr_dict[k]) - feature.craft_mmr_offset
            craft_score = mmr_score * feature_score
            if craft_score > 0:
                craft_score_list.append((k, summoner_name, summoner_id, feature_score, mmr_score, craft_score))
    return craft_score_list


def get_craft_score_dict(candidate_summoner_id_list, feature):
    manager = Manager()
    craft_score_list = manager.list()
    procs = []
    process_num = 15
    interval = len(candidate_summoner_id_list) // process_num
    for i in range(process_num):
        start = i * interval
        end = (i + 1) * interval
        proc = Process(target=collect_craft, args=(start, end, candidate_summoner_id_list, feature, craft_score_list))
        procs.append(proc)
        proc.start()
    for proc in procs:
        proc.join()
    craft_score_dict = defaultdict(list)
    for k, summoner_name, summoner_id, feature_score, mmr_score, craft_score in craft_score_list:
        craft_score_dict[k].append((summoner_name, summoner_id, feature_score, mmr_score, craft_score))
    return craft_score_dict


def update_craftsman_info(sql, version_id, craft_score_dict):
    sql.set_cursor()
    sql.delete('craftsman_info', condition='version_id=%d' % version_id)
    for k, vs in craft_score_dict.items():
        champion_name, lane_name = k
        champion_id = champion.champion_name_id_dict[champion_name]
        lane_id = Lane[lane_name]
        for item in sorted(vs, key=lambda x: x[-1], reverse=True)[:CRAFTSMAN_NUM_PER_CHAMPION_LANE]:
            summoner_name, summoner_id, feature_score, mmr_score, craft_score = item
            sql.insert('craftsman_info', '(version_id, champion_id, lane_id, summoner_name, summoner_id, feature_score, mmr_score, craft_score, updated_at)',
                       (version_id, champion_id, lane_id, summoner_name, summoner_id, feature_score, mmr_score, craft_score, get_now()))
    sql.commit()


CRAFTSMAN_NUM_PER_CHAMPION_LANE = 20
SEASON_NAME = '11'
collection_name_doc_game = 'game_summary'
collection_name_doc_summoner = 'summoner_summary'


def main():
    psql = PSQLHandler(db_name='lolps_sync')

    version_id = version.get_last_version_id()
    craftsman_logger.info("start to update a craftsman info table.")
    candidate_summoner_id_list = get_summoner_id_list(tier_set=Tier.very_high_elo, shuffle=True)
    craftsman_logger.info("%d candidate summoners are prepared." % len(candidate_summoner_id_list))
    feature = CraftFeature(candidate_summoner_id_list)
    craftsman_logger.info("A feature object is instantiated.")
    craft_score_dict = get_craft_score_dict(candidate_summoner_id_list, feature)
    craftsman_logger.info("A craft score dictinary is ready.")
    update_craftsman_info(psql, version_id, craft_score_dict)
    craftsman_logger.info("Updating craftsman info table is done.")


if __name__ == '__main__':
    main()
