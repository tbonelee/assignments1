from ..model.summoner import Summoner
from ..utils import get_now


def update_progamer_info(sql, pro_summoner_id_dict):
    sql.set_cursor()
    sql.delete('progamer_info')
    for k, vs in pro_summoner_id_dict.items():
        summoner_id = k
        nickname, team_name, summoner_name, pro_team_id, pro_or_academy = vs
        nickname = nickname[1:]
        summoner = Summoner(summoner_id)
        summoner_name = summoner.summoner_name
    
        sql.insert('progamer_info', '(nickname, team_name, summoner_name, pro_team_id, pro_or_academy, summoner_id, updated_at)',
                   (nickname, team_name, summoner_name, pro_team_id, pro_or_academy, summoner_id, get_now()))
    sql.commit()