import numpy as np
import random

def lane_predictor_wk(item):
    participant_ids = list(range(10))
    lanes = [None, None, None, None, None, None, None, None, None, None]
    teams = [None, None, None, None, None, None, None, None, None, None]
    # step 1. predict jungles  by jungle minions kiiled logs
    jungle_kills_blue = []
    jungle_kills_red = []
    for pid in participant_ids:
        logs = item['jungle_minions_killed_logs'][str(pid+1)]
        kills = np.mean(logs[:5])
        if pid < 5:
            jungle_kills_blue.append((kills, pid))
        else:
            jungle_kills_red.append((kills, pid))

    jungle_kills_blue = sorted(jungle_kills_blue, reverse=True)
    lanes[jungle_kills_blue[0][1]] = 'JUNGLE'

    jungle_kills_red = sorted(jungle_kills_red, reverse=True)
    lanes[jungle_kills_red[0][1]] = 'JUNGLE'

    # step 2. predict teams and lanes by position logs
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] is None, zip(participant_ids, lanes)):
        logs = item['position_logs'][str(pid+1)]
        avg_position = np.mean(logs[2:9], axis=0)
        if pid < 5:
            x = avg_position[0]
            blues.append((x, pid))
        else:
            x = avg_position[0]
            reds.append((x, pid))

    s = sorted(blues)
    #print(s)
    lanes[s[0][1]] = 'TOP'
    lanes[s[1][1]] = 'MIDDLE'
    lanes[s[2][1]] = 'BOTTOM'
    lanes[s[3][1]] = 'BOTTOM'

    s = sorted(reds)
    #print(s)
    lanes[s[0][1]] = 'TOP'
    lanes[s[1][1]] = 'MIDDLE'
    lanes[s[2][1]] = 'BOTTOM'
    lanes[s[3][1]] = 'BOTTOM'


    # step 3. discriminate adcarry and support
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] == 'BOTTOM', zip(participant_ids, lanes)):
        logs = item['minions_killed_logs'][str(pid+1)]
        kills = np.mean(logs[:30])
        if pid < 5:
            blues.append((kills, pid))
        else:
            reds.append((kills, pid))

    s = sorted(blues)
    lanes[s[0][1]] = 'SUPPORT'
    lanes[s[1][1]] = 'ADCARRY'

    s = sorted(reds)
    lanes[s[0][1]] = 'SUPPORT'
    lanes[s[1][1]] = 'ADCARRY'

    lane_dict = dict()
    for pid, lane in zip(participant_ids, lanes):
        lane_dict[str(pid+1)] = lane

    return lane_dict


def lane_predictor_harry(item):
    participant_ids = list(range(10))
    lanes_given = [None, None, None, None, None, None, None, None, None, None]
    lanes_predicted = [None, None, None, None, None, None, None, None, None, None]
    lanes = [None, None, None, None, None, None, None, None, None, None]
    lanes_ignore = [None, None, None, None, None, None, None, None, None, None]
    # step 1. check the given lines
    given_blue = []
    given_red = []
    for pid in participant_ids:
        logs = item['position_logs'][str(pid+1)]
        given=np.sum(logs[0])
        if pid < 5:
            given_blue.append((given,pid))
        else:
            given_red.append((given,pid))

    s = sorted(given_blue)
    #print(s)
    lanes_given[s[0][1]] = 'MIDDLE'
    lanes_given[s[1][1]] = 'ADCARRY'
    lanes_given[s[2][1]] = 'JUNGLE'
    lanes_given[s[3][1]] = 'SUPPORT'
    lanes_given[s[4][1]] = 'TOP'

    s = sorted(given_red)
    #print(s)
    lanes_given[s[0][1]] = 'MIDDLE'
    lanes_given[s[1][1]] = 'ADCARRY'
    lanes_given[s[2][1]] = 'JUNGLE'
    lanes_given[s[3][1]] = 'SUPPORT'
    lanes_given[s[4][1]] = 'TOP'

    # step 2. predict jungles  by jungle minions kiiled logs (rigid)
    jungle_kills_blue = []
    jungle_kills_red = []
    for pid in participant_ids:
        logs = item['jungle_minions_killed_logs'][str(pid+1)]
        kills = np.mean(logs[:5])
        if pid < 5:
            jungle_kills_blue.append((kills, pid))
        else:
            jungle_kills_red.append((kills, pid))

    jungle_kills_blue = sorted(jungle_kills_blue, reverse=True)
    lanes_predicted[jungle_kills_blue[0][1]] = 'JUNGLE'

    jungle_kills_red = sorted(jungle_kills_red, reverse=True)
    lanes_predicted[jungle_kills_red[0][1]] = 'JUNGLE'

    # step 3. predict teams and lanes by position logs (modified to an axis)
    blues = []
    reds = []
    for pid, _ in filter(lambda x: x[1] is None, zip(participant_ids, lanes_predicted)):
        logs = item['position_logs'][str(pid+1)]
        avg_position = np.mean(logs[2:5], axis=0)
        if pid < 5:
            x = avg_position[1]/avg_position[0]
            blues.append((x, pid))
        else:
            x = (15000-avg_position[0])/(15000-avg_position[1])
            reds.append((x, pid))

    s = sorted(blues, reverse=True)
    #print(s)
    if s[0][0]>1:
        lanes_predicted[s[0][1]] = 'TOP'
    if (s[1][0]>0.3) and (s[1][0]<4):
        lanes_predicted[s[1][1]] = 'MIDDLE'
    if (s[2][0]<1) and (s[2][0]>0):
        lanes_predicted[s[2][1]] = 'BOTTOM'
    if (s[3][0]<1) and (s[3][0]>0):
        lanes_predicted[s[3][1]] = 'BOTTOM'

    s = sorted(reds, reverse=True)
    #print(s)
    if s[0][0]>1:
        lanes_predicted[s[0][1]] = 'TOP'
    if (s[1][0]>0.3) and (s[1][0]<4):
        lanes_predicted[s[1][1]] = 'MIDDLE'
    if (s[2][0]<1) and (s[2][0]>0):
        lanes_predicted[s[2][1]] = 'BOTTOM'
    if (s[3][0]<1) and (s[3][0]>0):
        lanes_predicted[s[3][1]] = 'BOTTOM'

    # step 4. discriminate adcarry and support (rigid)
    bottom_blues = []
    bottom_reds = []
    for pid, _ in filter(lambda x: x[1] == 'BOTTOM', zip(participant_ids, lanes_predicted)):
        logs = item['minions_killed_logs'][str(pid+1)]
        kills = np.mean(logs[:30])
        if pid < 5:
            bottom_blues.append((kills, pid))
        else:
            bottom_reds.append((kills, pid))

    s = sorted(bottom_blues)
    if len(s) == 2:
        lanes_predicted[s[0][1]] = 'SUPPORT'
        lanes_predicted[s[1][1]] = 'ADCARRY'
    else:
        []
    s = sorted(bottom_reds)
    if len(s) == 2:
        lanes_predicted[s[0][1]] = 'SUPPORT'
        lanes_predicted[s[1][1]] = 'ADCARRY'
    else:
        []

    # step 5. compare the given and predicted lanes
    for pid in participant_ids:
        if lanes_predicted[pid]==lanes_given[pid]:
            lanes[pid]=lanes_predicted[pid]
        elif lanes_predicted[pid]==None or lanes_predicted[pid]=='BOTTOM':
            lanes[pid]=lanes_given[pid]+'*'
        else:
            lanes[pid]=lanes_predicted[pid]

    # ignore troll games
#    for pid in participant_ids:
#        if lanes_predicted[pid]==lanes_given[pid]:
#            lanes[pid]=lanes_predicted[pid]
#        elif lanes_predicted[pid]==None or lanes_predicted[pid]=='BOTTOM':
#            lanes=lanes_ignore
#            break
#        else:
#            lanes[pid]=lanes_predicted[pid]

    lane_dict = dict()
    for pid, lane in zip(participant_ids, lanes):
        lane_dict[str(pid+1)] = lane

    return lane_dict


def predict_lane(item):
    lane_pred1 = lane_predictor_wk(item)
    lane_pred2 = lane_predictor_harry(item)
    if lane_pred1 == lane_pred2:
        return lane_pred1
    else:
        return None

