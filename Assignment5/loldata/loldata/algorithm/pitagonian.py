import numpy as np

def time_norm(ps,origin):
    output=np.zeros((1,len(ps[0])), dtype=object)
    for i in range(len(ps[0])):
        weight=i/len(ps[0])
        output[0][i]=(1-weight)*ps[0][i]+weight*origin[0][i]

    return output[0]


def pitagonian_show(item, gold_rat=5,object_check=1):

    win_winrate_xp=np.zeros((1,len(item['xp_logs']['1'])-5))
    win_winrate_gold=np.zeros((1,len(item['xp_logs']['1'])-5))
    win_winrate=np.zeros((1,len(item['xp_logs']['1'])-5))


    for time in range(len(item['xp_logs']['1'])-5):
        tim=time+5
        exponent_gold=np.round((6/15)*tim)
        exponent_xp=12

        win_score_xp=0
        lose_score_xp=0

        win_score_gold=0
        lose_score_gold=0

        for participant in item['teams']['win']['participants']:
            participant_id = str(participant['stats']['participantId'])
            win_score_xp+=item['xp_logs'][participant_id][tim]
            win_score_gold+=item['total_gold_logs'][participant_id][tim]

        for participant in item['teams']['lose']['participants']:
            participant_id = str(participant['stats']['participantId'])
            lose_score_xp+=item['xp_logs'][participant_id][tim]
            lose_score_gold+=item['total_gold_logs'][participant_id][tim]

        win_score_gold=win_score_gold-2500-612*tim
        lose_score_gold=lose_score_gold-2500-612*tim

        obj_val=800
        if object_check == 1:
            if item['teams']['win']['participants'][0]['stats']['participantId']<6:
                win_ids = [1,2,3,4,5]
            else:
                win_ids = [6,7,8,9,10]

            for monster in item['monster_logs']:
                if monster['timestamp']<=60000*tim:
                    if monster['killer_id'] in win_ids:
                        win_score_gold+=obj_val
                    else:
                        lose_score_gold+=obj_val

        win_winrate_xp[0][time]=((win_score_xp/1000)**exponent_xp)/(((win_score_xp/1000)**exponent_xp)+((lose_score_xp/1000)**exponent_xp))
        win_winrate_gold[0][time]=((win_score_gold/1000)**exponent_gold)/(((win_score_gold/1000)**exponent_gold)+((lose_score_gold/1000)**exponent_gold))
        win_winrate[0][time]=(0.1*(10-gold_rat))*win_winrate_xp[0][time]+(0.1*(gold_rat))*win_winrate_gold[0][time]
        lose_winrate = 1-win_winrate
    return win_winrate, lose_winrate


def get_win_timeline_items(win_team_array, period_array, max_len=31):
    timeline_pita = np.zeros((1,max_len), dtype=object)
    timeline_count = np.zeros((1,max_len), dtype=object)
    timeline_origin = np.zeros((1,max_len), dtype=object)
    for i in range(len(win_team_array[0])):
        if i >= max_len:
            timeline_pita[0][max_len-1]+=win_team_array[0][i]
            timeline_count[0][max_len-1]+=period_array[0][i]
            timeline_origin[0][max_len-1]+=period_array[0][i]
        else:
            timeline_pita[0][i]+=win_team_array[0][i]
            timeline_count[0][i]+=period_array[0][i]
            timeline_origin[0][i]+=period_array[0][i]
    return timeline_pita, timeline_count, timeline_origin


def get_lose_timeline_items(lose_team_array, period_array, max_len=31):
    timeline_pita = np.zeros((1,max_len), dtype=object)
    timeline_count = np.zeros((1,max_len), dtype=object)
    timeline_origin = np.zeros((1,max_len), dtype=object)
    for i in range(len(lose_team_array[0])):
        if i >= max_len:
            timeline_pita[0][max_len-1]+=lose_team_array[0][i]
            timeline_count[0][max_len-1]+=period_array[0][i]
        else:
            timeline_pita[0][i]+=lose_team_array[0][i]
            timeline_count[0][i]+=period_array[0][i]
    return timeline_pita, timeline_count, timeline_origin


def get_timeline_arrs(item, gold_rat=5, object_check=1):
    win_team_array, lose_team_array = pitagonian_show(item=item, gold_rat=gold_rat,object_check=object_check)
    period_array=np.ones((1,len(win_team_array[0])), dtype=object)
    return win_team_array, lose_team_array, period_array


def pitagonian_one(item, win_participant_id, lose_participant_id, gold_rat=5, object_check=1):

    win_winrate_xp=np.zeros((1,len(item['xp_logs']['1'])-5))
    win_winrate_gold=np.zeros((1,len(item['xp_logs']['1'])-5))
    win_winrate=np.zeros((1,len(item['xp_logs']['1'])-5))


    for time in range(len(item['xp_logs']['1'])-5):
        tim=time+5
        exponent_gold=np.round((6/15)*tim)
        exponent_xp=12

        win_score_xp=0
        lose_score_xp=0

        win_score_gold=0
        lose_score_gold=0

        win_participant_id=str(win_participant_id)
        lose_participant_id=str(lose_participant_id)

        win_score_xp+=item['xp_logs'][win_participant_id][tim]*5
        win_score_gold+=item['total_gold_logs'][win_participant_id][tim]*5
        lose_score_xp+=item['xp_logs'][lose_participant_id][tim]*5
        lose_score_gold+=item['total_gold_logs'][lose_participant_id][tim]*5

        win_score_gold=win_score_gold-2500-612*tim
        lose_score_gold=lose_score_gold-2500-612*tim

        obj_val=800
        if object_check == 1:
            blue_idx=0
            if item['teams']['win']['participants'][0]['stats']['participantId']<6:
                win_ids = [1,2,3,4,5]
            else:
                win_ids = [6,7,8,9,10]

            for monster in item['monster_logs']:
                if monster['timestamp']<=60000*tim:
                    if monster['killer_id'] in win_ids:
                        win_score_gold += obj_val
                    else:
                        lose_score_gold += obj_val

        win_winrate_xp[0][time]=((win_score_xp/1000)**exponent_xp)/(((win_score_xp/1000)**exponent_xp)+((lose_score_xp/1000)**exponent_xp))
        win_winrate_gold[0][time]=((win_score_gold/1000)**exponent_gold)/(((win_score_gold/1000)**exponent_gold)+((lose_score_gold/1000)**exponent_gold))
        win_winrate[0][time]=(0.1*(10-gold_rat))*win_winrate_xp[0][time]+(0.1*(gold_rat))*win_winrate_gold[0][time]
        lose_winrate = 1-win_winrate
    return win_winrate, lose_winrate


