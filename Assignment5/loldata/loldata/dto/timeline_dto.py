from collections import defaultdict
from ..riotapi.riot_watcher_wrapper_async import timeline_by_match
from ..error import InvalidMatchDataError


class TimelineDTO(object):

    def __init__(self, my_region='kr'):
        self.my_region = my_region


    async def __call__(self, game_id) ->  dict:
        raw_timeline = await timeline_by_match(self.my_region, game_id)
        timeline_data = self.extract_timeline_data(raw_timeline)
        return timeline_data


    def extract_timeline_data(self, timeline) -> dict:

        timeline_data = dict()
        timeline_data['item_logs'] = defaultdict(list)
        timeline_data['item_timestamp_logs'] = defaultdict(list)
        timeline_data['position_logs'] = defaultdict(list)
        timeline_data['total_gold_logs'] = defaultdict(list)
        timeline_data['xp_logs'] = defaultdict(list)
        timeline_data['minions_killed_logs'] = defaultdict(list)
        timeline_data['jungle_minions_killed_logs'] = defaultdict(list)
        timeline_data['skill_logs'] = defaultdict(list)
        timeline_data['skill_timestamp_logs'] = defaultdict(list)
        timeline_data['monster_logs'] = []
        timeline_data['building_logs'] = []
        timeline_data['champion_kill_logs'] = []

        if 'frames' not in timeline:
            raise InvalidMatchDataError("frames not in timeline.")
        for frame in timeline['frames']:
            for event in frame['events']:
                if event['type'] in ['ITEM_PURCHASED', 'ITEM_SOLD', 'ITEM_UNDO', 'ITEM_DESTROYED']:
                    self.update_item_logs(event, timeline_data)
                elif event['type'] == 'ELITE_MONSTER_KILL':
                    self.update_monster_logs(event, timeline_data)
                elif event['type'] == 'SKILL_LEVEL_UP':
                    self.update_skill_logs(event, timeline_data)
                elif event['type'] == 'BUILDING_KILL':
                    self.update_building_kill_logs(event, timeline_data)
                elif event['type'] == 'CHAMPION_KILL':
                    self.update_champion_kill_logs(event, timeline_data)
            self.update_participant_frames_logs(frame['participantFrames'], timeline_data)

        return timeline_data


    def update_item_logs(self, event, timeline_data) -> None:
        if event['type'] == 'ITEM_PURCHASED':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)
        elif event['type'] == 'ITEM_DESTROYED':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(-item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)
        elif event['type'] == 'ITEM_UNDO':
            participant = str(event['participantId'])
            timeline_data['item_logs'][participant].pop(-1)
            timeline_data['item_timestamp_logs'][participant].pop(-1)
        elif event['type'] == 'ITEM_SOLD':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(-item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)


    def update_monster_logs(self, event, timeline_data) -> None:
        monster_type = event['monsterType']
        if monster_type == 'DRAGON':
            monster_type = event['monsterSubType']
        timestamp = event['timestamp']
        killer_id = event['killerId']
        timeline_data['monster_logs'].append(dict({
                'timestamp': timestamp,
                'killer_id': killer_id,
                'monster_type': monster_type
            }))


    def update_skill_logs(self, event, timeline_data) -> None:
        skill_slot = event['skillSlot']
        timestamp = event['timestamp']
        participant = str(event['participantId'])
        timeline_data['skill_logs'][participant].append(skill_slot)
        timeline_data['skill_timestamp_logs'][participant].append(timestamp)


    def update_building_kill_logs(self, event, timeline_data) -> None:
        building_type = event['buildingType']
        timestamp = event['timestamp']
        killer_id = event['killerId']
        timeline_data['building_logs'].append(dict({
                'timestamp': timestamp,
                'killer_id': killer_id,
                'building_type': building_type
            }))


    def update_champion_kill_logs(self, event, timeline_data) -> None:
        timestamp = event['timestamp']
        killer_id = event['killerId']
        victim_id = event['victimId']
        assisting_participant_ids = event['assistingParticipantIds']
        position = event['position']
        timeline_data['champion_kill_logs'].append(dict({
                'timestamp': timestamp,
                'killer_id': killer_id,
                'victim_id': victim_id,
                'assisting_participant_ids': assisting_participant_ids,
                'position': position,
            }))


    def update_participant_frames_logs(self, frame, timeline_data) -> None:
        for v in frame.values():
            participant_id = str(v['participantId'])
            if 'position' in v:
                position = (v['position']['x'], v['position']['y'])
            else:
                position = (-1, -1)
            total_gold = v['totalGold']
            xp = v['xp']
            minions_killed = v['minionsKilled']
            jungle_minions_killed = v['jungleMinionsKilled']
            timeline_data['position_logs'][participant_id].append(position)
            timeline_data['total_gold_logs'][participant_id].append(total_gold)
            timeline_data['xp_logs'][participant_id].append(xp)
            timeline_data['minions_killed_logs'][participant_id].append(minions_killed)
            timeline_data['jungle_minions_killed_logs'][participant_id].append(jungle_minions_killed)

