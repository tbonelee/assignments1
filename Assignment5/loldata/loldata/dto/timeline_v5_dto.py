from collections import defaultdict
# from ..riotapi.riot_watcher_wrapper_async import timeline_by_match_v5
# from ..error import InvalidMatchDataError


class TimelineDTO(object):

    def __init__(self, my_region='asia'):
        self.my_region = my_region
        self.champion_stat_keys = ["abilityHaste",
                                   "abilityPower",
                                   "armor",
                                   "armorPen",
                                   "armorPenPercent",
                                   "attackDamage",
                                   "attackSpeed",
                                   "bonusArmorPenPercent",
                                   "bonusMagicPenPercent",
                                   "ccReduction",
                                   "cooldownReduction",
                                   "health",
                                   "healthMax",
                                   "healthRegen",
                                   "lifesteal",
                                   "magicPen",
                                   "magicPenPercent",
                                   "magicResist",
                                   "movementSpeed",
                                   "omnivamp",
                                   "physicalVamp",
                                   "power",
                                   "powerMax",
                                   "powerRegen",
                                   "spellVamp"]
        self.damage_stat_keys = ["magicDamageDone",
                                 "magicDamageDoneToChampions",
                                 "magicDamageTaken",
                                 "physicalDamageDone",
                                 "physicalDamageDoneToChampions",
                                 "physicalDamageTaken",
                                 "totalDamageDone",
                                 "totalDamageDoneToChampions",
                                 "totalDamageTaken",
                                 "trueDamageDone",
                                 "trueDamageDoneToChampions",
                                 "trueDamageTaken"]

    def __call__(self, raw_timeline) -> dict:
        # 가공
        timeline_data = self.extract_timeline_data(raw_timeline)
        # print("timeline called")
        return timeline_data

    def validate_raw_timeline(self, raw_timeline):
        if 'info' not in raw_timeline.keys():
            # raise InvalidMatchDataError(raw_timeline)
            pass

    def extract_timeline_data(self, timeline) -> dict:
        timeline_data = dict()
        timeline_data['item_logs'] = defaultdict(list)
        timeline_data['item_timestamp_logs'] = defaultdict(list)
        timeline_data['position_logs'] = defaultdict(list)
        timeline_data['total_gold_logs'] = defaultdict(list)
        timeline_data['xp_logs'] = defaultdict(list)
        timeline_data['minions_killed_logs'] = defaultdict(list)
        timeline_data['jungle_minions_killed_logs'] = defaultdict(list)
        timeline_data['skill_logs'] = defaultdict(list)
        timeline_data['skill_timestamp_logs'] = defaultdict(list)
        timeline_data['championStats_logs'] = dict()
        timeline_data['damageStats_logs'] = dict()
        timeline_data['monster_logs'] = []
        timeline_data['dragon_soul_logs'] = []
        timeline_data['building_logs'] = []
        timeline_data['champion_kill_logs'] = []
        timeline_data['champion_special_kill_logs'] = []
        timeline_data['turret_plate_destroyed_logs'] = []

        if 'frames' not in timeline:
            # raise InvalidMatchDataError("frames not in timeline.")
            raise Exception("frames not in timeline")
        for frame in timeline['frames']:
            for event in frame['events']:
                if event['type'] in ['ITEM_PURCHASED', 'ITEM_SOLD', 'ITEM_UNDO', 'ITEM_DESTROYED']:
                    self.update_item_logs(event, timeline_data)
                elif event['type'] == 'ELITE_MONSTER_KILL':
                    self.update_monster_logs(event, timeline_data)
                elif event['type'] == 'DRAGON_SOUL_GIVEN':
                    self.update_dragon_soul_logs(event, timeline_data)
                elif event['type'] == 'SKILL_LEVEL_UP':
                    self.update_skill_logs(event, timeline_data)
                elif event['type'] == 'BUILDING_KILL':
                    self.update_building_kill_logs(event, timeline_data)
                elif event['type'] == 'CHAMPION_KILL':
                    self.update_champion_kill_logs(event, timeline_data)
                elif event['type'] == 'CHAMPION_SPECIAL_KILL':
                    self.update_champion_special_kill_logs(event, timeline_data)
                elif event['type'] == 'TURRET_PLATE_DESTROYED':
                    self.update_turret_plate_destroyed_logs(event, timeline_data)
            self.update_participant_frames_logs(frame['participantFrames'], timeline_data)

        return timeline_data

    def update_item_logs(self, event, timeline_data) -> None:
        if event['type'] == 'ITEM_PURCHASED':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)
        elif event['type'] == 'ITEM_DESTROYED':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(-item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)
        elif event['type'] == 'ITEM_UNDO':
            participant = str(event['participantId'])
            timeline_data['item_logs'][participant].pop(-1)
            timeline_data['item_timestamp_logs'][participant].pop(-1)
        elif event['type'] == 'ITEM_SOLD':
            participant = str(event['participantId'])
            item_id = event['itemId']
            timestamp = event['timestamp']
            timeline_data['item_logs'][participant].append(-item_id)
            timeline_data['item_timestamp_logs'][participant].append(timestamp)

    def update_monster_logs(self, event, timeline_data) -> None:
        monster_type = event['monsterType']
        if monster_type == 'DRAGON':
            monster_type = event['monsterSubType']
        timestamp = event['timestamp']
        killer_id = event['killerId']
        killer_team_id = event['killerTeamId']
        timeline_data['monster_logs'].append(dict({
            'timestamp': timestamp,
            'killer_id': killer_id,
            'killer_team_id': killer_team_id,
            'monster_type': monster_type
        }))

    def update_dragon_soul_logs(self, event, timeline_data) -> None:
        soul_name = event['name']
        timestamp = event['timestamp']
        team_id = event['teamId']
        timeline_data['dragon_soul_logs'].append(dict({
            'timestamp': timestamp,
            'team_id': team_id,
            'soul_name': soul_name
        }))

    def update_skill_logs(self, event, timeline_data) -> None:
        skill_slot = event['skillSlot']
        timestamp = event['timestamp']
        participant = str(event['participantId'])
        timeline_data['skill_logs'][participant].append(skill_slot)
        timeline_data['skill_timestamp_logs'][participant].append(timestamp)

    def update_building_kill_logs(self, event, timeline_data) -> None:
        building_type = event['buildingType']
        timestamp = event['timestamp']
        killer_id = event['killerId']
        timeline_data['building_logs'].append(dict({
            'timestamp': timestamp,
            'killer_id': killer_id,
            'building_type': building_type
        }))

    def update_champion_kill_logs(self, event, timeline_data) -> None:
        timestamp = event['timestamp']
        killer_id = event['killerId']
        victim_id = event['victimId']
        try:
            assisting_participant_ids = event['assistingParticipantIds']
        except:
            assisting_participant_ids = []
        position = event['position']
        timeline_data['champion_kill_logs'].append(dict({
            'timestamp': timestamp,
            'killer_id': killer_id,
            'victim_id': victim_id,
            'assisting_participant_ids': assisting_participant_ids,
            'position': position,
        }))

    def update_champion_special_kill_logs(self, event, timeline_data) -> None:
        timestamp = event['timestamp']
        killer_id = event['killerId']
        position = event['position']
        kill_type = event['killType']
        if kill_type == "KILL_MULTI":
            multikill_length = event['multiKillLength']
            timeline_data['champion_special_kill_logs'].append(dict({
                'timestamp': timestamp,
                'kill_type': kill_type,
                'killer_id': killer_id,
                'multikill_length': multikill_length,
                'position': position,
            }))
        else:
            timeline_data['champion_special_kill_logs'].append(dict({
                'timestamp': timestamp,
                'kill_type': kill_type,
                'killer_id': killer_id,
                'position': position,
            }))

    def update_turret_plate_destroyed_logs(self, event, timeline_data) -> None:
        timestamp = event['timestamp']
        killer_id = event['killerId']
        laneType = event['laneType']
        teamId = event['teamId']
        position = event['position']
        timeline_data['turret_plate_destroyed_logs'].append(dict({
            'timestamp': timestamp,
            'killer_id': killer_id,
            'laneType': laneType,
            'teamId': teamId,
            'position': position,
        }))

    def update_participant_frames_logs(self, frame, timeline_data) -> None:
        for v in frame.values():
            participant_id = str(v['participantId'])
            if 'position' in v:
                position = (v['position']['x'], v['position']['y'])
            else:
                position = (-1, -1)
            total_gold = v['totalGold']
            xp = v['xp']
            minions_killed = v['minionsKilled']
            jungle_minions_killed = v['jungleMinionsKilled']
            timeline_data['position_logs'][participant_id].append(position)
            timeline_data['total_gold_logs'][participant_id].append(total_gold)
            timeline_data['xp_logs'][participant_id].append(xp)
            timeline_data['minions_killed_logs'][participant_id].append(minions_killed)
            timeline_data['jungle_minions_killed_logs'][participant_id].append(jungle_minions_killed)
            self.update_champion_stats(v['championStats'], timeline_data, participant_id)
            self.update_damage_stats(v['damageStats'], timeline_data, participant_id)

    def update_champion_stats(self, championStats, timeline_data, participant_id) -> None:
        for k in self.champion_stat_keys:
            if k in championStats:
                try:
                    timeline_data['championStats_logs'][k][participant_id].append(championStats[k])
                except KeyError:
                    timeline_data['championStats_logs'][k] = defaultdict(list)
                    timeline_data['championStats_logs'][k][participant_id].append(championStats[k])
            else:
                # raise InvalidMatchDataError(f"{k} is not in stats_dict.")
                raise Exception(f"{k} is not in stats_dict.")

    def update_damage_stats(self, damageStats, timeline_data, participant_id) -> None:
        for k in self.damage_stat_keys:
            if k in damageStats:
                try:
                    timeline_data['damageStats_logs'][k][participant_id].append(damageStats[k])
                except KeyError:
                    timeline_data['damageStats_logs'][k] = defaultdict(list)
                    timeline_data['damageStats_logs'][k][participant_id].append(damageStats[k])
            else:
                # raise InvalidMatchDataError(f"{k} is not in stats_dict.")
                raise Exception(f"{k} is not in stats_dict.")
