import logging
import logging.config
import numpy as np
import time
from ..utils import convert_to_dict_mongodb
from ..config import LogConfig
from ..lol.tier import Tier
from ..db_handler.mongodb_handler import MongoDbHandler

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("dto.summoner_dto")


SEASON = '11'
SEASON_ID = 14
season_dict = {13: '10',
               14: '11'}


class SummonerDTO(object):

    def __init__(self):
        self.recent_game_num = 30

    async def get_tier_name_from_summoner_summary(self, async_mds, summoner_id):
        item = await async_mds.find_one_item({'summoner_id': summoner_id}, projection={'tier': 1, 'rank': 1})
        tier = item['tier']
        rank = item['rank']
        return tier, rank

    async def update_match_info_of_summoner_summary(self, async_mds, summoner_id, game_summary, update_summoner_name=True):

        gsp = game_summary['participants'][summoner_id]
        puuid = gsp['puuid']
        summoner_name = gsp['summoner_name']
        item = await async_mds.find_one_item({'summoner_id': summoner_id})
        if item is None:
            item = self.get_initial_summoner_info(summoner_id, puuid=puuid, summoner_name=summoner_name)
            await async_mds.insert_item(item)

        game_id = game_summary['game_id']
        champion_id = str(gsp['champion_id'])
        lane = gsp['info']['lane']
        win = 1 if gsp['info']['win'] else 0
        lose = 0 if gsp['info']['win'] else 1
        kills = gsp['info']['kills']
        deaths = gsp['info']['deaths']
        assists = gsp['info']['assists']
        creation = game_summary['creation']
        tup = (game_id, champion_id, lane, win, kills, deaths, assists, creation)

        season = season_dict[game_summary['season_id']]

        if game_id in [x[0] for x in item['recent_games']]:
            return
        recent_games = item['recent_games']
        recent_games.insert(0, tup)
        recent_games = sorted(recent_games, key=lambda x: x[-1], reverse=True)
        recent_games = recent_games[:self.recent_game_num]
        game_id_list = item['seasons'][season]['solo_rank']['game_id_list']
        if game_id in game_id_list:
            return
        game_id_list.insert(0, game_id)
        if champion_id in item['seasons'][season]['solo_rank']['champions']:
            champion_wins = item['seasons'][season]['solo_rank']['champions'][champion_id]['wins'] + win
            champion_losses = item['seasons'][season]['solo_rank']['champions'][champion_id]['losses'] + lose
            champion_counts = item['seasons'][season]['solo_rank']['champions'][champion_id]['counts'] + 1
        else:
            champion_wins = win
            champion_losses = lose
            champion_counts = 1
        if lane in item['seasons'][season]['solo_rank']['lanes']:
            lane_wins = item['seasons'][season]['solo_rank']['lanes'][lane]['wins'] + win
            lane_losses = item['seasons'][season]['solo_rank']['lanes'][lane]['losses'] + lose
            lane_counts = item['seasons'][season]['solo_rank']['lanes'][lane]['counts'] + 1
        else:
            lane_wins = win
            lane_losses = lose
            lane_counts = 1
        set_item = {'puuid': puuid,
                    'seasons': {
                        season: {
                            'solo_rank': {
                                'game_id_list': game_id_list,
                                'champions': {
                                    str(champion_id): {
                                        'wins': champion_wins,
                                        'losses': champion_losses,
                                        'counts': champion_counts
                                    }
                                },
                                'lanes': {
                                    lane: {
                                        'wins': lane_wins,
                                        'losses': lane_losses,
                                        'counts': lane_counts
                                    }
                                }
                            }
                        }
                    },
                    'recent_games': recent_games,
                    }
        if update_summoner_name:
            set_item['summoner_name'] = summoner_name
        converted_dict = convert_to_dict_mongodb(set_item)
        await async_mds.update_item({
            'summoner_id': summoner_id
        },
            {
            '$set': converted_dict
        }
        )
        logger.info(f'summoner summary is updated : {summoner_name}')

    async def update_summoner_tier_info(self, async_mds, summoner_id, summoner_info):

        tier = summoner_info['tier']
        rank = summoner_info['rank']
        league_points = summoner_info['leaguePoints']
        wins = summoner_info['wins']
        losses = summoner_info['losses']
        summoner_name = summoner_info['summonerName']
        item = await async_mds.find_one_item({'summoner_id': summoner_id})
        if item is None:
            item = self.get_initial_summoner_info(summoner_id, summoner_name=summoner_name,
                                                  tier=tier, rank=rank, league_points=league_points,
                                                  wins=wins, losses=losses)
            await async_mds.insert_item(item)
        else:
            last_best_tier = item['seasons'][SEASON]['solo_rank']['best_tier']
            last_best_rank = item['seasons'][SEASON]['solo_rank']['best_rank']
            last_best_lp = item['seasons'][SEASON]['solo_rank']['best_league_point']
            last_called_by_stacker = item['last_called_by_stacker']
            best_tier, best_rank, best_league_point = self.max_tier_rank_lp(last_best_tier, last_best_rank, last_best_lp, tier, rank, league_points)
            set_item = {'summoner_name': summoner_name,
                        'tier': tier,
                        'rank': rank,
                        'league_points': league_points,
                        'last_called_by_stacker': round(time.time()) * 1000,
                        'last_updated': round(time.time()) * 1000,
                        'seasons': {
                            SEASON: {
                                'solo_rank': {
                                    'tier': tier,
                                    'rank': rank,
                                    'league_points': league_points,
                                    'wins': wins,
                                    'losses': losses,
                                    'best_tier': best_tier,
                                    'best_rank': best_rank,
                                    'best_league_point': best_league_point,
                                }
                            }
                        }
                        }
            converted_dict = convert_to_dict_mongodb(set_item)
            await async_mds.update_item({'summoner_id': summoner_id},
                                        {'$set': converted_dict}
                                        )
            logger.info('update summoner tier info : %s' % summoner_name)
            return last_called_by_stacker

    def get_initial_summoner_info(self, summoner_id, puuid='', summoner_name='',
                                  tier='', rank='', league_points=0, wins=0, losses=0):
        info = dict()
        info['summoner_id'] = summoner_id
        info['summoner_name'] = summoner_name
        info['puuid'] = puuid
        info['summoner_level'] = None
        info['profile_icon_id'] = None
        info['tier'] = tier
        info['rank'] = rank
        info['league_points'] = league_points
        info['last_updated'] = round(time.time()) * 1000
        info['last_called_by_stacker'] = round(time.time()) * 1000
        info['seasons'] = dict()
        info['seasons'][SEASON] = dict()
        info['seasons'][SEASON]['solo_rank'] = self.get_initial_rank_info(tier=tier, rank=rank, league_points=league_points, wins=wins, losses=losses)
        info['seasons'][SEASON]['flex_rank'] = self.get_initial_rank_info()

        info['recent_games'] = []
        return info

    def max_tier_rank_lp(self, last_tier, last_rank, last_lp, now_tier, now_rank, now_lp):
        best_tier, best_rank, best_lp = max([(last_tier, last_rank, last_lp), (now_tier, now_rank, now_lp)], key=lambda x: (Tier.tier_enum_dict[x[0]], Tier.rank_enum_dict[x[1]], x[2]))
        return best_tier, best_rank, best_lp

    def get_initial_rank_info(self, tier='', rank='', league_points=0, wins=0, losses=0):
        rank_info = dict()
        rank_info['tier'] = tier
        rank_info['rank'] = rank
        rank_info['league_points'] = league_points
        rank_info['best_tier'] = tier
        rank_info['best_rank'] = rank
        rank_info['best_league_point'] = league_points
        rank_info['wins'] = wins
        rank_info['losses'] = losses
        rank_info['champions'] = dict()
        rank_info['lanes'] = dict()
        rank_info['game_id_list'] = []
        return rank_info
