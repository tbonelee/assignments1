
from collections import defaultdict
from ..riotapi.riot_watcher_wrapper_async import match_by_id
from ..utils import get_now_timestamp
from ..error import InvalidMatchDataError
from .timeline_dto import TimelineDTO


class MatchDTO(object):

    def __init__(self, my_region='kr', target_version=None):
        self.my_region = my_region
        self.target_version = target_version
        self.timeline_dto = TimelineDTO(my_region)
        self.stat_keys = ['kills',
                          'deaths',
                          'assists',
                          'largestMultiKill',
                          'longestTimeSpentLiving',
                          'doubleKills',
                          'tripleKills',
                          'quadraKills',
                          'pentaKills',
                          'unrealKills',
                          'totalDamageDealt',
                          'magicDamageDealt',
                          'physicalDamageDealt',
                          'trueDamageDealt',
                          'totalDamageDealtToChampions',
                          'magicDamageDealtToChampions',
                          'physicalDamageDealtToChampions',
                          'trueDamageDealtToChampions',
                          'totalHeal',
                          'totalUnitsHealed',
                          'visionScore',
                          'timeCCingOthers',
                          'totalDamageTaken',
                          'magicalDamageTaken',
                          'physicalDamageTaken',
                          'trueDamageTaken',
                          'goldEarned',
                          'goldSpent',
                          'champLevel',
                          'firstBloodKill',
                          'perk0',
                          'perk1',
                          'perk2',
                          'perk3',
                          'perk4',
                          'perk5',
                          'statPerk0',
                          'statPerk1',
                          'statPerk2',
                          'perkPrimaryStyle',
                          'perkSubStyle',
                          'wardsKilled',
                          'champLevel',
                          'damageDealtToObjectives',
                          'neutralMinionsKilled',
                          'firstTowerKill',
                          'wardsPlaced',
                          'totalMinionsKilled',
                          'visionWardsBoughtInGame',
                          'sightWardsBoughtInGame',
                          'turretKills',
                          'longestTimeSpentLiving',
                          'neutralMinionsKilledEnemyJungle',
                          'participantId']

    async def __call__(self, game_id, include_timeline=True):
        raw_match = await match_by_id(self.my_region, game_id)
        match_data = self.extract_match_data(raw_match)
        if match_data is not None and include_timeline:
            timeline_data = await self.timeline_dto(game_id)
            self.merge_data(match_data, timeline_data)
        return match_data

    def extract_match_data(self, match):
        game_version = self.extract_version(match)
        is_version_valid = self.check_valid_version(game_version)
        if not is_version_valid:
            raise InvalidMatchDataError('version issue')
        game_data = self.get_game_data_from_match_data(match)
        return game_data

    def merge_data(self, game_data, timeline_data) -> None:
        for key, value in timeline_data.items():
            if type(value) == defaultdict:
                game_data[key] = dict(value)
            else:
                game_data[key] = value

    def extract_version(self, match_data):
        if 'gameVersion' not in match_data:
            raise InvalidMatchDataError("gameVersion is not in match_data.")
        game_version = '.'.join(match_data['gameVersion'].split('.')[:2])
        return game_version

    def check_valid_version(self, game_version):
        if self.target_version is None:
            return True
        else:
            if game_version == self.target_version:
                return True
            else:
                return False

    def get_team_data(self, match_data: dict) -> dict:
        team_data = dict()
        team_data['win'] = dict()
        team_data['lose'] = dict()
        team_data['win']['participants'] = []
        team_data['win']['bans'] = []
        team_data['lose']['participants'] = []
        team_data['lose']['bans'] = []

        for team in match_data['teams']:
            w = 'win' if team['win'] == 'Win' else 'lose'
            team_data[w]['bans'] = [d['championId'] for d in team['bans']]
            team_id = int(team['teamId'])
            side = 'blue' if team_id == 100 else 'red'
            team_data[w]['team_id'] = int(team['teamId'])
            team_data[w]['side'] = side
            team_data[w]['first_dragon'] = team['firstDragon']
            team_data[w]['first_inhibitor'] = team['firstInhibitor']
            team_data[w]['baron_kills'] = team['baronKills']
            team_data[w]['tower_kills'] = team['towerKills']
            team_data[w]['first_rift_herald'] = team['firstRiftHerald']
            team_data[w]['first_baron'] = team['firstBaron']
            team_data[w]['rift_herald_kills'] = team['riftHeraldKills']
            team_data[w]['first_blood'] = team['firstBlood']
            team_data[w]['first_tower'] = team['firstTower']
            team_data[w]['vilemaw_kills'] = team['vilemawKills']
            team_data[w]['inhibitor_kills'] = team['inhibitorKills']
            team_data[w]['dominion_victory_score'] = team['dominionVictoryScore']
            team_data[w]['dragon_kills'] = team['dragonKills']

        win_team_id = team_data['win']['team_id']
        participants_data = self.get_participants_data(match_data, win_team_id)

        for w in ['win', 'lose']:
            for pid, vdict in participants_data[w].items():
                team_data[w]['participants'].append(vdict)

        return team_data

    def get_participants_data(self, match_data: dict, win_team_id: int) -> dict:
        win_participants = defaultdict(dict)
        lose_participants = defaultdict(dict)
        participants_data = {
            'win': win_participants,
            'lose': lose_participants
        }
        for participant in match_data['participants']:
            pid = participant['participantId']
            champion_id = participant['championId']
            lane = participant['timeline']['lane']
            w = 'win' if participant['teamId'] == win_team_id else 'lose'
            participants_data[w][pid]['championId'] = champion_id
            participants_data[w][pid]['lane'] = lane
            participants_data[w][pid]['spells'] = (participant['spell1Id'], participant['spell2Id'])
            participants_data[w][pid]['items'] = (participant['stats']['item0'],
                                                  participant['stats']['item1'],
                                                  participant['stats']['item2'],
                                                  participant['stats']['item3'],
                                                  participant['stats']['item4'],
                                                  participant['stats']['item5'],
                                                  participant['stats']['item6'])
            participants_data[w][pid]['stats'] = self.get_stats(participant['stats'])

        for pi in match_data['participantIdentities']:
            pid = pi['participantId']
            summoner_id = pi['player']['summonerId']
            account_id = pi['player']['accountId']
            summoner_name = pi['player']['summonerName']
            profile_icon = pi['player']['profileIcon']
            for w in ['win', 'lose']:
                if pid in participants_data[w]:
                    participants_data[w][pid]['summonerId'] = summoner_id
                    participants_data[w][pid]['accountId'] = account_id
                    participants_data[w][pid]['summonerName'] = summoner_name
                    participants_data[w][pid]['profileIcon'] = profile_icon

        return participants_data

    def get_game_data_from_match_data(self, match_data: dict) -> dict:
        team_data = self.get_team_data(match_data)

        game_id = match_data['gameId']
        game_mode = match_data['gameMode']
        game_type = match_data['gameType']
        timestamp = match_data['gameCreation']
        game_duration = match_data['gameDuration']
        version = '.'.join(match_data['gameVersion'].split('.')[:2])

        game_data = dict()
        game_data['game_id'] = game_id
        game_data['game_mode'] = game_mode
        game_data['game_type'] = game_type
        game_data['game_duration'] = game_duration
        game_data['timestamp'] = timestamp
        game_data['season_id'] = match_data['seasonId']
        game_data['region'] = match_data['platformId']
        game_data['version'] = version
        game_data['teams'] = team_data
        game_data['collect_timestamp'] = get_now_timestamp()

        return game_data

    def get_stats(self, stats_dict):
        ret_dict = dict()
        for k in self.stat_keys:
            if k in stats_dict:
                ret_dict[k] = stats_dict[k]
            else:
                raise InvalidMatchDataError(f"{k} is not in stats_dict.")
        return ret_dict
