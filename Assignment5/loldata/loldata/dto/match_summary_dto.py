from ..config import RiotConfig

config = RiotConfig()
my_region = config.my_region
SEASON = '11'
SEASON_ID = 14
season_dict = {13: '10',
               14: '11'}


class MatchSummaryDTO(object):

    def __init__(self):
        pass

    def __call__(self, match_data, lanes):
        match_summary = self.extract_match_summary_data(match_data, lanes)
        return match_summary

    def extract_match_summary_data(self, match_data, lanes):
        summary = dict()
        summary['game_id'] = match_data['game_id']
        summary['region'] = 'kr'  # match_data['platformId']
        summary['duration'] = match_data['game_duration']
        summary['creation'] = match_data['timestamp']
        summary['season_id'] = SEASON_ID
        summary['version'] = match_data['version']
        summary['game_mode'] = match_data['game_mode']
        summary['game_type'] = match_data['game_type']

        summary['team'] = self.get_team_summary(match_data['teams'])
        summary['participants'] = dict()

        for w in ['win', 'lose']:
            for p in match_data['teams'][w]['participants']:
                team = match_data['teams'][w]['side']
                win = True if w == 'win' else False
                # win = True
                summoner_id = p['summonerId']
                participant_id = p['stats']['participantId']
                lane = lanes[str(participant_id)]
                p_summary = self.get_summary_of_participant(p, team, win, lane)
                summary['team'][team]['info']['kills'] += p_summary['kills']
                summary['team'][team]['summoner_id_list'].append(summoner_id)
                summary['team'][team]['summoner_name_list'].append(p['summonerName'])
                # summary['team'][team]['account_id_list'].append(p['accountId'])
                summary['team'][team]['puuid_list'].append(p['puuid'])
                summary['team'][team]['champion_id_list'].append(p['championId'])
                summary['participants'][summoner_id] = dict()
                summary['participants'][summoner_id]['champion_id'] = p['championId']
                summary['participants'][summoner_id]['summoner_id'] = p['summonerId']
                # summary['participants'][summoner_id]['account_id'] = p['accountId']
                summary['participants'][summoner_id]['puuid'] = p['puuid']
                summary['participants'][summoner_id]['summoner_name'] = p['summonerName']
                summary['participants'][summoner_id]['info'] = p_summary

        for k, vs in summary['participants'].items():
            total_kills = summary['team'][vs['info']['team']]['info']['kills']
            related_kills = vs['info']['kills'] + vs['info']['assists']
            if total_kills != 0:
                percent_related_kills = related_kills / total_kills * 100
                vs['info']['percent_related_kills'] = round(percent_related_kills)
            else:
                vs['info']['percent_related_kills'] = 0
            vs['info']['cs_per_min'] = round(vs['info']['total_minions_killed'] / summary['duration'] * 60, 1)
        return summary

    # def get_participant_identities_summary(self, pi):
    #     summary = dict()
    #     summary['participant_id'] = pi['participantId']
    #     summary['region'] = 'kr' # pi['player']['platformId']
    #     summary['account_id'] = pi['player']['accountId']
    #     summary['summoner_id'] = pi['player']['summonerId']
    #     summary['summoner_name'] = pi['player']['summonerName']
    #     return summary

    def get_team_summary(self, teams):
        win_team = teams['win']['side']
        lose_team = teams['lose']['side']

        summary = dict()
        summary[win_team] = dict()
        summary[lose_team] = dict()
        summary[win_team]['win'] = True
        summary[lose_team]['win'] = False
        summary[win_team]['info'] = dict()
        summary[lose_team]['info'] = dict()
        summary[win_team]['info']['kills'] = 0
        summary[lose_team]['info']['kills'] = 0
        summary[win_team]['info']['dragon_kills'] = teams['win']['dragon_kills']
        summary[lose_team]['info']['dragon_kills'] = teams['lose']['dragon_kills']
        summary[win_team]['info']['inhibitor_kills'] = teams['win']['inhibitor_kills']
        summary[lose_team]['info']['inhibitor_kills'] = teams['lose']['inhibitor_kills']
        summary[win_team]['info']['baron_kills'] = teams['win']['baron_kills']
        summary[lose_team]['info']['baron_kills'] = teams['lose']['baron_kills']
        summary[win_team]['info']['tower_kills'] = teams['win']['tower_kills']
        summary[lose_team]['info']['tower_kills'] = teams['lose']['tower_kills']
        # summary[win_team]['info']['vilemaw_kills'] = teams['win']['vilemaw_kills']
        # summary[lose_team]['info']['vilemaw_kills'] = teams['lose']['vilemaw_kills']
        summary[win_team]['info']['rift_herald_kills'] = teams['win']['rift_herald_kills']
        summary[lose_team]['info']['rift_herald_kills'] = teams['lose']['rift_herald_kills']
        summary[win_team]['info']['bans'] = teams['win']['bans']
        summary[lose_team]['info']['bans'] = teams['lose']['bans']

        summary[lose_team]['summoner_id_list'] = []
        summary[lose_team]['summoner_name_list'] = []
        summary[lose_team]['champion_id_list'] = []
        # summary[lose_team]['account_id_list'] = []
        summary[lose_team]['puuid_list'] = []
        summary[win_team]['summoner_id_list'] = []
        summary[win_team]['summoner_name_list'] = []
        summary[win_team]['champion_id_list'] = []
        # summary[win_team]['account_id_list'] = []
        summary[win_team]['puuid_list'] = []

        return summary

    def get_summary_of_participant(self, participant, team, win, lane):
        summary = dict()
        summary['champion_id'] = participant['championId']
        summary['participant_id'] = participant['stats']['participantId']
        summary['team'] = team
        summary['win'] = win
        summary['spell1_id'] = participant['spells'][0]
        summary['spell2_id'] = participant['spells'][1]
        summary['items'] = participant['items']
        summary['kills'] = participant['stats']['kills']
        summary['deaths'] = participant['stats']['deaths']
        summary['assists'] = participant['stats']['assists']
        summary['dealt'] = participant['stats']['totalDamageDealt']
        summary['champ_level'] = participant['stats']['champLevel']
        summary['ward_placed'] = participant['stats']['wardsPlaced']
        summary['ward_killed'] = participant['stats']['wardsKilled']
        summary['vision_wards_bought_in_game'] = participant['stats']['visionWardsBoughtInGame']
        summary['main_rune1'] = participant['stats']['perk0']
        summary['main_rune2'] = participant['stats']['perk1']
        summary['main_rune3'] = participant['stats']['perk2']
        summary['main_rune4'] = participant['stats']['perk3']
        summary['sub_rune1'] = participant['stats']['perk4']
        summary['sub_rune2'] = participant['stats']['perk5']
        summary['main_rune_category'] = participant['stats']['perkPrimaryStyle']
        summary['sub_rune_category'] = participant['stats']['perkSubStyle']
        summary['statperk1'] = participant['stats']['statPerk0']
        summary['statperk2'] = participant['stats']['statPerk1']
        summary['statperk3'] = participant['stats']['statPerk2']
        summary['gold_earned'] = participant['stats']['goldEarned']
        summary['total_minions_killed'] = participant['stats']['totalMinionsKilled']
        summary['lane'] = lane
        return summary
