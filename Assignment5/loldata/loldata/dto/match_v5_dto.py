
from collections import defaultdict
# from ..riotapi.riot_watcher_wrapper_async import match_by_id_v5
from ..utils import get_now_timestamp
from ..error import InvalidMatchDataError
from .timeline_v5_dto import TimelineDTO


class MatchDTO(object):

    def __init__(self, my_region='asia', target_version=None):
        self.my_region = my_region
        self.target_version = target_version
        self.timeline_dto = TimelineDTO(my_region)
        self.stat_keys = ['kills',
                          'deaths',
                          'assists',
                          'largestMultiKill',
                          'longestTimeSpentLiving',
                          'doubleKills',
                          'tripleKills',
                          'quadraKills',
                          'pentaKills',
                          'unrealKills',
                          'totalDamageDealt',
                          'magicDamageDealt',
                          'physicalDamageDealt',
                          'trueDamageDealt',
                          'totalDamageDealtToChampions',
                          'magicDamageDealtToChampions',
                          'physicalDamageDealtToChampions',
                          'trueDamageDealtToChampions',
                          'totalHeal',
                          'totalUnitsHealed',
                          'visionScore',
                          'timeCCingOthers',
                          'totalDamageTaken',
                          'physicalDamageTaken',
                          'trueDamageTaken',
                          'goldEarned',
                          'goldSpent',
                          'champLevel',
                          'firstBloodKill',
                          'wardsKilled',
                          'champLevel',
                          'damageDealtToObjectives',
                          'neutralMinionsKilled',
                          'firstTowerKill',
                          'wardsPlaced',
                          'totalMinionsKilled',
                          'visionWardsBoughtInGame',
                          'sightWardsBoughtInGame',
                          'turretKills',
                          'longestTimeSpentLiving',
                          'participantId',
                          "spell1Casts",
                          "spell2Casts",
                          "spell3Casts",
                          "spell4Casts",
                          "summoner1Casts",
                          "summoner2Casts",
                          "teamEarlySurrendered",
                          "baronKills",
                          "bountyLevel",
                          "champExperience",
                          "consumablesPurchased",
                          "damageDealtToBuildings",
                          "detectorWardsPlaced",
                          "dragonKills",
                          "gameEndedInEarlySurrender",
                          "gameEndedInSurrender",
                          "individualPosition",
                          "inhibitorTakedowns",
                          "inhibitorsLost",
                          "itemsPurchased",
                          "nexusKills",
                          "nexusLost",
                          "nexusTakedowns",
                          "objectivesStolen",
                          "objectivesStolenAssists",
                          "teamEarlySurrendered",
                          "teamPosition",
                          "timePlayed",
                          "totalDamageShieldedOnTeammates",
                          "totalHealsOnTeammates",
                          "totalTimeCCDealt",
                          "totalTimeSpentDead",
                          "turretTakedowns",
                          "turretsLost"]

    def __call__(self, raw_match, raw_timeline=None, include_timeline=True):
        # raw_match = await match_by_id_v5(self.my_region, game_id)
        match_data = self.extract_match_data(raw_match)
        if match_data is not None and include_timeline and raw_timeline is not None:
            timeline_data = self.timeline_dto(raw_timeline)
            self.merge_data(match_data, timeline_data)
        # print("type="+str(type(match_data)))
        return match_data

    def validate_raw_match(self, raw_match):
        if 'info' not in raw_match.keys():
            # raise InvalidMatchDataError(raw_match)
            pass

    def extract_match_data(self, match):
        game_version = self.extract_version(match)
        is_version_valid = self.check_valid_version(game_version)
        if not is_version_valid:
            # raise InvalidMatchDataError('version issue')
            raise Exception('version issue')
        game_data = self.get_game_data_from_match_data(match)
        return game_data

    def merge_data(self, game_data, timeline_data) -> None:
        for key, value in timeline_data.items():
            if type(value) == defaultdict:
                game_data[key] = dict(value)
            else:
                game_data[key] = value

    def extract_version(self, match_data):
        if 'gameVersion' not in match_data:
            # raise InvalidMatchDataError("gameVersion is not in match_data.")
            raise Exception("gameVersion is not in match_data.")
        game_version = '.'.join(match_data['gameVersion'].split('.')[:2])
        return game_version

    def check_valid_version(self, game_version):
        if self.target_version is None:
            return True
        else:
            if game_version == self.target_version:
                return True
            else:
                return False

    def get_team_data(self, match_data: dict) -> dict:
        team_data = dict()
        team_data['win'] = dict()
        team_data['lose'] = dict()
        team_data['win']['participants'] = []
        team_data['win']['bans'] = []
        team_data['lose']['participants'] = []
        team_data['lose']['bans'] = []

        for team in match_data['teams']:
            w = 'win' if team['win'] == True else 'lose'
            team_data[w]['bans'] = [d['championId'] for d in team['bans']]
            team_id = int(team['teamId'])
            side = 'blue' if team_id == 100 else 'red'
            team_object = team['objectives']
            team_data[w]['team_id'] = int(team['teamId'])
            team_data[w]['side'] = side
            team_data[w]['first_dragon'] = team_object['dragon']['first']
            team_data[w]['first_inhibitor'] = team_object['inhibitor']['first']
            team_data[w]['baron_kills'] = team_object['baron']['kills']
            team_data[w]['tower_kills'] = team_object['tower']['kills']
            team_data[w]['first_rift_herald'] = team_object['riftHerald']['first']
            team_data[w]['first_baron'] = team_object['baron']['first']
            team_data[w]['rift_herald_kills'] = team_object['riftHerald']['kills']
            team_data[w]['first_blood'] = team_object['champion']['first']
            team_data[w]['first_tower'] = team_object['tower']['first']
            team_data[w]['inhibitor_kills'] = team_object['inhibitor']['kills']
            team_data[w]['dragon_kills'] = team_object['dragon']['kills']

        win_team_id = team_data['win']['team_id']
        participants_data = self.get_participants_data(match_data, win_team_id)

        for w in ['win', 'lose']:
            for pid, vdict in participants_data[w].items():
                team_data[w]['participants'].append(vdict)

        return team_data

    def get_participants_data(self, match_data: dict, win_team_id: int) -> dict:
        win_participants = defaultdict(dict)
        lose_participants = defaultdict(dict)
        participants_data = {
            'win': win_participants,
            'lose': lose_participants
        }
        for participant in match_data['participants']:
            pid = participant['participantId']
            champion_id = participant['championId']
            lane = participant['lane']
            w = 'win' if participant['teamId'] == win_team_id else 'lose'
            participants_data[w][pid]['championId'] = champion_id
            participants_data[w][pid]['lane'] = lane
            participants_data[w][pid]['spells'] = (participant['summoner1Id'], participant['summoner2Id'])
            participants_data[w][pid]['items'] = (participant['item0'],
                                                  participant['item1'],
                                                  participant['item2'],
                                                  participant['item3'],
                                                  participant['item4'],
                                                  participant['item5'],
                                                  participant['item6'])
            participants_data[w][pid]['stats'] = self.get_stats(participant)
            puuid = participant['puuid']
            summoner_id = participant['summonerId']
            summoner_name = participant['summonerName']
            summoner_level = participant['summonerLevel']
            profile_icon = participant['profileIcon']
            participants_data[w][pid]['puuid'] = puuid
            participants_data[w][pid]['summonerId'] = summoner_id
            participants_data[w][pid]['summonerName'] = summoner_name
            participants_data[w][pid]['summonerLevel'] = summoner_level
            participants_data[w][pid]['profileIcon'] = profile_icon

        return participants_data

    def get_game_data_from_match_data(self, match_data: dict) -> dict:
        team_data = self.get_team_data(match_data)

        game_id = match_data['gameId']
        game_mode = match_data['gameMode']
        game_type = match_data['gameType']
        timestamp = match_data['gameCreation']
        game_duration = match_data['gameDuration']
        version = '.'.join(match_data['gameVersion'].split('.')[:2])

        game_data = dict()
        game_data['game_id'] = game_id
        game_data['game_mode'] = game_mode
        game_data['game_type'] = game_type
        game_data['game_duration'] = int(game_duration / 1000)
        game_data['timestamp'] = timestamp
        #game_data['season_id'] = match_data['seasonId']
        game_data['region'] = match_data['platformId']
        game_data['version'] = version
        game_data['teams'] = team_data
        game_data['collect_timestamp'] = get_now_timestamp()

        return game_data

    def get_perk(self, perk):
        ret_dict = dict()
        ret_dict['perk0'] = perk['styles'][0]['selections'][0]['perk']
        ret_dict['perk1'] = perk['styles'][0]['selections'][1]['perk']
        ret_dict['perk2'] = perk['styles'][0]['selections'][2]['perk']
        ret_dict['perk3'] = perk['styles'][0]['selections'][3]['perk']
        ret_dict['perk4'] = perk['styles'][1]['selections'][0]['perk']
        ret_dict['perk5'] = perk['styles'][1]['selections'][1]['perk']
        ret_dict['statPerk0'] = perk['statPerks']['offense']
        ret_dict['statPerk1'] = perk['statPerks']['flex']
        ret_dict['statPerk2'] = perk['statPerks']['defense']
        ret_dict['perkPrimaryStyle'] = perk['styles'][0]['style']
        ret_dict['perkSubStyle'] = perk['styles'][1]['style']
        return ret_dict

    def get_stats(self, stats_dict):
        ret_dict = dict()
        for k in self.stat_keys:
            if k in stats_dict:
                ret_dict[k] = stats_dict[k]
            else:
                # raise InvalidMatchDataError(f"{k} is not in stats_dict.")
                raise Exception(f"{k} is not in stats_dict.")
        ret_dict['magicalDamageTaken'] = stats_dict['magicDamageTaken']
        perk = self.get_perk(stats_dict['perks'])
        ret_dict.update(perk)
        return ret_dict
