import time
import pytz
from datetime import datetime


def get_now_timestamp():
    return int(time.time() * 1000)


def get_now():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))


def timestamp_to_str(timestamp, locale='Asia/Seoul'):
    timezone = pytz.timezone(locale)
    ts = datetime.fromtimestamp(timestamp // 1000)
    ts = ts.astimezone(timezone)
    return ts.strftime('%Y-%m-%d %H:%M')


def get_next10am_timestamp():
    out = get_now_timestamp()
    out = out // (1000 * 24 * 60 * 60) * (1000 * 24 * 60 * 60) + 1000 * 25 * 60 * 60  # next day 10:00
    return out


def date_str_to_datetime(d):
    return datetime.date(*[int(k) for k in (d.split('-'))])


def get_info_dict_from_sql(sql, table, key_index=1, fn=lambda x: x, id_name=False):
    sql.set_cursor()
    sql.query('SELECT * FROM %s' % table)
    items = sql.cur.fetchall()
    ret_dict = dict()
    for item in items:
        key = fn(item[key_index])
        if id_name:
            ret_dict[item[0]] = key
        else:
            ret_dict[key] = item[0]
    return ret_dict


def convert_to_dict_mongodb(obj):
    result = {}
    for key, val in obj.items():
        if not isinstance(val, dict):
            result[key] = val
            continue

        for sub_key, sub_val in val.items():
            new_key = '{}.{}'.format(key, sub_key)
            result[new_key] = sub_val
            if not isinstance(sub_val, dict):
                continue

            result.update(convert_to_dict_mongodb(result))
            if new_key in result:
                del result[new_key]

    return result
