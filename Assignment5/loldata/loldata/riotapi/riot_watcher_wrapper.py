import logging
import logging.config
import urllib.parse as urlparse
import requests
import time

from ..config import MiddlewareConfig, LogConfig

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("riotapi.riot_watcher_wrapper")

config = MiddlewareConfig()
BASE_URL = config.base_url
session = requests.Session()


def get_response_from_api(suburl, retry_num=0, **kwargs):
    def _drop_none(dictionary):
        new_dict = dict()
        for k, v in dictionary.items():
            if v is not None:
                new_dict[k] = v
        return new_dict

    url = urlparse.urljoin(BASE_URL, suburl)
    params = _drop_none(kwargs)
    if retry_num >= 2:
        return {'max_retries exceeded': True, 'bad_request': True, 'url': url, 'params': params, 'status_code': 500, 'retry_num': retry_num}
    with session.get(url, params=params) as response:
        try:
            my_response = response.json()
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if e.code == 429:
                logger.warning(f'retry_after: {int(e.headers["Retry-After"])}, bad_request: False, suburl: {suburl}, params: {params}')
                time.sleep(int(e.headers['Retry-After']) + 1)
                my_response = get_response_from_api(suburl, **kwargs)
            elif e.code == 500:
                logger.warning(f'500 internal_error : {suburl}, retry_num = {retry_num}')
                my_response = get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)
            else:
                my_response = {'bad_request': True, 'url': url, 'params': params, 'status_code': e.code}
        finally:
            return my_response


def summoner_by_name(region, name, retry_num=0):
    return get_response_from_api('summoner-by-name', region=region, name=name, retry_num=0)


def summoner_by_id(region, summoner_id, retry_num=0):
    return get_response_from_api('summoner-by-id', region=region, summoner_id=summoner_id, retry_num=0)


def summoner_by_account(region, account_id, retry_num=0):
    return get_response_from_api('summoner-by-account', region=region, account_id=account_id, retry_num=0)


def matchlist_by_account(region, account_id, begin_time=None, begin_index=None, end_index=None, retry_num=0):
    return get_response_from_api('matchlist-by-account', region=region, account_id=account_id,
                                       begin_time=begin_time, begin_index=begin_index, 
                                       end_index=end_index, retry_num=0)


def match_by_id(region, match_id, retry_num=0):
    return get_response_from_api('match-by-id', region=region, match_id=match_id, retry_num=0)


def masters_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return get_response_from_api('masters-by-queue', region=region, queue=queue, retry_num=0)


def challenger_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return get_response_from_api('challenger-by-queue', region=region, queue=queue, retry_num=0)


def grandmaster_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return get_response_from_api('grandmaster-by-queue', region=region, queue=queue, retry_num=0)


def league_entries(region, tier, division, queue='RANKED_SOLO_5x5', retry_num=0):
    return get_response_from_api('league-entries', region=region, queue=queue, tier=tier, division=division, retry_num=0)


def timeline_by_match(region, match_id, retry_num=0):
    return get_response_from_api('timeline-by-match', region=region, match_id=match_id, retry_num=0)


def close_session():
    session.close()
