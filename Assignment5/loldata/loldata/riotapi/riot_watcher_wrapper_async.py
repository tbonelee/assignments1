import logging
import logging.config
import urllib.parse as urlparse
import asyncio
import aiohttp

from ..config import MiddlewareConfig, LogConfig

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("riotapi.riot_watcher_wrapper_async")

config = MiddlewareConfig()
BASE_URL = config.base_url
session = aiohttp.ClientSession()


async def get_response_from_api(suburl, retry_num=0, **kwargs):
    def _drop_none(dictionary):
        new_dict = dict()
        for k, v in dictionary.items():
            if v is not None:
                new_dict[k] = v
        return new_dict

    url = urlparse.urljoin(BASE_URL, suburl)
    params = _drop_none(kwargs)
    if retry_num >= 2:
        return {'max_retries exceeded': True, 'bad_request': True, 'url': url, 'params': params, 'status_code': 500, 'retry_num': retry_num}

    try:
        async with session.get(url, params=params) as response:
            my_response = await response.json()
            response.raise_for_status()
    except aiohttp.ClientResponseError as e:
        if e.code == 429:
            logger.info(f'retry_after: {int(e.headers["Retry-After"])}, bad_request: False, suburl: {suburl}, params: {params}')
            await asyncio.sleep(int(e.headers['Retry-After']))
            my_response = await get_response_from_api(suburl, **kwargs)
        elif e.code == 500:
            logger.info(f'500 internal_error : {suburl}, retry_num = {retry_num}')
            my_response = await get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)
        elif e.code == 502:
            logger.info(f'502 bad_gateway : {suburl}, retry_num = {retry_num}')
            my_response = await get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)
        elif e.code == 503:
            logger.info(f'503 service_unavailable : {suburl}, retry_num = {retry_num}')
            my_response = await get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)
        elif e.code == 504:
            logger.info(f'504 gateway_timeout : {suburl}, retry_num = {retry_num}')
            my_response = await get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)
        else:
            my_response = {'bad_request': True, 'url': url, 'params': params, 'status_code': e.code}

    except aiohttp.client_exceptions.ServerDisconnectedError as e:
        logger.warning(f'server_disconnected_error : {suburl}, retry_num = {retry_num}')
        my_response = await get_response_from_api(suburl, retry_num=retry_num + 1, **kwargs)

    finally:
        return my_response


async def summoner_by_name(region, name, retry_num=0):
    return await get_response_from_api('summoner-by-name', region=region, name=name, retry_num=0)


async def summoner_by_id(region, summoner_id, retry_num=0):
    return await get_response_from_api('summoner-by-id', region=region, summoner_id=summoner_id, retry_num=0)


async def summoner_by_account(region, account_id, retry_num=0):
    return await get_response_from_api('summoner-by-account', region=region, account_id=account_id, retry_num=0)


async def matchlist_by_account(region, account_id, begin_time=None, begin_index=None, end_index=None, retry_num=0):
    return await get_response_from_api('matchlist-by-account', region=region, account_id=account_id,
                                       begin_time=begin_time, begin_index=begin_index,
                                       end_index=end_index, retry_num=0)


async def matchlist_by_puuid(region, puuid, start=None, count=None, queue=None, type=None, begin_time=None, end_time=None, retry_num=0):
    return await get_response_from_api('matchlist-by-puuid', region=region, puuid=puuid, start=start, count=count, queue=queue, type=type, begin_time=begin_time, end_time=end_time, retry_num=0)


async def match_by_id(region, match_id, retry_num=0):
    return await get_response_from_api('match-by-id', region=region, match_id=match_id, retry_num=0)


async def match_by_id_v5(region, match_id, retry_num=0):
    return await get_response_from_api('match-by-id-v5', region=region, match_id=match_id, retry_num=0)


async def masters_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return await get_response_from_api('masters-by-queue', region=region, queue=queue, retry_num=0)


async def challenger_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return await get_response_from_api('challenger-by-queue', region=region, queue=queue, retry_num=0)


async def grandmaster_by_queue(region, queue='RANKED_SOLO_5x5', retry_num=0):
    return await get_response_from_api('grandmaster-by-queue', region=region, queue=queue, retry_num=0)


async def league_entries(region, tier, division, queue='RANKED_SOLO_5x5', retry_num=0):
    return await get_response_from_api('league-entries', region=region, queue=queue, tier=tier, division=division, retry_num=0)


async def timeline_by_match(region, match_id, retry_num=0):
    return await get_response_from_api('timeline-by-match', region=region, match_id=match_id, retry_num=0)


async def timeline_by_match_v5(region, match_id, retry_num=0):
    return await get_response_from_api('timeline-by-match-v5', region=region, match_id=match_id, retry_num=0)


async def close_session():
    await session.close()
