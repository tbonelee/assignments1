import configparser


class Config(object):

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        self.initialize()

    def initialize(self):
        raise NotImplementedError()


class RiotConfig(Config):

    def __init__(self):
        super().__init__()

    def initialize(self):
        self.config.get('RIOT', 'API_KEY')
        self.config.get('RIOT', 'MY_REGION')
        self.api_key = self.config['RIOT']['API_KEY']
        self.my_region = self.config['RIOT']['MY_REGION']


class LogConfig():

    def __init__(self):
        self.config = {
            "version": 1,
            "formatters": {
                "simple": {"format": "%(asctime)s [%(name)s] - %(message)s"},
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "simple",
                    "level": "DEBUG",
                },
            },
            "root": {"handlers": ["console"], "level": "DEBUG"},
            "loggers": {
                "stacker": {"level": "INFO"},
                "dto.summoner_dto": {"level": "WARNING"},
                "puuid_updater": {"level": "INFO"},
                "riotapi.riot_watcher_wrapper": {"level": "WARNING"},
                "riotapi.riot_watcher_wrapper_async": {"level": "WARNING"},
                "craftsman": {"level": "INFO"},
                "craft_feature": {"level": "INFO"},
                "statistics": {"level": "INFO"},
            },
        }


class DBConfig(Config):

    def __init__(self):
        super().__init__()


class PSQLConfig(DBConfig):

    def __init__(self, db_name='lolps_sync'):
        self.db_name = db_name
        super().__init__()

    def initialize(self):
        if self.db_name == 'lolps_sync':
            ini_category = 'PSQL_LOLPS_SYNC'
        elif self.db_name == 'lolps':
            ini_category = 'PSQL_LOLPS'
        elif self.db_name == 'psweb':
            ini_category = 'PSQL_PSWEB'
        self.config.get(ini_category, 'HOST')
        self.config.get(ini_category, 'USER')
        self.config.get(ini_category, 'PASSWORD')
        self.config.get(ini_category, 'DATABASE')
        self.config.get(ini_category, 'PORT')

        self.config_dict = {
            'host': self.config[ini_category]['HOST'],
            'user': self.config[ini_category]['USER'],
            'password': self.config[ini_category]['PASSWORD'],
            'database': self.config[ini_category]['DATABASE'],
            'port': self.config[ini_category]['PORT'],
        }


class MongoDBConfig(DBConfig):

    def __init__(self):
        super().__init__()

    def initialize(self):
        ini_category = 'MONGODB'
        self.config.get(ini_category, 'LOCAL_IP')
        self.config.get(ini_category, 'USER')
        self.config.get(ini_category, 'PASSWORD')
        self.config.get(ini_category, 'PORT')

        self.config_dict = {
            'local_ip': self.config[ini_category]['LOCAL_IP'],
            'user': self.config[ini_category]['USER'],
            'password': self.config[ini_category]['PASSWORD'],
            'port': self.config[ini_category]['PORT'],
        }


class MiddlewareConfig(Config):

    def __init__(self):
        super().__init__()

    def initialize(self):
        self.config.get('MIDDLEWARE', 'BASE_URL')
        self.base_url = self.config['MIDDLEWARE']['BASE_URL']
