import logging
import logging.config

from .updating_functions_sync import *
from .updating_functions_lolps import *
from ..db_handler.psql_handler import PSQLHandler
from ..config import LogConfig
from ..utils import get_info_dict_from_sql

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("statistics")

sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()

sql_real = PSQLHandler(connect=False, db_name='lolps')
sql_real.connect()

version_dict = get_info_dict_from_sql(sql_sync, 'version_info')

region_id = 0
version_id = sorted(list(version_dict.values()))[-1]


def main():
    update_spell_win_rate_table(sql_sync, region_id, version_id)
    update_statperk_win_rate_table(sql_sync, region_id, version_id)
    update_rune_total_win_rate_table(sql_sync, region_id, version_id)
    logger.info('update_rune_total_win_rate_table finished')

    update_spell(sql_sync, sql_real, region_id, version_id)
    update_statperk(sql_sync, sql_real, region_id, version_id)
    update_rune_total(sql_sync, sql_real, region_id, version_id)
    logger.info('update_rune_total finished')
    
    sql_sync.connection.close()
    sql_real.connection.close()


if __name__=='__main__':
    logger.info('update_sync_and_lolps_db9 start')
    main()
    logger.info('update_sync_and_lolps_db9 done')

