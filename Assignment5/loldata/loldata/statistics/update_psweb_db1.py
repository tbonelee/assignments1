import logging
import logging.config

from .updating_functions_psweb import *
from ..db_handler.psql_handler import PSQLHandler
from ..config import LogConfig
from ..utils import get_now, get_info_dict_from_sql

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("statistics")

sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()

sql_real = PSQLHandler(connect=False, db_name='psweb')
sql_real.connect()

version_dict = get_info_dict_from_sql(sql_sync, 'version_info')
# region_dict = get_info_dict_from_sql(sql_sync, 'region_info')
# lane_dict = get_info_dict_from_sql(sql_sync, 'lane_info', key_index=2)
# tier_dict = get_info_dict_from_sql(sql_sync, 'tier_info')
date_dict = get_info_dict_from_sql(sql_sync, 'date_info')
# champion_dict = get_info_dict_from_sql(sql_sync, 'champion_info')

region_id = 0
version_id = sorted(list(version_dict.values()))[-1]
version = {v:k for k, v in version_dict.items()}[version_id]
date_id = sorted(list(date_dict.values()))[-1]
date = {v:k for k, v in date_dict.items()}[date_id]


def update_version_info(sql_real, version_id, last_version, date):
    version_dict_real = get_info_dict_from_sql(sql_real, 'version_info')
    if version_id not in list(version_dict_real.values()):
        sql_real.set_cursor()
        sql_real.insert('version_info',
                   '(version_id, description, patch_date, updated_at)',
                   (version_id, last_version, date, get_now()))
        sql_real.commit()
        return True
    else:
        return False


def delete_version_from_table(sql, version_id_for_delete, table):
    sql.set_cursor()
    sql.delete(table, condition='version_id = %d'%version_id_for_delete)
    sql.commit()


def delete_former_version_data(sql, last_version_id):
    version_id_for_delete = last_version_id - 4

    table_name_list = ['lane_version_winrate', 'counter_winrate', 'lane_version_timeline_winrate', 
        'duo_synergy_winrate', 'item_1st_core_winrate', 'item_2nd_core_winrate', 'item_3rd_core_winrate', 
        'item_shoes_winrate', 'item_starting_winrate', 'item_three_core_winrate', 'item_total_core_winrate',
        'rune_main1_winrate', 'rune_main2_winrate', 'rune_main3_winrate', 'rune_main4_winrate',
        'rune_sub1_winrate', 'rune_sub2_winrate', 'rune_total_winrate', 'statperk_total_winrate', 
        'skill_lv1_winrate', 'skill_lv3_winrate', 'skill_lv6_winrate', 'skill_lv11_winrate', 
        'skill_lv15_winrate', 'skill_master_winrate', 'spell_winrate']

    for table_name in table_name_list:
        delete_version_from_table(sql, version_id_for_delete, table_name)



def update_date_info(sql_real, date_id, last_date):
    date_dict_real = get_info_dict_from_sql(sql_real, 'date_info')
    if date_id not in list(date_dict_real.values()):
        sql_real.set_cursor()
        sql_real.insert('date_info',
                   '(date_id, date)',
                   (date_id, last_date))
        sql_real.commit()
        return True
    else:
        return False


def delete_former_date_data(sql, last_date_id):
    date_id_for_delete = last_date_id - 14
    delete_former_date_from_table(sql, date_id_for_delete, 'lane_date_winrate')


def delete_former_date_from_table(sql, date_id_for_delete, table):
    sql.set_cursor()
    sql.delete(table, condition='date_id = %d'%date_id_for_delete)
    sql.commit()


def main():

    is_updated = update_version_info(sql_real, version_id, version, date)
    if is_updated:
        delete_former_version_data(sql_real, version_id)

    is_updated = update_date_info(sql_real, date_id, date)
    if is_updated:
        delete_former_date_data(sql_real, date_id)

    update_counter(sql_sync, sql_real, region_id, version_id)
    logger.info('update_counter finished')
    update_lane_version(sql_sync, sql_real, region_id, version_id)
    logger.info('update_lane_version finished')
    update_lane_date(sql_sync, sql_real, region_id, version_id, date_id)
    logger.info('update_lane_date finished')
    #update_lane_date(sql_sync, sql_real, region_id, version_id, date_id-1)
    update_champion_summary(sql_sync, sql_real, region_id, version_id)
    logger.info('update_champion_summary finished')
    sql_sync.connection.close()
    sql_real.connection.close()

if __name__=='__main__':
    logger.info('update_psweb_db1 start')
    main()
    logger.info('update_psweb_db1 done')

