import logging
import logging.config

from .updating_functions_psweb import *
from ..db_handler.psql_handler import PSQLHandler
from ..config import LogConfig
from ..utils import get_info_dict_from_sql

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("statistics")


sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()

sql_real = PSQLHandler(connect=False, db_name='psweb')
sql_real.connect()

version_dict = get_info_dict_from_sql(sql_sync, 'version_info')

region_id = 0
version_id = sorted(list(version_dict.values()))[-1]


def main():
    
    update_item_1st_core(sql_sync, sql_real, region_id, version_id)
    update_item_2nd_core(sql_sync, sql_real, region_id, version_id)
    update_item_3rd_core(sql_sync, sql_real, region_id, version_id)
    update_item_two_core(sql_sync, sql_real, region_id, version_id)
    update_item_three_core(sql_sync, sql_real, region_id, version_id)
    logger.info('update_item_three_core finished')

    sql_sync.connection.close()
    sql_real.connection.close()


if __name__=='__main__':
    logger.info('update_psweb_db5 start')
    main()
    logger.info('update_psweb_db5 done')

