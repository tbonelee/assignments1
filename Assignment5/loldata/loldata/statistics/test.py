from pydantic import BaseModel
from typing import TypedDict, NamedTuple, Dict, List

class Hi(NamedTuple):
	a:int
	b:int

class Hi2(BaseModel):
	a:int=0
	b:int=0
	c:int=0


class Hi3(Dict[str, int]):
	pass

if __name__ == "__main__":
	tt = Hi(a=1, b=2)
	ttt = Hi2(a=1,b=2,c=3)
	#ttt= Hi2(*tt, c=3)
	# a, b = tt
	a, b, c = ttt
	print(tuple(ttt.dict().values()))
	# print('a:%s, b:%s'%(a, b))
	print('a:%s, b:%s, c:%s'%tuple(ttt.dict().values()))
	print(ttt.a, ttt.b, ttt.c)
	#print(Hi2(1,2))
	
	aa = Hi2()
	aa.a = 1
	#aa.a, aa.b, aa.c = 3,4,5
	aa.__setattr__('b', 5)
	print(aa)
	print(tt.a)