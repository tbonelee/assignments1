from collections import defaultdict
import numpy as np
from sklearn.mixture import GaussianMixture
from ..db_handler.psql_handler import PSQLHandler
from .ps_score_config import PSScoreConfig, HoneyScoreConfig, NumOfGMMClusters, Tier
from pydantic import BaseModel
from typing import NamedTuple, TypedDict, Dict, List


sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()


def get_info_dict_from_sql(sql, table, key_index=1, fn=lambda x: x, id_name=False):
    sql.set_cursor()
    sql.query('SELECT * FROM %s' % table)
    items = sql.cur.fetchall()
    ret_dict = dict()
    for item in items:
        key = fn(item[key_index])
        if id_name:
            ret_dict[item[0]] = key
        else:
            ret_dict[key] = item[0]
    return ret_dict


champion_dict = get_info_dict_from_sql(sql_sync, 'champion_info')
TOTAL_NUM_OF_CHAMPIONS = len(champion_dict)


class RVTL(NamedTuple):
    region_id: int
    version_id: int
    tier_id: int
    lane_id: int


class RVTLC(NamedTuple):
    region_id: int
    version_id: int
    tier_id: int
    lane_id: int
    champion_id: int


class Feature(BaseModel):
    champion_id: int
    count: int
    win_rate: float
    pick_rate: float
    ban_rate: float
    pickban_rate: float
    counter_score: float
    pickban_rate_diff: float = 0
    win_rate_diff: float = 0
    pick_rate_diff: float = 0


class LaneVersionWinrateColumns(BaseModel):
    count: int = 0
    pick_rate: float = 0
    ban_rate: float = 0
    win_rate: float = 0
    op_score: float = 0
    op_tier: float = 6
    is_honey: bool = False
    is_op: bool = False
    counter_score: float = 0
    revising_score: float = 0
    ranking: int = 0
    ranking_variation: int = 0
    honey_score: float = 0
    last_ranking: int = TOTAL_NUM_OF_CHAMPIONS
    ban_rate_considering_ratio: float = 0
    pbi: float = -100
    z_win_rate: float = -100
    z_pick_rate: float = -100
    z_ban_rate: float = -100
    z_pickban_rate: float = -100
    z_count: float = -100
    z_counter_score: float = -100
    z_pickban_rate_diff: float = 0
    z_win_rate_diff: float = 0
    z_pick_rate_diff: float = 0
    overall_ranking: int = TOTAL_NUM_OF_CHAMPIONS * 5
    overall_ranking_variation: int = 0
    last_overall_ranking: int = TOTAL_NUM_OF_CHAMPIONS * 5


def get_ps_tier(ps_score_list):
    x = ps_score_list
    gm = GaussianMixture(NumOfGMMClusters)
    x = np.array(x).reshape(-1, 1)
    out = gm.fit(x)
    ys = defaultdict(list)
    for xx, y in zip(x, out.predict(x)):
        ys[y].append(xx[0])

    mean_dict = defaultdict(float)
    for k, vs in ys.items():
        mean_dict[k] = np.min(vs)

    sorted_keys = sorted(mean_dict.items(), key=lambda x: x[1], reverse=True)

    sep_line_tier1and2 = np.mean(sorted(x, reverse=True)[:int(len(x) * 0.2)])  # 상위 20% 의 평균 ps_score 를 1,2티어의 구분선으로 사용
    sep_line_tier2and3 = np.mean(sorted(x, reverse=True)[int(len(x) * 0.2):int(len(x) * 0.4)])
    sep_line_tier3and4 = np.mean(sorted(x, reverse=True)[int(len(x) * 0.4):int(len(x) * 0.8)])
    sep_line_tier4and5 = np.mean(sorted(x, reverse=True)[int(len(x) * 0.8):int(len(x) * 1)])
    key_dict = dict()
    tier1_count = 0
    for i, (k, v) in enumerate(sorted_keys):
        if v > sep_line_tier1and2:
            tier = 0
            tier1_count += 1
        elif sep_line_tier1and2 >= v > sep_line_tier2and3:
            tier = 1
        elif sep_line_tier2and3 >= v > sep_line_tier3and4:
            tier = 2
        elif sep_line_tier3and4 >= v > sep_line_tier4and5:
            tier = 3
        else:
            tier = 4
        key_dict[k] = tier
    if tier1_count == 0:
        key_dict[sorted_keys[0][0]] = 0
    y_gmm = []
    for y in out.predict(x):
        y_gmm.append(key_dict[y] + 1)

    return y_gmm


def get_basic_info_dict(sql, region_id, version_id) -> Dict[RVTLC, LaneVersionWinrateColumns]:

    def __update_ban_rate_considering_ratio(info_dict) -> Dict[RVTLC, LaneVersionWinrateColumns]:
        ratio_dict = defaultdict(lambda: np.zeros(5))
        for rvtlc, v in info_dict.items():
            ratio_dict[(rvtlc.region_id, rvtlc.version_id, rvtlc.tier_id, rvtlc.champion_id)][rvtlc.lane_id] = v.pick_rate

        for (region_id, version_id, tier_id, champion_id), v in ratio_dict.items():
            v = v / (np.sum(v) + 1e-10)
            for lane_id in range(5):
                ratio = v[lane_id]
                if ratio == 0:
                    continue
                ban_rate = info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].ban_rate
                info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].ban_rate_considering_ratio = round(ban_rate * ratio, 2)
        return info_dict

    info_dict = defaultdict()
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, lane_id, champion_id, win_rate, pick_rate, ban_rate, count \
				 FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d' % (region_id, version_id))
    for region_id, version_id, tier_id, lane_id, champion_id, win_rate, pick_rate, ban_rate, count in sql.cur.fetchall():
        info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)] = LaneVersionWinrateColumns(
            win_rate=win_rate, pick_rate=pick_rate, ban_rate=ban_rate, count=count)
    sql.cur.close()

    info_dict = __update_ban_rate_considering_ratio(info_dict)

    return info_dict


def update_counter_score(sql, region_id, version_id, info_dict, pickrate_threshold=0.5) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    sql.set_cursor()
    sql.query('SELECT champion_id, lane_id, region_id, tier_id, version_id, counter_winrate_list, counter_count_list FROM counter_winrate WHERE region_id = %d and version_id = %d' % (region_id, version_id))
    for champion_id, lane_id, region_id, tier_id, version_id, winrate_list, count_list in sql.cur.fetchall():
        if len(count_list) > 2:
            winrate_arr = np.array(eval(winrate_list))
            count_arr = np.array(eval(count_list))
            total_score = (winrate_arr * count_arr).sum()
            total_count = count_arr.sum()
            winrate_std = winrate_arr.std()
            counter_score = total_score / total_count - winrate_std
        else:
            counter_score = 0
        if info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].pick_rate > pickrate_threshold:
            info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].counter_score = round(counter_score, 2)
    sql.cur.close()
    return info_dict


def update_pbi(info_dict) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    winrate_list_dict: Dict[RVTL, List[float]] = defaultdict(list)
    count_list_dict: Dict[RVTL, List[int]] = defaultdict(list)
    avg_winrate_dict: Dict[RVTL, float] = dict()
    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        winrate_list_dict[RVTL(region_id, version_id, tier_id, lane_id)].append(v.win_rate)
        count_list_dict[RVTL(region_id, version_id, tier_id, lane_id)].append(v.count)

    for rvtl in winrate_list_dict.keys():
        winrate_arr = np.array(winrate_list_dict[rvtl])
        count_arr = np.array(count_list_dict[rvtl])
        avg_winrate_dict[rvtl] = np.sum(winrate_arr * count_arr) / np.sum(count_arr)

    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        avg_winrate = avg_winrate_dict[RVTL(region_id, version_id, tier_id, lane_id)]
        pbi = ((v.win_rate - avg_winrate) * v.pick_rate * 100) / (100 - v.ban_rate_considering_ratio)
        v.pbi = pbi
    return info_dict


def normalize(arr):
    def __get_mean_and_std(arr):
        mean = np.mean(arr)
        std = np.std(arr)
        return mean, std

    arr = np.array(arr)
    mean, std = __get_mean_and_std(arr)
    if std == 0.0:
        return (arr - mean)
    else:
        return (arr - mean) / std


def update_z_scores(info_dict, pickrate_threshold=0.5) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    def banrate_fn(b):
        max_banrate = 25
        b = np.clip(b, 0, max_banrate)
        return 100 * 0.1 * (np.log(b + 5) - np.log(5))

    def pickrate_fn(b):
        return 100 * 0.2 * (np.log(b + 15) - np.log(15))

    diff_clip_min = -6
    diff_clip_max = 6
    features_dict = defaultdict(list)
    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        if v.pick_rate > pickrate_threshold:
            feature = Feature(champion_id=champion_id, count=v.count, win_rate=v.win_rate, pick_rate=pickrate_fn(v.pick_rate), ban_rate=banrate_fn(v.ban_rate_considering_ratio), pickban_rate=v.pick_rate + v.ban_rate_considering_ratio, counter_score=v.counter_score)
            features_dict[RVTL(region_id, version_id, tier_id, lane_id)].append(feature)

            if PSScoreConfig.tier_revise_dict[tier_id]:
                tier_id_for_diff = PSScoreConfig.tier_for_diff_dict[tier_id]
                key = RVTLC(region_id, version_id, tier_id_for_diff, lane_id, champion_id)
                if key not in info_dict:
                    continue
                diff_info = info_dict[key]
                if diff_info.pick_rate > pickrate_threshold:
                    feature.pick_rate_diff = np.clip(diff_info.pick_rate - v.pick_rate, diff_clip_min, diff_clip_max)
                    feature.win_rate_diff = np.clip(diff_info.win_rate - v.win_rate, diff_clip_min, diff_clip_max)
                    feature.pickban_rate_diff = np.clip((diff_info.pick_rate + diff_info.ban_rate_considering_ratio) - (v.pick_rate + v.ban_rate_considering_ratio), diff_clip_min, diff_clip_max)

    for (region_id, version_id, tier_id, lane_id), vs in features_dict.items():
        for key in ['win_rate', 'pick_rate', 'ban_rate', 'pickban_rate', 'count', 'counter_score', 'pick_rate_diff', 'pickban_rate_diff', 'win_rate_diff']:
            arr = [feature.__getattribute__(key) for feature in vs]
            arr = normalize(arr)
            for col, feature in zip(arr, vs):
                champion_id = feature.champion_id
                info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].__setattr__('z_' + key, col)

    return info_dict


def update_ps_scores(info_dict, pickrate_threshold=0.5, offset=50):

    # calculate ps_scores
    ps_scores_dict: Dict[RVTL] = defaultdict(list)
    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        if not v.pick_rate > pickrate_threshold:
            continue
        weights = PSScoreConfig.tier_weights_dict[tier_id]
        revise = PSScoreConfig.tier_revise_dict[tier_id]
        revise_weight = PSScoreConfig.tier_revise_weight_dict[tier_id]
        cut = PSScoreConfig.tier_cut_dict[tier_id]

        op_score = weights["winrate"] * v.z_win_rate + \
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         weights["pickrate"] * v.z_pick_rate + \
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         weights["banrate"] * v.z_ban_rate + \
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         weights["count"] * v.z_count + \
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         weights["counter"] * v.z_counter_score

        # 승률이 백분위점수 50 보다 낮으면 패널티
        if v.z_win_rate < 0:
            op_score = op_score + v.z_win_rate * 2

        # 천상계 지표 보정
        if revise:
            if cut:
                revising_score = min(v.z_pickban_rate_diff, 0) * revise_weight['pickbanrate'] + \
                    min(v.z_win_rate_diff, 0) * revise_weight['winrate'] + \
                    min(v.z_pick_rate_diff, 0) * revise_weight['pickrate']
            else:
                revising_score = v.z_pickban_rate_diff * revise_weight['pickbanrate'] + \
                    v.z_win_rate_diff * revise_weight['winrate'] + \
                    v.z_pick_rate_diff * revise_weight['pickrate']
            v.revising_score = revising_score
            op_score = op_score + revising_score

        v.op_score = op_score
        ps_scores_dict[RVTL(region_id, version_id, tier_id, lane_id)].append((champion_id, op_score))

    # ranking ps_scores
    ps_tier_dict: Dict[RVTL] = dict()
    ranking_dict: Dict[RVTL] = dict()
    for rvtl, vs in ps_scores_dict.items():
        sorted_list = sorted(vs, key=lambda x: x[1], reverse=True)
        champion_id_list = [champion_id for champion_id, ps_scores in sorted_list]
        ps_score_list = [ps_scores for champion_id, ps_scores in sorted_list]
        ps_tier_list = get_ps_tier(ps_score_list)
        ps_tier_dict[rvtl] = {champion_id: ps_tier for champion_id, ps_tier in zip(champion_id_list, ps_tier_list)}
        ranking_dict[rvtl] = {champion_id: i + 1 for i, (champion_id, ps_scores) in enumerate(sorted_list)}

    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        if champion_id in ps_tier_dict[RVTL(region_id, version_id, tier_id, lane_id)]:
            v.op_tier = ps_tier_dict[RVTL(region_id, version_id, tier_id, lane_id)][champion_id]
            v.ranking = ranking_dict[RVTL(region_id, version_id, tier_id, lane_id)][champion_id]
            op_criterion = PSScoreConfig.op_criterion_dict[lane_id]
            if v.op_score > op_criterion and v.op_tier == 1:
                v.is_op = True

            v.op_score = v.op_score + offset

    return info_dict


def update_honey_scores(info_dict, pickrate_threshold=0.5):
    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        if not v.pick_rate > pickrate_threshold:
            continue
        v.honey_score = v.pbi
        v.is_honey = v.honey_score > HoneyScoreConfig.tier_threshold_dict[tier_id]
    return info_dict


def update_last_ranking(sql, region_id, version_id, info_dict: Dict[RVTLC, LaneVersionWinrateColumns]) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    sql.set_cursor()
    query = 'SELECT tier_id, lane_id, champion_id, ranking FROM lane_version_winrate WHERE region_id=%s and version_id=%s'
    params = (region_id, version_id - 1)
    sql.query(query, params)
    for tier_id, lane_id, champion_id, last_ranking in sql.cur.fetchall():
        key = RVTLC(region_id, version_id, tier_id, lane_id, champion_id)
        if key in info_dict:
            info_dict[key].last_ranking = last_ranking

    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        last_ranking = v.last_ranking
        now_ranking = v.ranking
        info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].ranking_variation = last_ranking - now_ranking
    sql.cur.close()
    return info_dict


def update_overall_ranking(info_dict: Dict[RVTLC, LaneVersionWinrateColumns], pickrate_threshold=0.5) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    overall_ranking_dict = defaultdict(list)
    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        if not v.pick_rate > pickrate_threshold:
            continue
        overall_ranking_dict[(region_id, version_id, tier_id)].append((lane_id, champion_id, v.op_score))

    for (region_id, version_id, tier_id), v in overall_ranking_dict.items():
        for i, (lane_id, champion_id, ps_score) in enumerate(sorted(v, key=lambda x: x[2], reverse=True)):
            info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].overall_ranking = i + 1
    return info_dict


def update_last_overall_ranking(sql, region_id, version_id, info_dict: Dict[RVTLC, LaneVersionWinrateColumns]) -> Dict[RVTLC, LaneVersionWinrateColumns]:
    sql.set_cursor()
    query = 'SELECT tier_id, lane_id, champion_id, overall_ranking FROM lane_version_winrate WHERE region_id=%s and version_id=%s'
    params = (region_id, version_id - 1)
    sql.query(query, params)
    for tier_id, lane_id, champion_id, last_overall_ranking in sql.cur.fetchall():
        info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].last_overall_ranking = last_overall_ranking

    for (region_id, version_id, tier_id, lane_id, champion_id), v in info_dict.items():
        last_overall_ranking = v.last_overall_ranking
        now_overall_ranking = v.overall_ranking
        info_dict[RVTLC(region_id, version_id, tier_id, lane_id, champion_id)].overall_ranking_variation = last_overall_ranking - now_overall_ranking
    sql.cur.close()
    return info_dict


def get_ps_score_dict(sql, region_id, version_id):
    basic_info_dict = get_basic_info_dict(sql, region_id, version_id)
    basic_info_dict = update_counter_score(sql, region_id, version_id, basic_info_dict)
    basic_info_dict = update_pbi(basic_info_dict)
    basic_info_dict = update_z_scores(basic_info_dict)
    basic_info_dict = update_ps_scores(basic_info_dict)
    basic_info_dict = update_honey_scores(basic_info_dict)
    basic_info_dict = update_last_ranking(sql, region_id, version_id, basic_info_dict)
    basic_info_dict = update_overall_ranking(basic_info_dict)
    # basic_info_dict = update_last_overall_ranking(basic_info_dict)

    return basic_info_dict


if __name__ == "__main__":
    sql = sql_sync
    region_id = 0
    version_id = 27
    ps_score_dict = get_ps_score_dict(sql, region_id, version_id)
    print(ps_score_dict)
