from ..utils import get_now


def update_item_shoes(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate \
        FROM item_shoes_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_shoes_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_shoes_winrate_id_seq', (SELECT MAX(id) FROM item_shoes_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_shoes_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_starting(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
        FROM item_starting_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_starting_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_starting_winrate_id_seq', (SELECT MAX(id) FROM item_starting_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_starting_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_two_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
        FROM item_two_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_two_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_two_core_winrate_id_seq', (SELECT MAX(id) FROM item_two_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_two_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_item_three_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
        FROM item_three_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_three_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_three_core_winrate_id_seq', (SELECT MAX(id) FROM item_three_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_three_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_item_four_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
        FROM item_four_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_four_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_four_core_winrate_id_seq', (SELECT MAX(id) FROM item_four_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_four_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_item_five_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
        FROM item_five_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_five_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_five_core_winrate_id_seq', (SELECT MAX(id) FROM item_five_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_five_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_1st_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
        FROM item_1st_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_1st_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_1st_core_winrate_id_seq', (SELECT MAX(id) FROM item_1st_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_1st_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_2nd_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
        FROM item_2nd_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_2nd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_2nd_core_winrate_id_seq', (SELECT MAX(id) FROM item_2nd_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_2nd_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_3rd_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
        FROM item_3rd_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_3rd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_3rd_core_winrate_id_seq', (SELECT MAX(id) FROM item_3rd_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_3rd_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_4th_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
        FROM item_4th_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_4th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_4th_core_winrate_id_seq', (SELECT MAX(id) FROM item_4th_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_4th_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_item_5th_core(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
        FROM item_5th_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('item_5th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('item_5th_core_winrate_id_seq', (SELECT MAX(id) FROM item_5th_core_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('item_5th_core_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_lane_date(sql_sync, sql_real, region, version, date):
    sql_sync.set_cursor()
    sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, date_id, counter_score \
        FROM lane_date_winrate WHERE region_id = %d and version_id = %d and date_id = %d;'%(region, version, date))

    sql_real.set_cursor()
    sql_real.delete('lane_date_winrate', condition='version_id = %d and region_id = %d and date_id = %d'%(version, region, date))
    sql_real.query("SELECT setval('lane_date_winrate_id_seq', (SELECT MAX(id) FROM lane_date_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('lane_date_winrate',
                  '(champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, date_id, counter_score, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_lane_version_timeline(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, timeline_winrate_list \
        FROM lane_version_timeline_winrate WHERE region_id = %d and version_id = %d;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('lane_version_timeline_winrate', condition='version_id = %d and region_id = %d'%(version, region))
    sql_real.query("SELECT setval('lane_version_timeline_winrate_id_seq', (SELECT MAX(id) FROM lane_version_timeline_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('lane_version_timeline_winrate',
                  '(champion_id, lane_id, version_id, region_id, tier_id, timeline_winrate_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_lane_version(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, op_tier, ranking, ranking_variation, is_honey, is_op, counter_score, honey_score, revising_score, \
        overall_ranking, overall_ranking_variation\
        FROM lane_version_winrate WHERE region_id = %d and version_id = %d;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('lane_version_winrate', condition='version_id = %d and region_id = %d'%(version, region))
    sql_real.query("SELECT setval('lane_version_winrate_id_seq', (SELECT MAX(id) FROM lane_version_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('lane_version_winrate',
                    '(champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, op_tier, ranking, ranking_variation, is_honey, is_op, counter_score, honey_score, revising_score, \
                        overall_ranking, overall_ranking_variation, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_rune_main1(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_main1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_main1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_main1_winrate_id_seq', (SELECT MAX(id) FROM rune_main1_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_main1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_rune_main2(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_main2_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_main2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_main2_winrate_id_seq', (SELECT MAX(id) FROM rune_main2_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_main2_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_rune_main3(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_main3_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_main3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_main3_winrate_id_seq', (SELECT MAX(id) FROM rune_main3_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_main3_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_rune_main4(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_main4_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_main4_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_main4_winrate_id_seq', (SELECT MAX(id) FROM rune_main4_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_main4_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_rune_sub1(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_sub1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_sub1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_sub1_winrate_id_seq', (SELECT MAX(id) FROM rune_sub1_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_sub1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_rune_sub2(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
        FROM rune_sub2_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_sub2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_sub2_winrate_id_seq', (SELECT MAX(id) FROM rune_sub2_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_sub2_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_rune_total(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, count, pick_rate, win_rate, category1_rune_id_list, category2_rune_id_list \
        FROM rune_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('rune_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('rune_total_winrate_id_seq', (SELECT MAX(id) FROM rune_total_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('rune_total_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, count, pick_rate, win_rate, category1_rune_id_list, category2_rune_id_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_lv1(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_lv1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_lv1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_lv1_winrate_id_seq', (SELECT MAX(id) FROM skill_lv1_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_lv1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_lv3(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_lv3_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_lv3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_lv3_winrate_id_seq', (SELECT MAX(id) FROM skill_lv3_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_lv3_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_lv6(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_lv6_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_lv6_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_lv6_winrate_id_seq', (SELECT MAX(id) FROM skill_lv6_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_lv6_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_lv11(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_lv11_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_lv11_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_lv11_winrate_id_seq', (SELECT MAX(id) FROM skill_lv11_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_lv11_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_lv15(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_lv15_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_lv15_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_lv15_winrate_id_seq', (SELECT MAX(id) FROM skill_lv15_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_lv15_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_skill_master(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
        FROM skill_master_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('skill_master_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('skill_master_winrate_id_seq', (SELECT MAX(id) FROM skill_master_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('skill_master_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_spell(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, spell1_id, spell2_id \
        FROM spell_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('spell_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('spell_winrate_id_seq', (SELECT MAX(id) FROM spell_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('spell_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, spell1_id, spell2_id, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_statperk(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list \
        FROM statperk_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('statperk_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('statperk_total_winrate_id_seq', (SELECT MAX(id) FROM statperk_total_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('statperk_total_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()


def update_counter(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, counter_champion_id_list, counter_winrate_list, counter_count_list, counter_pickrate_list \
        FROM counter_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('counter_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('counter_winrate_id_seq', (SELECT MAX(id) FROM counter_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('counter_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, counter_champion_id_list, counter_winrate_list, counter_count_list, counter_pickrate_list, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

def update_duo(sql_sync, sql_real, region, version):
    sql_sync.set_cursor()
    sql_sync.query('SELECT region_id, version_id, tier_id, duo_id, champion_id1, champion_id2, synergy_score, winrate1, winrate2, duo_winrate, count, pickrate, pickrate1, pickrate2 \
        FROM duo_synergy_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    sql_real.set_cursor()
    sql_real.delete('duo_synergy_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql_real.query("SELECT setval('duo_synergy_winrate_id_seq', (SELECT MAX(id) FROM duo_synergy_winrate)+1)")

    for item in sql_sync.cur.fetchall():
        sql_real.insert('duo_synergy_winrate',
                  '(region_id, version_id, tier_id, duo_id, champion_id1, champion_id2, synergy_score, winrate1, winrate2, duo_winrate, count, pickrate, pickrate1, pickrate2, updated_at)',
                    (*item, get_now()))
    sql_real.commit()
    sql_sync.cur.close()
    sql_real.cur.close()

