from collections import defaultdict
import numpy as np
from sklearn.mixture import GaussianMixture
from ..db_handler.psql_handler import PSQLHandler


def get_info_dict_from_sql(sql, table, key_index=1, fn=lambda x:x, id_name=False):
    sql.set_cursor()
    sql.query('SELECT * FROM %s'%table)
    items = sql.cur.fetchall()
    ret_dict = dict()
    for item in items:
        key = fn(item[key_index])
        if id_name:
            ret_dict[item[0]] = key
        else:
            ret_dict[key] = item[0]
    return ret_dict

sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()

champion_dict = get_info_dict_from_sql(sql_sync, 'champion_info')
TOTAL_NUM_OF_CHAMPIONS = len(champion_dict)


def get_ps_tier(ps_score_list):
    x = ps_score_list
    gm = GaussianMixture(15)
    x = np.array(x).reshape(-1,1)
    out = gm.fit(x)
    ys = defaultdict(list)
    for xx, y in zip(x, out.predict(x)):
        ys[y].append(xx[0])

    mean_dict = defaultdict(float)
    for k, vs in ys.items():
        mean_dict[k] = np.min(vs)

    sorted_keys = sorted(mean_dict.items(), key=lambda x: x[1], reverse=True)

    sep_line_tier1and2 = np.mean(sorted(x, reverse=True)[:int(len(x) * 0.2)]) # 상위 20% 의 평균 ps_score 를 1,2티어의 구분선으로 사용
    sep_line_tier2and3 = np.mean(sorted(x, reverse=True)[int(len(x)*0.2):int(len(x) * 0.4)])
    sep_line_tier3and4 = np.mean(sorted(x, reverse=True)[int(len(x)*0.4):int(len(x) * 0.8)])
    sep_line_tier4and5 = np.mean(sorted(x, reverse=True)[int(len(x)*0.8):int(len(x) * 1)])
    key_dict = dict()
    tier1_count = 0
    for i, (k, v) in enumerate(sorted_keys):
        if v > sep_line_tier1and2: # 4.0:
            tier = 0
            tier1_count +=1
        elif sep_line_tier1and2 >= v > sep_line_tier2and3:
        #elif 4.0>= v > 1.5:
            tier = 1
        elif sep_line_tier2and3 >= v > sep_line_tier3and4:
        #elif 1.5>= v >-1:
            tier = 2
        elif sep_line_tier3and4 >= v> sep_line_tier4and5:
        #elif -1>= v> -2.5:
            tier = 3
        else:
            tier = 4
        key_dict[k] = tier
    if tier1_count == 0:
        key_dict[sorted_keys[0][0]] = 0
    y_gmm = []
    for y in out.predict(x):
        y_gmm.append(key_dict[y]+1)

    return y_gmm


# calculate opscore
def get_op_score_info(sql, region, tier, version, lane):
    tier_weights_dict = {
        # winrate, pickrate, banrate, count, counter
        1: [1, 1, 0.15, 0, 1],
        2: [1.3, 0.8, 1, 0, 1],
        3: [0.7, 1, 1, 0, 1],
        13: [0.7, 1, 1, 0, 1],
    }
    tier_revise_dict = {
        1: False,
        2: True,
        3: False,
        13: True
    }
    tier_revise_weight_dict = {
        1: [1,0,0],
        2: [1,0,0],
        3: [1,0,0],
        13: [0.7,0,0],
    }
    tier_cut_dict = {
        1: False,
        2: True,
        3: False,
        13: True,
    }
    criterion_dict = {
        0: 5, # TOP
        1: 5.5, # JUNGLE
        2: 6, # MIDDLE
        3: 5, # ADCARRY
        4: 5 # SUPPORT
    }

    op_score_tuple = get_sorted_op_score_of_lane_and_tier(sql, region, tier, version, lane,
                                                          weights=tier_weights_dict[tier], revise=tier_revise_dict[tier],
                                                          revise_weight=tier_revise_weight_dict[tier], cut=tier_cut_dict[tier])


    OFFSET = 50
    op_score_dict = dict()
    values = []
    for rank, (champion_id, op_score, counter_score, revising_score) in enumerate(op_score_tuple):
        op_score_dict[champion_id] = (op_score, counter_score, revising_score, rank+1)
        values.append(op_score)
    ps_tier_list = get_ps_tier(values)
    ps_tier_dict = {k:v for k, v in zip(op_score_dict.keys(), ps_tier_list)}

    items = dict()
    for champion_id in champion_dict.values():
        if champion_id not in op_score_dict:
            op_score = 0
            op_tier = 6
            counter_score = 0
            is_op = False
            ranking = TOTAL_NUM_OF_CHAMPIONS
        else:
            op_score = op_score_dict[champion_id][0] + OFFSET
            counter_score = op_score_dict[champion_id][1] + OFFSET
            revising_score = op_score_dict[champion_id][2]
            ranking = op_score_dict[champion_id][3]
            op_tier = ps_tier_dict[champion_id]
            criterion = criterion_dict[lane]
            if op_score > OFFSET + criterion and op_tier == 1:
                is_op = True
            else:
                is_op = False
        items[(region, tier, version, lane, champion_id)] = [op_score, op_tier, is_op, counter_score, revising_score, ranking]
    return items


def get_honey_score_info(sql, region, tier, version, lane):
    tier_weights_dict = {
        1: [3,0.2,0.2,1,3],
        2: [3,0.2,0.2,1,3],
        3: [3,0.2,0.2,1,3],
        13: [3,0.2,0.2,1,3],
    }
    tier_revise_dict = {
        1: True,
        2: True,
        3: True,
        13: True,
    }
    tier_revise_weight_dict = {
        1: [1,0,0],
        2: [1,0,0],
        3: [1,0,0],
        13: [1,0,0],
    }
    tier_cut_dict = {
        1: False,
        2: False,
        3: False,
        13: False,
    }
    honey_score_tuple = get_sorted_op_score_of_lane_and_tier(sql, region, tier, version, lane,
                                                          weights=tier_weights_dict[tier], revise=tier_revise_dict[tier],
                                                          revise_weight=tier_revise_weight_dict[tier], cut=tier_cut_dict[tier])

    OFFSET = 50
    honey_score_dict = dict()
    items = dict()
    values = []
    for champion_id, honey_score, _, _ in honey_score_tuple:
        honey_score_dict[champion_id] = honey_score
        values.append(honey_score)
    max_honey_score = max(values)
    for champion_id in champion_dict.values():
        if champion_id not in honey_score_dict:
            honey_score = -100
        else:
            honey_score = honey_score_dict[champion_id]

        if honey_score > 12 or (max_honey_score - honey_score < 3 and honey_score > 8):
            is_honey = True
        else:
            is_honey = False
        items[(region, tier, version, lane, champion_id)] = [honey_score, is_honey]
    return items

def get_sorted_op_score_of_lane_and_tier(sql, region, tier, version, lane, weights=[1,1,1,1,1],
                                         revise=False, revise_weight=None, cut=True):
    candidates = get_candidate(sql, region, tier, version, lane)
    features = normalize_features(candidates)
    op_scores = get_op_scores(features, weights) # win, pick, ban, count
    if revise:
        op_scores = revise_score(sql, lane, op_scores, version, region, revise_weight, tier, cut)

    return op_scores

def get_ratio_dict(sql, region_id, version_id, tier_id, lane_id):
    # monkey patch
    tier_id = 2
    sql.set_cursor()
    sql.query('SELECT champion_id, lane_id, pick_rate FROM tmp_lane_version_winrate WHERE region_id = %s and version_id = %s and tier_id = %s',
         (region_id, version_id, tier_id))

    ratio_dict = defaultdict(float)
    pickrate_list = defaultdict(lambda: np.zeros(5))
    for champion, lane, pickrate in sql.cur.fetchall():
        pickrate_list[champion][lane] = float(pickrate)

    for champion_id in pickrate_list.keys():
        ratio_list = pickrate_list[champion_id] / (np.sum(pickrate_list[champion_id]) + 1e-10)
        ratio_dict[champion_id] = ratio_list[lane_id]
    return ratio_dict

def get_candidate(sql, region, tier, version, lane, pickrate_threshold=0.5):
    def banrate_fn(b):
        max_banrate = 25
        b = np.clip(b, 0, max_banrate)
        return 100*0.1*(np.log(b+5) - np.log(5))
    def pickrate_fn(b):
        return 100*0.2*(np.log(b+15) - np.log(15))
    def winrate_fn(b):
        penalty_winrate = 46
        if b <= penalty_winrate:
            b = b - 20
        return b

    counter_score_dict = get_counter_score_dict(sql, region, version)
    ratio_dict = get_ratio_dict(sql, region, version, tier, lane)
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, win_rate, pick_rate, ban_rate, count \
               FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d and lane_id=%d and tier_id=%d and pick_rate > %s'%
              (region, version, lane, tier, pickrate_threshold))
    candidate_list = []
    fetchall = sql.cur.fetchall()
    for region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count in fetchall:
        counter_score = counter_score_dict[(region_id, version_id, tier_id, lane_id, champion_id)]
        ratio = ratio_dict[champion_id]
        candidate_list.append((champion_id, int(count), winrate_fn(float(winrate)), pickrate_fn(float(pickrate)), banrate_fn(float(banrate)*ratio), float(pickrate) + float(banrate)*ratio, counter_score))
    return candidate_list


def get_counter_score_dict(sql, region, version):
    sql.set_cursor()
    sql.query('SELECT champion_id, lane_id, region_id, tier_id, version_id, counter_winrate_list, counter_count_list FROM counter_winrate WHERE region_id = %d and version_id = %d'%(region, version))
    counter_score_dict = dict()
    for champion, lane, region, tier, version, winrate_list, count_list in sql.cur.fetchall():
        if len(count_list) > 2:
            winrate_arr = np.array(eval(winrate_list))
            count_arr = np.array(eval(count_list))
            total_score = (winrate_arr * count_arr).sum()
            total_count = count_arr.sum()
            winrate_std = winrate_arr.std()
            counter_score = total_score / total_count - winrate_std
        else:
            counter_score = 0
        counter_score_dict[(region, version, tier, lane, champion)] = counter_score
    return counter_score_dict


def normalize_features(candidates):
    champ_name_list = []
    win_rate_list = []
    pick_rate_list = []
    ban_rate_list = []
    count_list = []
    pickban_rate_list = []
    counter_score_list = []
    for champ_name, count, win_rate, pick_rate, ban_rate, pickban_rate, counter_score in candidates:
        champ_name_list.append(champ_name)
        count_list.append(count)
        win_rate_list.append(win_rate)
        pick_rate_list.append(pick_rate)
        ban_rate_list.append(ban_rate)
        pickban_rate_list.append(pickban_rate)
        counter_score_list.append(counter_score)

    count_list = normalize(count_list)
    win_rate_list = normalize(win_rate_list)
    pick_rate_list = normalize(pick_rate_list)
    ban_rate_list = normalize(ban_rate_list)
    pickban_rate_list = normalize(pickban_rate_list)
    counter_score_list = normalize(counter_score_list)

    return list(zip(champ_name_list, count_list, win_rate_list, pick_rate_list, ban_rate_list, pickban_rate_list, counter_score_list))


def normalize(arr):
    def _get_mean_and_std(arr):
        mean = np.mean(arr)
        std = np.std(arr)
        return mean, std

    arr = np.array(arr)
    mean, std = _get_mean_and_std(arr)
    return (arr - mean) / std


def get_op_scores(features, weights):

    op_scores = []
    for champ_name, count, win_rate, pick_rate, ban_rate, pickban_rate, counter_score in features:
        #print("%30s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f"%(champ_name, win_rate, pick_rate, ban_rate, count, counter_score))
        op_score = weights[0] * win_rate + weights[1] * pick_rate + weights[2] * ban_rate + weights[3] * count + weights[4] * counter_score
        op_scores.append((champ_name, op_score, counter_score, 0))
    op_scores =  sorted(op_scores, key=lambda x:x[1], reverse=True)
    return op_scores


def revise_score(sql, lane, op_scores, version, region, weights=[1,1,1], tier=None, cut=True):
    ban_diff = get_diff_of_feature(sql, lane, 'ban_rate', tier, version, region)
    win_diff = get_diff_of_feature(sql, lane, 'win_rate', tier, version, region)
    pick_diff = get_diff_of_feature(sql, lane, 'pick_rate', tier, version, region)
    pickban_diff = get_diff_of_feature(sql, lane, 'pickban_rate', tier, version, region)
    revised = []
    for champion_name, op_score, counter_score, rv in op_scores:
        if champion_name not in ban_diff:
            bd = 0
        else:
            bd = ban_diff[champion_name]
        if champion_name not in win_diff:
            wd = 0
        else:
            wd = win_diff[champion_name]
        if champion_name not in pick_diff:
            pd = 0
        else:
            pd = pick_diff[champion_name]
        if champion_name not in pickban_diff:
            pbd = 0
        else:
            pbd = pickban_diff[champion_name]
        if cut:
            #revising_score = min(bd, 0) * weights[0] + min(wd, 0) * weights[1] + min(pd, 0) * weights[2]
            revising_score = min(pbd, 0) * weights[0] + min(wd, 0) * weights[1] + min(pd, 0) * weights[2]
            new_score = op_score + revising_score
        else:
            #revising_score = bd * weights[0] + wd * weights[1] + pd * weights[2]
            revising_score = pbd * weights[0] + wd * weights[1] + pd * weights[2]
            new_score = op_score + revising_score
        revised.append((champion_name, new_score, counter_score, revising_score))

    revised = sorted(revised, key=lambda x:x[1], reverse=True)

    return revised


def get_diff_of_feature(sql, lane, feature_name, tier, version, region):
    #tier1 = 3 # MGC
    if tier == 13:
        tier1 = 3
        tier2 = tier
    else:
        tier1 = 13
        tier2 = tier
    #tier1 = 13 # DIA+, tmp
    #tier2 = tier

    candidates1 = get_candidate(sql, region, tier1, version, lane, 0)
    #features1 = normalize_featuers(candidates1)

    candidates2 = get_candidate(sql, region, tier2, version, lane, 0)
    #features2 = normalize_featuers(candidates2)

    feature_dict1 = dict()
    feature_dict2 = dict()
    for idx in range(len(candidates1)):
        champion_name1, c1,w1, p1, b1, pb1, counter1 = candidates1[idx]
        if feature_name == 'pick_rate':
            feature_dict1[champion_name1] = p1
        elif feature_name == 'win_rate':
            feature_dict1[champion_name1] = w1
        elif feature_name == 'count':
            feature_dict1[champion_name1] = c1
        elif feature_name == 'ban_rate':
            feature_dict1[champion_name1] = b1
        elif feature_name == 'pickban_rate':
            feature_dict1[champion_name1] = pb1
        elif feature_name == 'counter':
            feature_dict1[champion_name1] = counter1
    for idx in range(len(candidates2)):
        champion_name2, c2, w2, p2, b2, pb2, counter2 = candidates2[idx]
        if feature_name == 'pick_rate':
            feature_dict2[champion_name2] = p2
        elif feature_name == 'win_rate':
            feature_dict2[champion_name2] = w2
        elif feature_name == 'count':
            feature_dict2[champion_name2] = c2
        elif feature_name == 'ban_rate':
            feature_dict2[champion_name2] = b2
        elif feature_name == 'pickban_rate':
            feature_dict2[champion_name2] = pb2
        elif feature_name == 'counter':
            feature_dict2[champion_name2] = counter2

    diff_dict = dict()
    for champion_name, _,_,_,_,_,_ in candidates2:
        if champion_name in feature_dict1 and champion_name in feature_dict2:
            diff_dict[champion_name] = max(min(feature_dict1[champion_name] - feature_dict2[champion_name], 6), -6)

    normalized_features = normalize(list(diff_dict.values()))
    new_diff_dict = dict()
    for i, k in enumerate(diff_dict.keys()):
        new_diff_dict[k] = normalized_features[i]

    return new_diff_dict
