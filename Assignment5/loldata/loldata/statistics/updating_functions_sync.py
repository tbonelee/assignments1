import numpy as np
from collections import defaultdict

# from .ps_score2 import get_op_score_info, get_honey_score_info
from .ps_score3 import get_ps_score_dict
from ..db_handler.psql_handler import PSQLHandler
from ..utils import get_now

sql_sync = PSQLHandler(connect=False, db_name='lolps_sync')
sql_sync.connect()


def get_core_item_id_list(sql):
    sql.set_cursor()
    sql.query('SELECT item_id FROM item_info WHERE is_core = True')
    items = sql.cur.fetchall()
    item_id_list = []
    for item in items:
        item_id_list.append(item[0])

    return item_id_list

def get_mythic_item_id_list(sql):
    sql.set_cursor()
    sql.query('SELECT item_id FROM item_info WHERE itemclass_id = 3')
    items = sql.cur.fetchall()
    item_id_list = []
    for item in items:
        item_id_list.append(item[0])

    return item_id_list

def get_info_dict_from_sql(sql, table, key_index=1, fn=lambda x:x, id_name=False):
    sql.set_cursor()
    sql.query('SELECT * FROM %s'%table)
    items = sql.cur.fetchall()
    ret_dict = dict()
    for item in items:
        key = fn(item[key_index])
        if id_name:
            ret_dict[item[0]] = key
        else:
            ret_dict[key] = item[0]
    return ret_dict



# version_dict = get_info_dict_from_sql(sql_sync, 'version_info')
# region_dict = get_info_dict_from_sql(sql_sync, 'region_info')
# lane_dict = get_info_dict_from_sql(sql_sync, 'lane_info', key_index=2)
# tier_dict = get_info_dict_from_sql(sql_sync, 'tier_info')
# date_dict = get_info_dict_from_sql(sql_sync, 'date_info')
champion_dict = get_info_dict_from_sql(sql_sync, 'champion_info')
mythic_item_id_list = get_mythic_item_id_list(sql_sync)
TOTAL_NUM_OF_CHAMPIONS = len(champion_dict)

def get_mythic_item_count(item_id_list):
    count = 0
    for item_id in item_id_list:
        if item_id in mythic_item_id_list:
            count += 1
    return count

def update_counter_winrate_table(sql, region_id, version_id, threshold=2):
    def _get_items():
        items = []
        counter_champion_id_list = defaultdict(list)
        counter_winrate_list = defaultdict(list)
        counter_count_list = defaultdict(list)
        counter_pickrate_list = defaultdict(list)

        sql.set_cursor()
        sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id FROM view_version_tier_champion_lane_count WHERE region_id = %d and version_id = %d'%(region_id, version_id))
        for region, version, tier, champion,lane in sql.cur.fetchall():
            counter_champion_id_list[(region, version, tier, lane, champion)] = []
            counter_winrate_list[(region, version, tier, lane, champion)] = []
            counter_count_list[(region, version, tier, lane, champion)] = []
            counter_pickrate_list[(region, version, tier, lane, champion)] = []
        sql.cur.close()

        sql.set_cursor()
        sql.query('SELECT region_id, version_id, tier_id, champion1_id, champion2_id, lane_id, win_rate, pick_rate, count FROM view_counter_winrate WHERE region_id = %d and version_id = %d'%(region_id, version_id))
        for region, version, tier, champion1, champion2, lane, win_rate, pick_rate, count in sql.cur.fetchall():
            if pick_rate >= 1:
                counter_champion_id_list[(region, version, tier, lane, champion1)].append(champion2)
                counter_winrate_list[(region, version, tier, lane, champion1)].append(float(win_rate))
                counter_count_list[(region, version, tier, lane, champion1)].append(int(count))
                counter_pickrate_list[(region, version, tier, lane, champion1)].append(float(pick_rate))
        sql.cur.close()

        for k in counter_champion_id_list.keys():
            region, version, tier, lane, champion = k
            chlist = counter_champion_id_list[k]
            wlist = counter_winrate_list[k]
            clist = counter_count_list[k]
            plist = counter_pickrate_list[k]
            tup = sorted([(ch, w, c, p) for ch, w, c, p in zip(chlist, wlist, clist, plist)], key=lambda x:x[1])
            chlist = [ch for ch, w, c, p in tup]
            wlist = [round(w,2) for ch, w, c, p in tup]
            clist = [round(c,2) for ch, w, c, p in tup]
            plist = [round(p,2) for ch, w, c, p in tup]

            items.append((champion, lane, region, version, tier, str(chlist), str(wlist), str(clist), str(plist)))
        return items

    sql.set_cursor()
    sql.delete('counter_winrate', condition='version_id = %d and region_id = %d'%(version_id, region_id))
    sql.query("SELECT setval('counter_winrate_id_seq', (SELECT MAX(id) FROM counter_winrate)+1)")
    items = _get_items()

    sql.set_cursor()
    for item in items:
        sql.insert('counter_winrate',
                   '(champion_id, lane_id, region_id, version_id, tier_id, counter_champion_id_list, counter_winrate_list, counter_count_list, counter_pickrate_list, updated_at)',
                  (*item,  get_now()))
    sql.commit()
    sql.cur.close()



def get_basic_info_dict(sql, region, tier, version, lane):
    info_dict = dict()
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, win_rate, pick_rate, ban_rate, count \
               FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d and lane_id=%d and tier_id=%d'%
              (region, version, lane, tier))
    for region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count in sql.cur.fetchall():
        info_dict[(region_id, tier_id, version_id, lane_id, champion_id)] = [winrate, pickrate, banrate, count]
    sql.cur.close()
    return info_dict

def update_tmp_lane_version_winrate_table(sql, region, version):
    info_dict = dict()
    sql.set_cursor()
    sql.delete('tmp_lane_version_winrate', condition='version_id=%d and region_id=%d'%(version, region))
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count \
               FROM view_lane_version_winrate WHERE region_id = %d and version_id = %d'%
              (region, version))
    for region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count in sql.cur.fetchall():
        sql.insert('tmp_lane_version_winrate',
                   '(region_id, tier_id, version_id, lane_id, champion_id, win_rate, pick_rate, ban_rate, count)',
                   (region_id, tier_id, version_id, lane_id, champion_id, winrate, pickrate, banrate, count))
    sql.commit()
    sql.cur.close()


# def update_version_win_rate_table(sql, region, version):
#     update_tmp_lane_version_winrate_table(sql, region, version)
#     sql.set_cursor()
#     sql.delete('lane_version_winrate', condition='version_id = %d and region_id = %d'%(version, region))
#     sql.query("SELECT setval('lane_version_winrate_id_seq', (SELECT MAX(id) FROM lane_version_winrate)+1)")
#     #sql.cur.close()

#     def get_last_ranking_dict(sql, region_id, version_id, tier_id, lane_id):
#         #sql.set_cursor()
#         query = 'SELECT champion_id, ranking FROM lane_version_winrate WHERE region_id=%s and version_id=%s and tier_id=%s and lane_id=%s'
#         params = (region_id, version_id-1, tier_id, lane_id)
#         sql.query(query, params)
#         last_ranking_dict = dict()
#         for c, r in sql.cur.fetchall():
#             last_ranking_dict[c] = r
#         #sql.cur.close()
#         return last_ranking_dict

#     # update_op_score(sql, region, version)
#     for tier in [1,2,3,13]: # BSG, PLA+, MSG, DIA+
#         for lane in range(5):
#             last_ranking_dict = get_last_ranking_dict(sql, region, version, tier, lane)
#             basic_info = get_basic_info_dict(sql, region, tier, version, lane)
#             op_score_info = get_op_score_info(sql, region, tier, version, lane)
#             honey_score_info = get_honey_score_info(sql, region, tier, version, lane)
#             #sql.set_cursor()
#             for k in basic_info.keys():
#                 region_id, tier_id, version_id, lane_id, champion_id = k
#                 win_rate, pick_rate, ban_rate, count = basic_info[k]
#                 op_score, op_tier, is_op, counter_score, revising_score, ranking = op_score_info[k]
#                 honey_score, is_honey = honey_score_info[k]
#                 #honey_score = 0 # tmp
#                 #is_honey = False # tmp
#                 if champion_id in last_ranking_dict:
#                     last_ranking = last_ranking_dict[champion_id]
#                 else:
#                     last_ranking = TOTAL_NUM_OF_CHAMPIONS
#                 ranking_variation = last_ranking - ranking
#                 item = (champion_id, lane_id, region_id, version_id, tier_id,
#                         count, pick_rate, ban_rate, win_rate, op_score, op_tier, is_honey, is_op, counter_score, revising_score,
#                         ranking, ranking_variation, honey_score)
#                 sql.insert('lane_version_winrate',
#                           '(champion_id, lane_id, region_id, version_id, tier_id, count, pick_rate, \
#                             ban_rate, win_rate, op_score, op_tier, is_honey, is_op, counter_score, revising_score, ranking, ranking_variation, honey_score, updated_at)',
#                             (*item,  get_now()))
#     sql.commit()
#     #sql.cur.close()

def update_version_win_rate_table(sql, region, version):
    update_tmp_lane_version_winrate_table(sql, region, version)
    sql.set_cursor()
    sql.delete('lane_version_winrate', condition='version_id = %d and region_id = %d'%(version, region))
    sql.query("SELECT setval('lane_version_winrate_id_seq', (SELECT MAX(id) FROM lane_version_winrate)+1)")
    #sql.cur.close()

    ps_score_dict = get_ps_score_dict(sql, region, version)
    sql.set_cursor()
    for (region_id, version_id, tier_id, lane_id, champion_id), v in ps_score_dict.items():
        item = (champion_id, lane_id, region_id, version_id, tier_id,
                v.count, v.pick_rate, v.ban_rate, v.win_rate, v.op_score, v.op_tier, bool(v.is_honey), bool(v.is_op), v.z_counter_score, v.revising_score,
                v.ranking, v.ranking_variation, v.honey_score, v.overall_ranking, v.overall_ranking_variation)
        sql.insert('lane_version_winrate',
                    '(champion_id, lane_id, region_id, version_id, tier_id, count, pick_rate, \
                    ban_rate, win_rate, op_score, op_tier, is_honey, is_op, counter_score, revising_score, ranking, ranking_variation, honey_score, \
                    overall_ranking, overall_ranking_variation, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    #sql.cur.close()



def update_date_win_rate_table(sql, region, version, date):
    sql.set_cursor()
    sql.delete('lane_date_winrate', condition='version_id = %d and region_id = %d and date_id = %d'%(version, region, date))
    sql.query("SELECT setval('lane_date_winrate_id_seq', (SELECT MAX(id) FROM lane_date_winrate)+1)")
    #sql.cur.close()

    #sql.set_cursor()
    sql.query('SELECT a.region_id, a.version_id, a.tier_id, a.champion_id, a.lane_id, a.date_id, a.winrate, \
    a.pickrate, a.banrate, a.count, b.op_score, b.counter_score FROM \
    (( SELECT region_id, version_id, tier_id, champion_id, lane_id, date_id, winrate, pickrate, banrate, count \
    FROM view_lane_date_winrate) a JOIN ( SELECT region_id, version_id, tier_id, champion_id, lane_id, op_score, counter_score \
    FROM lane_version_winrate) b ON a.region_id = b.region_id and a.version_id = b.version_id and a.tier_id = b.tier_id \
    and a.champion_id = b.champion_id and a.lane_id = b.lane_id) WHERE a.region_id = %d and a.version_id = %d and a.date_id = %d ;'%(region, version, date))

    for item in sql.cur.fetchall():
        sql.insert('lane_date_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, date_id, win_rate, pick_rate, ban_rate, \
                    count, op_score, counter_score, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    #sql.cur.close()

def update_shoes_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_shoes_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_shoes_winrate_id_seq', (SELECT MAX(id) FROM item_shoes_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, winrate, pickrate, count \
    FROM view_item_shoes_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('item_shoes_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

#def update_starting_win_rate_table(sql, region, version):
#    sql.set_cursor()
#    sql.delete('item_starting_winrate', condition='version_id = %d and region_id = %d '%(version, region))

#    sql.set_cursor()
#    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, winrate, pickrate, count \
#    FROM view_item_starting_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

#    for item in sql.cur.fetchall():
#        region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count = item
#        #item_id_list = str(sorted(eval(item_id_list)))
#        #item = (region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count)
#        if pick_rate > 0.5:
#            sql.insert('item_starting_winrate',
#                        '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
#                        (*item,  get_now()))
#    sql.commit()

def update_starting_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_starting_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_starting_winrate_id_seq', (SELECT MAX(id) FROM item_starting_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win, lose, count \
        FROM view_starting_count WHERE region_id = %d and version_id = %d ;'%(region, version))

    starting_win_dict = defaultdict(float)
    starting_count_dict = defaultdict(float)
    champ_count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win, lose, count = item
        item_id_list = str(sorted(eval(item_id_list)))
        starting_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, item_id_list)] += win
        starting_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, item_id_list)] += (win + lose)
        champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += (win + lose)

    for region_id, version_id, tier_id, champion_id, lane_id, item_id_list in starting_win_dict.keys():
        win = starting_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, item_id_list)]
        count = starting_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, item_id_list)]
        champ_count = champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        win_rate = win / count * 100
        pick_rate = count / champ_count * 100
        item = region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count
        if pick_rate > 0.5:
            sql.insert('item_starting_winrate',
                       '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
                       (*item,  get_now()))
    sql.commit()
    sql.cur.close()


def update_spell_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('spell_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('spell_winrate_id_seq', (SELECT MAX(id) FROM spell_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, spell1_id, spell2_id, win_rate, pick_rate, count \
    FROM view_spell_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('spell_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, spell1_id, spell2_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_statperk_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('statperk_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('statperk_total_winrate_id_seq', (SELECT MAX(id) FROM statperk_total_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, statperk_id_list, win_rate, pick_rate, count \
    FROM view_statperk_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('statperk_total_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, statperk_id_list, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def get_basic_info(sql, region, version):
    info_dict = dict()
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count \
               FROM view_lane_version_winrate WHERE region_id = %d and version_id = %d'%
              (region, version))
    for region_id, version_id, tier_id, champion_id, lane_id, winrate, pickrate, banrate, count in sql.cur.fetchall():
        info_dict[(region_id, tier_id, version_id, lane_id, champion_id)] = [winrate, pickrate, count]
    return info_dict

def get_synergy_list(sql, region, version):

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion1_id, champion2_id, duo_id, win_rate, pick_rate, count \
               FROM view_duo_synergy_winrate WHERE region_id = %d and version_id = %d'%
              (region, version))
    return sql.cur.fetchall()

def get_pick_count1(sql, region, version):
    info_dict = dict()
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion1_id, duo_id, count \
               FROM view_duo_pickcount1 WHERE region_id = %d and version_id = %d'%
              (region, version))
    for region_id, version_id, tier_id, champion1_id, duo_id, count in sql.cur.fetchall():
        info_dict[(region_id, version_id, tier_id, champion1_id, duo_id)] = count
    return info_dict

def get_pick_count2(sql, region, version):
    info_dict = dict()
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion2_id, duo_id, count \
               FROM view_duo_pickcount2 WHERE region_id = %d and version_id = %d'%
              (region, version))
    for region_id, version_id, tier_id, champion2_id, duo_id, count in sql.cur.fetchall():
        info_dict[(region_id, version_id, tier_id, champion2_id, duo_id)] = count
    return info_dict

def get_synergy_score(pickrate1, pickrate2, count, duo_winrate, winrate1, winrate2):
    pickrate1 = float(pickrate1)/100
    pickrate2 = float(pickrate2)/100
    duo_winrate = float(duo_winrate)/100
    winrate1 = float(winrate1)/100
    winrate2 = float(winrate2)/100
    T = 15
    def modified_win_rate(win_rate):
        mu = 0.0
        std = 0.1
        m_win_rate = (win_rate - mu) / std
        return m_win_rate

    score = None
    if (pickrate1 + pickrate2)/2 > 0.01:
        score_source = np.exp(pickrate1 / T) * np.log2(count + 1e-10) * modified_win_rate(duo_winrate - winrate1) * 100
        score_target = np.exp(pickrate2 / T) * np.log2(count + 1e-10) * modified_win_rate(duo_winrate - winrate2) * 100
        score = (score_source + score_target) / 2
    return score

def get_lane_pair(duo_id):
    if duo_id == 0:
        return 3, 4
    elif duo_id == 1:
        return 2, 1
    elif duo_id == 2:
        return 0, 1
    elif duo_id == 3:
        return 1, 4
    else:
        raise NotImplementedError

def update_duo_synergy_table(sql, region, version):
    sql.set_cursor()
    sql.delete('duo_synergy_winrate', condition='version_id = %d and region_id = %d'%(version, region))
    sql.query("SELECT setval('duo_synergy_winrate_id_seq', (SELECT MAX(id) FROM duo_synergy_winrate)+1)")

    basic_info = get_basic_info(sql, region, version)
    pick_count1_info = get_pick_count1(sql, region, version)
    pick_count2_info = get_pick_count2(sql, region, version)
    for region_id, version_id, tier_id, champion1_id, champion2_id, duo_id, win_rate, pick_rate, count in get_synergy_list(sql, region, version):
        lane1_id, lane2_id = get_lane_pair(duo_id)
        winrate1, pickrate1, _ = basic_info[(region_id, tier_id, version_id, lane1_id, champion1_id)]
        winrate2, pickrate2, _ = basic_info[(region_id, tier_id, version_id, lane2_id, champion2_id)]
        if pickrate1 > 0.1 and pickrate2 > 0.1:
            count1 = pick_count1_info[(region_id, version_id, tier_id, champion1_id, duo_id)]
            count2 = pick_count2_info[(region_id, version_id, tier_id, champion2_id, duo_id)]
            duo_pickrate1 = count / count1 * 100
            duo_pickrate2 = count / count2 * 100
            synergy_score = get_synergy_score(duo_pickrate1, duo_pickrate2, count, win_rate, winrate1, winrate2)
            if synergy_score is not None:
                item = (region_id, version_id, tier_id, duo_id, champion1_id, champion2_id, synergy_score, winrate1, winrate2,
                          win_rate, count, pick_rate, duo_pickrate1, duo_pickrate2)
                sql.insert('duo_synergy_winrate',
                          '(region_id, version_id, tier_id, duo_id, champion_id1, champion_id2, synergy_score, winrate1, winrate2, \
                          duo_winrate, count, pickrate, pickrate1, pickrate2, updated_at)',
                            (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_main1_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_main1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_main1_winrate_id_seq', (SELECT MAX(id) FROM rune_main1_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count \
    FROM view_rune_main1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('rune_main1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_main2_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_main2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_main2_winrate_id_seq', (SELECT MAX(id) FROM rune_main2_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count \
    FROM view_rune_main2_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('rune_main2_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_main3_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_main3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_main3_winrate_id_seq', (SELECT MAX(id) FROM rune_main3_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count \
    FROM view_rune_main3_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('rune_main3_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_main4_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_main4_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_main4_winrate_id_seq', (SELECT MAX(id) FROM rune_main4_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count \
    FROM view_rune_main4_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        sql.insert('rune_main4_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (*item,  get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_sub_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_sub1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_sub1_winrate_id_seq', (SELECT MAX(id) FROM rune_sub1_winrate)+1)")
    sql.delete('rune_sub2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_sub2_winrate_id_seq', (SELECT MAX(id) FROM rune_sub2_winrate)+1)")

    rune_sub_win_dict = defaultdict(float)
    rune_sub_count_dict = defaultdict(float)
    champ_count_dict = defaultdict(float)

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win, lose, count \
    FROM view_rune_sub1_count WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win, lose, count = item
        rune_sub_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)] += win
        rune_sub_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)] += (win + lose)
        champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += (win + lose)

    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win, lose, count \
    FROM view_rune_sub2_count WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win, lose, count = item
        rune_sub_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)] += win
        rune_sub_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)] += (win + lose)
        champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += (win + lose)

    for region_id, version_id, tier_id, champion_id, lane_id, rune_sub in rune_sub_win_dict.keys():
        win = rune_sub_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)]
        count = rune_sub_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)]
        champ_count = champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        win_rate = win / count * 100
        pick_rate = count / champ_count * 100 * 2

        sql.insert('rune_sub1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()
    sql.set_cursor()
    for region_id, version_id, tier_id, champion_id, lane_id, rune_sub in rune_sub_win_dict.keys():
        win = rune_sub_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)]
        count = rune_sub_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, rune_sub)]
        champ_count = champ_count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        win_rate = win / count * 100
        pick_rate = count / champ_count * 100 * 2

        sql.insert('rune_sub2_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, win_rate, pick_rate, count, updated_at)',
                    (region_id, version_id, tier_id, champion_id, lane_id, rune_sub, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_rune_total_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('rune_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('rune_total_winrate_id_seq', (SELECT MAX(id) FROM rune_total_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_main1, rune_main2, rune_main3, rune_main4, \
        rune_sub1, rune_sub2, rune_category1, rune_category2, win_rate, pick_rate, count \
        FROM view_rune_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, rune_main1, rune_main2, rune_main3, rune_main4,
         rune_sub1, rune_sub2, rune_category1, rune_category2, win_rate, pick_rate, count) = item
        category1_rune_id_list = str([rune_main1, rune_main2, rune_main3, rune_main4])
        category2_rune_id_list = str([rune_sub1, rune_sub2])
        if pick_rate > 0.5:
            sql.insert('rune_total_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, category1_rune_id_list, category2_rune_id_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, category1_rune_id_list, category2_rune_id_list, win_rate, pick_rate, count,  get_now()))
    sql.commit()
    sql.cur.close()

def update_item_1st_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_1st_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_1st_core_winrate_id_seq', (SELECT MAX(id) FROM item_1st_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 1:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[0])] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[0])] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('item_1st_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_item_2nd_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_2nd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_2nd_core_winrate_id_seq', (SELECT MAX(id) FROM item_2nd_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 2:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[1])] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[1])] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('item_2nd_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_item_3rd_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_3rd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_3rd_core_winrate_id_seq', (SELECT MAX(id) FROM item_3rd_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 3:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[2])] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[2])] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('item_3rd_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_item_4th_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_4th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_4th_core_winrate_id_seq', (SELECT MAX(id) FROM item_4th_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 4:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[3])] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[3])] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('item_4th_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()


def update_item_5th_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_5th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_5th_core_winrate_id_seq', (SELECT MAX(id) FROM item_5th_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 5:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[4])] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, core_id_list[4])] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('item_5th_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()


def update_item_two_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_two_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_two_core_winrate_id_seq', (SELECT MAX(id) FROM item_two_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT tier_id, champion_id, lane_id, count \
        FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))
    lane_champ_count_dict = dict()
    for item in sql.cur.fetchall():
        (tier_id, champion_id, lane_id, count) = item
        lane_champ_count_dict[(tier_id, champion_id, lane_id)] = count

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 2 and get_mythic_item_count(core_id_list) <= 1:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:2]))] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:2]))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        lane_champ_count = lane_champ_count_dict[(tier_id, champion_id, lane_id)]
        lane_pick_rate = count / (lane_champ_count + 1e-10 )* 100
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5 and lane_pick_rate > 0.2:
            sql.insert('item_two_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_item_three_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_three_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_three_core_winrate_id_seq', (SELECT MAX(id) FROM item_three_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT tier_id, champion_id, lane_id, count \
        FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))
    lane_champ_count_dict = dict()
    for item in sql.cur.fetchall():
        (tier_id, champion_id, lane_id, count) = item
        lane_champ_count_dict[(tier_id, champion_id, lane_id)] = count

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 3 and get_mythic_item_count(core_id_list) <= 1:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:3]))] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:3]))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        lane_champ_count = lane_champ_count_dict[(tier_id, champion_id, lane_id)]
        lane_pick_rate = count / (lane_champ_count + 1e-10 )* 100
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5 and lane_pick_rate > 0.2:
            sql.insert('item_three_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_item_four_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_four_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_four_core_winrate_id_seq', (SELECT MAX(id) FROM item_four_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT tier_id, champion_id, lane_id, count \
        FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))
    lane_champ_count_dict = dict()
    for item in sql.cur.fetchall():
        (tier_id, champion_id, lane_id, count) = item
        lane_champ_count_dict[(tier_id, champion_id, lane_id)] = count

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 4 and get_mythic_item_count(core_id_list) <= 1:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:4]))] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:4]))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        lane_champ_count = lane_champ_count_dict[(tier_id, champion_id, lane_id)]
        lane_pick_rate = count / (lane_champ_count + 1e-10 )* 100
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5 and lane_pick_rate > 0.2:
            sql.insert('item_four_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()


def update_item_five_core_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('item_five_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('item_five_core_winrate_id_seq', (SELECT MAX(id) FROM item_five_core_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT tier_id, champion_id, lane_id, count \
        FROM tmp_lane_version_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))
    lane_champ_count_dict = dict()
    for item in sql.cur.fetchall():
        (tier_id, champion_id, lane_id, count) = item
        lane_champ_count_dict[(tier_id, champion_id, lane_id)] = count

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose \
        FROM view_item_core_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    core_win_dict = defaultdict(float)
    core_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, core_id_list, count, win, lose) = item
        core_id_list = eval(core_id_list)
        if len(core_id_list) >= 5 and get_mythic_item_count(core_id_list) <= 1:
            core_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:5]))] += win
            core_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(core_id_list[:5]))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in core_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list) = k
        count = core_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        lane_champ_count = lane_champ_count_dict[(tier_id, champion_id, lane_id)]
        lane_pick_rate = count / (lane_champ_count + 1e-10 )* 100
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5 and lane_pick_rate > 0.2:
            sql.insert('item_five_core_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, item_id_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()


def get_skill_name(arr):
    d = {
        1:'Q',
        2:'W',
        3:'E',
        4:'R',
    }
    return [d[k] for k in arr]


def update_skill_lv1_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('skill_lv1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_lv1_winrate_id_seq', (SELECT MAX(id) FROM skill_lv1_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        if len(skill_list) >= 1:
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:1])))] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:1])))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        sql.insert('skill_lv1_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, updated_at)',
                    (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_skill_lv3_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('skill_lv3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_lv3_winrate_id_seq', (SELECT MAX(id) FROM skill_lv3_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        if len(skill_list) >= 3:
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:3])))] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:3])))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        sql.insert('skill_lv3_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, updated_at)',
                    (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_skill_lv6_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('skill_lv6_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_lv6_winrate_id_seq', (SELECT MAX(id) FROM skill_lv6_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        if len(skill_list) >= 6:
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:6])))] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:6])))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('skill_lv6_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_skill_lv11_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('skill_lv11_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_lv11_winrate_id_seq', (SELECT MAX(id) FROM skill_lv11_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        if len(skill_list) >= 11:
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:11])))] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:11])))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('skill_lv11_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

def update_skill_lv15_win_rate_table(sql, region, version):

    def get_first_master_skill_id(skill_list):
        skill_num_dict = defaultdict(int)
        for idx in skill_list:
            skill_num_dict[idx] += 1
            if skill_num_dict[idx] == 5:
                return idx
        return None

    sql.set_cursor()
    sql.delete('skill_lv15_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_lv15_winrate_id_seq', (SELECT MAX(id) FROM skill_lv15_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        if len(skill_list) >= 15:
            first_master_skill_id = get_first_master_skill_id(skill_list)
            if first_master_skill_id is None:
                continue
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:15])), first_master_skill_id)] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(get_skill_name(skill_list[0:15])), first_master_skill_id)] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, first_master_skill_id) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('skill_lv15_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, first_master_skill_id, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, first_master_skill_id, get_now()))
    sql.commit()
    sql.cur.close()

def get_master_list(skill_list):
    master=[]
    cn=0
    ult=0
    for lv in range(len(skill_list)):
        if skill_list[0:lv].count(1) >= 5:
            break
        elif skill_list[0:lv].count(2) >= 5:
            break
        elif skill_list[0:lv].count(3) >= 5:
            break
        elif skill_list[0:lv].count(4) >= 5:
            master.append('R')
            ult+=1
            break

    for lv in range(len(skill_list)):
        if skill_list[0:lv].count(1) >= 5:
            master.append('Q')
            cn+=1
            break
        elif skill_list[0:lv].count(2) >= 5:
            master.append('W')
            cn+=2
            break
        elif skill_list[0:lv].count(3) >= 5:
            master.append('E')
            cn+=3
            break

    for lv in range(len(skill_list)):
        if skill_list[0:lv].count(1) >= 3 and (cn == 2 or cn == 3):
            master.append('Q')
            cn+=1
            break
        elif skill_list[0:lv].count(2) >= 3 and (cn == 1 or cn == 3):
            master.append('W')
            cn+=2
            break
        elif skill_list[0:lv].count(3) >= 3 and (cn == 1 or cn == 2):
            master.append('E')
            cn+=3
            break

    if ult != 1:
        if (6 - cn) == 1:
            master.append('Q')
        elif(6 - cn) == 2:
            master.append('W')
        elif (6 - cn) == 3:
            master.append('E')

    if len(master) == 3:
        return master
    else:
        return None


def update_skill_master_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('skill_master_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('skill_master_winrate_id_seq', (SELECT MAX(id) FROM skill_master_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose \
        FROM view_skill_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    skill_win_dict = defaultdict(float)
    skill_count_dict = defaultdict(float)
    count_dict = defaultdict(float)
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_list, count, win, lose) = item
        skill_list = eval(skill_list)
        master_list = get_master_list(skill_list)
        if master_list is not None:
            skill_win_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(master_list))] += win
            skill_count_dict[(region_id, version_id, tier_id, champion_id, lane_id, str(master_list))] += count
            count_dict[(region_id, version_id, tier_id, champion_id, lane_id)] += count
    for k, win in skill_win_dict.items():
        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list) = k
        count = skill_count_dict[k]
        win_rate = win / count * 100
        champ_count = count_dict[(region_id, version_id, tier_id, champion_id, lane_id)]
        pick_rate = count / champ_count * 100
        if pick_rate > 0.5:
            sql.insert('skill_master_winrate',
                      '(region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, updated_at)',
                        (region_id, version_id, tier_id, champion_id, lane_id, skill_name_list, win_rate, pick_rate, count, get_now()))
    sql.commit()
    sql.cur.close()

tier_merge_dict = {
    4: [],
    5: [1],
    6: [1],
    7: [1],
    8: [2],
    9: [2, 13],
    10: [2, 13, 3],
    11: [2, 13, 3],
    12: [2, 13, 3]
}

def time_norm(pita, origin):
    output=[]
    for i in range(len(pita)):
        weight=i/len(pita)
        output.append(round((1-weight) * pita[i] + weight * origin[i], 4)*100)
    return output


def update_timeline_win_rate_table(sql, region, version):
    sql.set_cursor()
    sql.delete('lane_version_timeline_winrate', condition='version_id = %d and region_id = %d '%(version, region))
    sql.query("SELECT setval('lane_version_timeline_winrate_id_seq', (SELECT MAX(id) FROM lane_version_timeline_winrate)+1)")

    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, timeline_pita, timeline_count, timeline_origin \
        FROM timeline_count WHERE region_id = %d and version_id = %d ;'%(region, version))
    timeline_pita_dict = defaultdict(lambda : np.zeros(31))
    timeline_count_dict = defaultdict(lambda : np.zeros(31))
    timeline_origin_dict = defaultdict(lambda : np.zeros(31))
    for item in sql.cur.fetchall():
        (region_id, version_id, tier_id, champion_id, lane_id, timeline_pita, timeline_count, timeline_origin) = item
        timeline_pita = np.array(timeline_pita)
        timeline_count = np.array(timeline_count)
        timeline_origin = np.array(timeline_origin)
        for tier in tier_merge_dict[tier_id]:
            timeline_pita_dict[(region_id, version_id, tier, champion_id, lane_id)] += timeline_pita
            timeline_count_dict[(region_id, version_id, tier, champion_id, lane_id)] += timeline_count
            timeline_origin_dict[(region_id, version_id, tier, champion_id, lane_id)] += timeline_origin

    for k in timeline_pita_dict.keys():
        (region_id, version_id, tier_id, champion_id, lane_id) = k
        timeline_pita = timeline_pita_dict[k]
        timeline_count = timeline_count_dict[k]
        timeline_origin = timeline_origin_dict[k]
        timeline_pita = timeline_pita / (timeline_count + 1e-10)
        timeline_origin = timeline_origin / (timeline_count + 1e-10)
        timeline = str(time_norm(timeline_pita, timeline_origin))
        sql.insert('lane_version_timeline_winrate',
                  '(region_id, version_id, tier_id, champion_id, lane_id, timeline_winrate_list, updated_at)',
                    (region_id, version_id, tier_id, champion_id, lane_id, timeline, get_now()))
    sql.commit()
    sql.cur.close()

def get_key_tuples(sql, region, version):
    sql.set_cursor()
    sql.query('SELECT region_id, version_id, tier_id, lane_id, champion_id \
        FROM lane_version_winrate WHERE region_id = %d and version_id = %d;'%(region, version))
    keys = []
    for i, item in enumerate(sql.cur.fetchall()):
        keys.append(item)

    sql.cur.close()
    return keys


def get_base_winrate_info(sql, key):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    sql.query('SELECT win_rate, ban_rate, pick_rate, op_score, op_tier, ranking, count  \
        FROM lane_version_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d;'%key)

    item = sql.cur.fetchone()
    if item is None:
        return None

    win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count = item

    sql.query('SELECT op_score, ranking \
        FROM lane_version_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d;'%(region_id, version_id-1, tier_id, lane_id, champion_id))

    item = sql.cur.fetchone()
    if item is None:
        last_ps_score, last_ranking = 0, TOTAL_NUM_OF_CHAMPIONS
    else:
        last_ps_score, last_ranking = item

    sql.query('SELECT lane_id, count \
        FROM lane_version_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and champion_id = %d ORDER BY count DESC;'%(region_id, version_id, tier_id, champion_id))
    item = sql.cur.fetchall()
    ratio_arr = []
    total_count = 0
    for l, c in item:
        ratio_arr.append((l, c))
        total_count += c

    default_lane_arr = [0,1,2,3,4]
    top1_lane_id = ratio_arr[0][0]
    top1_lane_ratio = ratio_arr[0][1] / (total_count + 1e-10) * 100
    default_lane_arr.remove(top1_lane_id)
    if len(ratio_arr) > 1:
        top2_lane_id = ratio_arr[1][0]
        top2_lane_ratio = ratio_arr[1][1] / (total_count + 1e-10) * 100
        default_lane_arr.remove(top2_lane_id)
    else:
        top2_lane_id = default_lane_arr[0]
        top2_lane_ratio = 0
        default_lane_arr.remove(top2_lane_id)
    if len(ratio_arr) > 2:
        top3_lane_id = ratio_arr[2][0]
        top3_lane_ratio = ratio_arr[2][1] / (total_count + 1e-10) * 100
        default_lane_arr.remove(top3_lane_id)
    else:
        top3_lane_id = default_lane_arr[0]
        top3_lane_ratio = 0
        default_lane_arr.remove(top3_lane_id)
    if len(ratio_arr) > 3:
        top4_lane_id = ratio_arr[3][0]
        top4_lane_ratio = ratio_arr[3][1] / (total_count + 1e-10) * 100
        default_lane_arr.remove(top4_lane_id)
    else:
        top4_lane_id = default_lane_arr[0]
        top4_lane_ratio = 0
        default_lane_arr.remove(top4_lane_id)
    if len(ratio_arr) > 4:
        top5_lane_id = ratio_arr[4][0]
        top5_lane_ratio = ratio_arr[4][1] / (total_count + 1e-10) * 100
    else:
        top5_lane_id = default_lane_arr[0]
        top5_lane_ratio = 0

    sql.cur.close()
    ret = (win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking,
           top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio, top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio)
    return ret


def get_counter_winrate_info(sql, key):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    sql.query('SELECT counter_champion_id_list, counter_winrate_list \
        FROM counter_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return ('[]', '[]')
    else:
        counter_champion_id_list, counter_winrate_list = item
        counter_champion_id_list = str(eval(counter_champion_id_list)[:5])
        counter_winrate_list = str(eval(counter_winrate_list)[:5])
        sql.cur.close()
        return counter_champion_id_list, counter_winrate_list


def get_rune_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT rune_category1, rune_category2, pick_rate, win_rate, count, category1_rune_id_list, category2_rune_id_list \
            FROM rune_total_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT rune_category1, rune_category2, pick_rate, win_rate, count, category1_rune_id_list, category2_rune_id_list \
            FROM rune_total_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)
    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return (None, None, None, None, None, None, None, None, 0, 0, 0)
    else:
        main_rune_category, sub_rune_category, rune_total_pick_rate, rune_total_win_rate, rune_total_count, category1_rune_id_list, category2_rune_id_list = item
        main_rune1, main_rune2, main_rune3, main_rune4 = eval(category1_rune_id_list)
        sub_rune1, sub_rune2 = eval(category2_rune_id_list)
        ret = (main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, sub_rune1, sub_rune2, rune_total_pick_rate, rune_total_win_rate, rune_total_count)
        sql.cur.close()
        return ret

def get_statperk_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT statperk_id_list, pick_rate, win_rate, count \
            FROM statperk_total_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT statperk_id_list, pick_rate, win_rate, count \
            FROM statperk_total_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return (None, None, None)
    else:
        statperk_id_list, pick_rate, win_rate, count = item
        statperk1_id, statperk2_id, statperk3_id = eval(statperk_id_list)
        ret = (statperk1_id, statperk2_id, statperk3_id)
        sql.cur.close()
        return ret


def get_spell_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT spell1_id, spell2_id, pick_rate, win_rate, count \
            FROM spell_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT spell1_id, spell2_id, pick_rate, win_rate, count \
            FROM spell_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return (None, None)
    else:
        spell1_id, spell2_id, pick_rate, win_rate, count = item
        ret = (spell1_id, spell2_id)
        sql.cur.close()
        return ret


def get_skill_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT skill_name_list, pick_rate, win_rate, count \
            FROM skill_master_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT skill_name_list, pick_rate, win_rate, count \
            FROM skill_master_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return ('[]', 0, 0, 0, '[]')
    else:
        skill_master_list, skill_master_pickrate, skill_master_winrate, skill_master_count = item
        skill_master_list = skill_master_list.replace("'", '"')

        if build_type_id == 0:
            sql.query('SELECT skill_name_list \
                FROM skill_lv15_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
        else:
            sql.query('SELECT skill_name_list \
            FROM skill_lv15_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

        item = sql.cur.fetchone()
        if item is None:
            sql.cur.close()
            return (skill_master_list, skill_master_pickrate, skill_master_winrate, skill_master_count, '[]')
        else:
            skill_lv15_list = item[0]
            skill_lv15_list = skill_lv15_list.replace("'", '"')
            ret = (skill_master_list, skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list)
            sql.cur.close()
            return ret


def get_starting_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT item_id_list, pick_rate, win_rate, count \
            FROM item_starting_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT item_id_list, pick_rate, win_rate, count \
            FROM item_starting_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return ('[]', 0, 0, 0)
    else:
        starting_item_id_list, starting_pickrate, starting_winrate, starting_count = item
        ret = (starting_item_id_list, starting_pickrate, starting_winrate, starting_count)
        sql.cur.close()
        return ret


def get_shoes_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT item_id, pick_rate, win_rate, count \
            FROM item_shoes_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 1;'%key)
    else:
        sql.query('SELECT item_id, pick_rate, win_rate, count \
            FROM item_shoes_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 1;'%key)

    item = sql.cur.fetchone()
    if item is None:
        sql.cur.close()
        return None
    else:
        shoes_id, pick_rate, win_rate, count= item
        sql.cur.close()
        return shoes_id


def get_core_winrate_info(sql, key, build_type_id=0):
    region_id, version_id, tier_id, lane_id, champion_id = key
    sql.set_cursor()
    if build_type_id == 0:
        sql.query('SELECT item_id \
            FROM item_1st_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id \
            FROM item_1st_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item_1core_list = []
    item = sql.cur.fetchall()
    for k in item:
        item_1core_list.append(k[0])
    item_1core_list = str(item_1core_list)

    if build_type_id == 0:
        sql.query('SELECT item_id \
            FROM item_2nd_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id \
            FROM item_2nd_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item_2core_list = []
    item = sql.cur.fetchall()
    for k in item:
        item_2core_list.append(k[0])
    item_2core_list = str(item_2core_list)

    if build_type_id == 0:
        sql.query('SELECT item_id \
            FROM item_3rd_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id \
            FROM item_3rd_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item_3core_list = []
    item = sql.cur.fetchall()
    for k in item:
        item_3core_list.append(k[0])
    item_3core_list = str(item_3core_list)

    if build_type_id == 0:
        sql.query('SELECT item_id \
            FROM item_4th_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id \
            FROM item_4th_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item_4core_list = []
    item = sql.cur.fetchall()
    for k in item:
        item_4core_list.append(k[0])
    item_4core_list = str(item_4core_list)

    if build_type_id == 0:
        sql.query('SELECT item_id \
            FROM item_5th_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id \
            FROM item_5th_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item_5core_list = []
    item = sql.cur.fetchall()
    for k in item:
        item_5core_list.append(k[0])
    item_5core_list = str(item_5core_list)

    if build_type_id == 0:
        sql.query('SELECT item_id_list, pick_rate, win_rate, count \
            FROM item_three_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d ORDER BY pick_rate DESC LIMIT 3;'%key)
    else:
        sql.query('SELECT item_id_list, pick_rate, win_rate, count \
            FROM item_three_core_winrate WHERE region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and pick_rate > 5 ORDER BY win_rate DESC LIMIT 3;'%key)

    item = sql.cur.fetchall()
    top1_three_core_id_list = '[]'
    top1_three_core_winrate = 0
    top1_three_core_pickrate = 0
    top1_three_core_count = 0
    top2_three_core_id_list = '[]'
    top2_three_core_winrate = 0
    top2_three_core_pickrate = 0
    top2_three_core_count = 0
    top3_three_core_id_list = '[]'
    top3_three_core_winrate = 0
    top3_three_core_pickrate = 0
    top3_three_core_count = 0

    for i, k in enumerate(item):
        if i == 0:
            top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count = k
        if i == 1:
            top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count = k
        if i == 2:
            top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count = k

    sql.cur.close()
    ret = (item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list,
           top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count,
           top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count,
           top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count)
    return ret


def update_champion_summary_table(sql, region, version):
    keys = get_key_tuples(sql, region, version)
    print(len(keys))
    for i, key in enumerate(keys):
        region_id, version_id, tier_id, lane_id, champion_id = key
        ret_tup = get_base_winrate_info(sql, key)
        if ret_tup is None:
            continue
        (win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking,
         top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio,
         top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio) = ret_tup
        counter_champion_id_list, counter_winrate_list = get_counter_winrate_info(sql, key)

        build_type_id = 0
        main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count = get_rune_winrate_info(sql, key, build_type_id)
        statperk1_id, statperk2_id, statperk3_id = get_statperk_winrate_info(sql, key, build_type_id)
        spell1_id, spell2_id = get_spell_winrate_info(sql, key, build_type_id)
        skill_master_list, skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list = get_skill_winrate_info(sql, key, build_type_id)
        starting_item_id_list, starting_pickrate, starting_winrate, starting_count = get_starting_winrate_info(sql, key, build_type_id)
        shoes_id = get_shoes_winrate_info(sql, key, build_type_id)
        (item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list,
                   top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count,
                   top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count,
                   top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count) = get_core_winrate_info(sql, key, build_type_id)

        sql.set_cursor()
        sql.delete('champion_summary', condition='region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d'%key)
        sql.query("SELECT setval('champion_summary_id_seq', (SELECT MAX(id) FROM champion_summary)+1)")

        columns = '(region_id, version_id, tier_id, lane_id, champion_id, \
                 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking, \
                 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio, \
                 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio, \
                 counter_champion_id_list, counter_winrate_list, \
                 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, \
                 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count, \
                 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list, \
                 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list, \
                 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id, \
                 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list, \
                 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count, \
                 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count, \
                 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count, \
                 build_type_id, updated_at)'
        values = (region_id, version_id, tier_id, lane_id, champion_id,
                 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking,
                 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio,
                 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio,
                 counter_champion_id_list, counter_winrate_list,
                 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4,
                 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count,
                 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list,
                 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list,
                 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id,
                 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list,
                 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count,
                 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count,
                 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count,
                  build_type_id, get_now())

        sql.insert('champion_summary', columns, values)

        build_type_id = 1
        main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count = get_rune_winrate_info(sql, key, build_type_id)
        statperk1_id, statperk2_id, statperk3_id = get_statperk_winrate_info(sql, key, build_type_id)
        spell1_id, spell2_id = get_spell_winrate_info(sql, key, build_type_id)
        skill_master_list, skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list = get_skill_winrate_info(sql, key, build_type_id)
        starting_item_id_list, starting_pickrate, starting_winrate, starting_count = get_starting_winrate_info(sql, key, build_type_id)
        shoes_id = get_shoes_winrate_info(sql, key, build_type_id)
        (item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list,
                   top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count,
                   top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count,
                   top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count) = get_core_winrate_info(sql, key, build_type_id)

        sql.set_cursor()
        #sql.delete('champion_summary', condition='region_id = %d and version_id = %d and tier_id = %d and lane_id = %d and champion_id = %d and build_type_id = %d'%(*key, build_type_id))
        #sql.query("SELECT setval('champion_summary_id_seq', (SELECT MAX(id) FROM champion_summary)+1)")

        columns = '(region_id, version_id, tier_id, lane_id, champion_id, \
                 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking, \
                 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio, \
                 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio, \
                 counter_champion_id_list, counter_winrate_list, \
                 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, \
                 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count, \
                 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list, \
                 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list, \
                 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id, \
                 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list, \
                 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count, \
                 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count, \
                 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count, \
                 build_type_id, updated_at)'
        values = (region_id, version_id, tier_id, lane_id, champion_id,
                 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking,
                 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio,
                 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio,
                 counter_champion_id_list, counter_winrate_list,
                 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4,
                 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count,
                 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list,
                 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list,
                 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id,
                 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list,
                 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count,
                 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count,
                 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count,
                  build_type_id, get_now())

        sql.insert('champion_summary', columns, values)
        if i % 100 == 0:
            print(i)
    sql.commit()
    sql.cur.close()



