from ..utils import get_now

def get_rune_id_dict(sql):
	sql.set_cursor()
	sql.query('SELECT id, data_id FROM lol_rune')
	items = sql.cur.fetchall()
	rune_id_dict = dict()
	for item in items:
		rune_id, data_id = item
		rune_id_dict[int(data_id)] = int(rune_id)
	sql.cur.close()

	return rune_id_dict

def get_statperk_id_dict(sql):
	sql.set_cursor()
	sql.query('SELECT id, data_id, order_number FROM lol_rune WHERE is_fragment=true')
	items = sql.cur.fetchall()
	statperk_id_dict = dict()
	for item in items:
		statperk_id, data_id, order_num = item
		statperk_id_dict[int(data_id), order_num] = statperk_id
	sql.cur.close()

	return statperk_id_dict


def get_spell_id_dict(sql):
	sql.set_cursor()
	sql.query('SELECT id, data_key FROM lol_summonersspell')
	items = sql.cur.fetchall()
	spell_id_dict = dict()
	for item in items:
		spell_id, data_id = item
		spell_id_dict[int(data_id)] = int(spell_id)
	sql.cur.close()

	return spell_id_dict


def update_item_shoes(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate \
		FROM item_shoes_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_shoes_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_shoes_winrate_id_seq', (SELECT MAX(id) FROM item_shoes_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_shoes_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_starting(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
		FROM item_starting_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_starting_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_starting_winrate_id_seq', (SELECT MAX(id) FROM item_starting_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_starting_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_two_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
		FROM item_two_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_two_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_two_core_winrate_id_seq', (SELECT MAX(id) FROM item_two_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_two_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_item_three_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
		FROM item_three_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_three_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_three_core_winrate_id_seq', (SELECT MAX(id) FROM item_three_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_three_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_item_four_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
		FROM item_four_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_four_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_four_core_winrate_id_seq', (SELECT MAX(id) FROM item_four_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_four_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_item_five_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate \
		FROM item_five_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_five_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_five_core_winrate_id_seq', (SELECT MAX(id) FROM item_five_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_five_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id_list, count, pick_rate, win_rate, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_1st_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
		FROM item_1st_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_1st_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_1st_core_winrate_id_seq', (SELECT MAX(id) FROM item_1st_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_1st_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_2nd_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
		FROM item_2nd_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_2nd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_2nd_core_winrate_id_seq', (SELECT MAX(id) FROM item_2nd_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_2nd_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_3rd_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
		FROM item_3rd_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_3rd_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_3rd_core_winrate_id_seq', (SELECT MAX(id) FROM item_3rd_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_3rd_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_4th_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
		FROM item_4th_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_4th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_4th_core_winrate_id_seq', (SELECT MAX(id) FROM item_4th_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_4th_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_item_5th_core(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime \
		FROM item_5th_core_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('item_5th_core_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('item_5th_core_winrate_id_seq', (SELECT MAX(id) FROM item_5th_core_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('item_5th_core_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, item_id, count, pick_rate, win_rate, coretime, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_lane_date(sql_sync, sql_real, region, version, date):
	sql_sync.set_cursor()
	sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, date_id, counter_score \
		FROM lane_date_winrate WHERE region_id = %d and version_id = %d and date_id = %d;'%(region, version, date))

	sql_real.set_cursor()
	sql_real.delete('lane_date_winrate', condition='version_id = %d and region_id = %d and date_id = %d'%(version, region, date))
	sql_real.query("SELECT setval('lane_date_winrate_id_seq', (SELECT MAX(id) FROM lane_date_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('lane_date_winrate',
				  '(champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, date_id, counter_score, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_lane_version_timeline(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, timeline_winrate_list \
		FROM lane_version_timeline_winrate WHERE region_id = %d and version_id = %d;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('lane_version_timeline_winrate', condition='version_id = %d and region_id = %d'%(version, region))
	sql_real.query("SELECT setval('lane_version_timeline_winrate_id_seq', (SELECT MAX(id) FROM lane_version_timeline_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('lane_version_timeline_winrate',
				  '(champion_id, lane_id, version_id, region_id, tier_id, timeline_winrate_list, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_lane_version(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, \
		op_tier, ranking, ranking_variation, is_honey, is_op, counter_score, honey_score, revising_score, \
		overall_ranking, overall_ranking_variation \
		FROM lane_version_winrate WHERE region_id = %d and version_id = %d;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('lane_version_winrate', condition='version_id = %d and region_id = %d'%(version, region))
	sql_real.query("SELECT setval('lane_version_winrate_id_seq', (SELECT MAX(id) FROM lane_version_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('lane_version_winrate',
					'(champion_id, lane_id, version_id, region_id, tier_id, count, pick_rate, ban_rate, win_rate, op_score, \
						op_tier, ranking, ranking_variation, is_honey, is_op, counter_score, honey_score, revising_score, \
						overall_ranking, overall_ranking_variation, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_rune_main1(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_main1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_main1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_main1_winrate_id_seq', (SELECT MAX(id) FROM rune_main1_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_main1_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_rune_main2(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_main2_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_main2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_main2_winrate_id_seq', (SELECT MAX(id) FROM rune_main2_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_main2_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_rune_main3(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_main3_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_main3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_main3_winrate_id_seq', (SELECT MAX(id) FROM rune_main3_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_main3_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_rune_main4(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_main4_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_main4_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_main4_winrate_id_seq', (SELECT MAX(id) FROM rune_main4_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_main4_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_rune_sub1(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_sub1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_sub1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_sub1_winrate_id_seq', (SELECT MAX(id) FROM rune_sub1_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_sub1_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_rune_sub2(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate \
		FROM rune_sub2_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_sub2_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_sub2_winrate_id_seq', (SELECT MAX(id) FROM rune_sub2_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate = item
		sql_real.insert('rune_sub2_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_id, count, pick_rate, win_rate, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_id_dict[rune_id], count, pick_rate, win_rate, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_rune_total(sql_sync, sql_real, region, version):
	rune_id_dict = get_rune_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, count, pick_rate, win_rate, category1_rune_id_list, category2_rune_id_list \
		FROM rune_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('rune_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('rune_total_winrate_id_seq', (SELECT MAX(id) FROM rune_total_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, count, pick_rate, win_rate, category1_rune_id_list, category2_rune_id_list = item
		category1_rune_id_list = str([rune_id_dict[rune_id] for rune_id in eval(category1_rune_id_list)])
		category2_rune_id_list = str([rune_id_dict[rune_id] for rune_id in eval(category2_rune_id_list)])

		sql_real.insert('rune_total_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, rune_category1, rune_category2, count, pick_rate, win_rate, category1_rune_id_list, category2_rune_id_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, rune_category1,
					 rune_category2, count, pick_rate, win_rate, category1_rune_id_list,
					 category2_rune_id_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_lv1(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_lv1_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_lv1_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_lv1_winrate_id_seq', (SELECT MAX(id) FROM skill_lv1_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_lv1_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
						(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_lv3(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_lv3_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_lv3_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_lv3_winrate_id_seq', (SELECT MAX(id) FROM skill_lv3_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_lv3_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_lv6(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_lv6_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_lv6_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_lv6_winrate_id_seq', (SELECT MAX(id) FROM skill_lv6_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_lv6_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_lv11(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_lv11_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_lv11_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_lv11_winrate_id_seq', (SELECT MAX(id) FROM skill_lv11_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_lv11_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_lv15(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_lv15_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_lv15_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_lv15_winrate_id_seq', (SELECT MAX(id) FROM skill_lv15_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_lv15_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_skill_master(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list \
		FROM skill_master_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('skill_master_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('skill_master_winrate_id_seq', (SELECT MAX(id) FROM skill_master_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list = item
		skill_name_list = skill_name_list.replace("'", '"')
		sql_real.insert('skill_master_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, skill_name_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_spell(sql_sync, sql_real, region, version):
	spell_id_dict = get_spell_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, spell1_id, spell2_id \
		FROM spell_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('spell_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('spell_winrate_id_seq', (SELECT MAX(id) FROM spell_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, spell1_id, spell2_id = item
		spell1_id = spell_id_dict[spell1_id]
		spell2_id = spell_id_dict[spell2_id]
		sql_real.insert('spell_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, spell1_id, spell2_id, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate,
					 win_rate, spell1_id, spell2_id, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_statperk(sql_sync, sql_real, region, version):
	statperk_id_dict = get_statperk_id_dict(sql_real)
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list \
		FROM statperk_total_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('statperk_total_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('statperk_total_winrate_id_seq', (SELECT MAX(id) FROM statperk_total_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list = item
		statperk_id_list = str([statperk_id_dict[(sid, i)] for i, sid in enumerate(eval(statperk_id_list))])
		sql_real.insert('statperk_total_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list, updated_at)',
					(region_id, version_id, tier_id, champion_id, lane_id, count, pick_rate, win_rate, statperk_id_list, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_counter(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, champion_id, lane_id, counter_champion_id_list, counter_winrate_list, counter_count_list, counter_pickrate_list \
		FROM counter_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('counter_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('counter_winrate_id_seq', (SELECT MAX(id) FROM counter_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('counter_winrate',
				  '(region_id, version_id, tier_id, champion_id, lane_id, counter_champion_id_list, counter_winrate_list, counter_count_list, counter_pickrate_list, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()

def update_duo(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, duo_id, champion_id1, champion_id2, synergy_score, winrate1, winrate2, duo_winrate, count, pickrate, pickrate1, pickrate2 \
		FROM duo_synergy_winrate WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('duo_synergy_winrate', condition='version_id = %d and region_id = %d '%(version, region))
	sql_real.query("SELECT setval('duo_synergy_winrate_id_seq', (SELECT MAX(id) FROM duo_synergy_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('duo_synergy_winrate',
				  '(region_id, version_id, tier_id, duo_id, champion_id1, champion_id2, synergy_score, winrate1, winrate2, duo_winrate, count, pickrate, pickrate1, pickrate2, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_champion_summary(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT region_id, version_id, tier_id, lane_id, champion_id, \
				 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking, \
				 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio, \
				 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio, \
				 counter_champion_id_list, counter_winrate_list, \
				 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, \
				 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count, \
				 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list, \
				 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list, \
				 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id, \
				 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list, \
				 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count, \
				 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count, \
				 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count, \
				 build_type_id \
		FROM champion_summary WHERE region_id = %d and version_id = %d ;'%(region, version))

	sql_real.set_cursor()
	sql_real.delete('champion_summary', condition='version_id = %d and region_id = %d '%(version, region))
	#sql_real.query("SELECT setval('_id_seq', (SELECT MAX(id) FROM duo_synergy_winrate)+1)")

	for item in sql_sync.cur.fetchall():
		sql_real.insert('champion_summary',
				  '(region_id, version_id, tier_id, lane_id, champion_id, \
				 win_rate, ban_rate, pick_rate, ps_score, ps_tier, ranking, count, last_ps_score, last_ranking, \
				 top1_lane_id, top1_lane_ratio, top2_lane_id, top2_lane_ratio, top3_lane_id, top3_lane_ratio, \
				 top4_lane_id, top4_lane_ratio, top5_lane_id, top5_lane_ratio, \
				 counter_champion_id_list, counter_winrate_list, \
				 main_rune_category, sub_rune_category, main_rune1, main_rune2, main_rune3, main_rune4, \
				 sub_rune1, sub_rune2, rune_total_pickrate, rune_total_winrate, rune_total_count, \
				 statperk1_id, statperk2_id, statperk3_id, spell1_id, spell2_id, skill_master_list, \
				 skill_master_pickrate, skill_master_winrate, skill_master_count, skill_lv15_list, \
				 starting_item_id_list, starting_pickrate, starting_winrate, starting_count, shoes_id, \
				 item_1core_list, item_2core_list, item_3core_list, item_4core_list, item_5core_list, \
				 top1_three_core_id_list, top1_three_core_pickrate, top1_three_core_winrate, top1_three_core_count, \
				 top2_three_core_id_list, top2_three_core_pickrate, top2_three_core_winrate, top2_three_core_count, \
				 top3_three_core_id_list, top3_three_core_pickrate, top3_three_core_winrate, top3_three_core_count, \
				 build_type_id, updated_at)',
					(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


def update_probuild(sql_sync, sql_real, region, version):
	sql_sync.set_cursor()
	sql_sync.query('SELECT champion_id, lane_id \
		FROM lane_version_winrate WHERE region_id = %d and version_id = %d and tier_id = %d;'%(region, version, 2)) # platinum+

	for champion_id, lane_id in sql_sync.cur.fetchall():
		sql_sync.set_cursor()
		sql_sync.query('SELECT region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id, \
						level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list, \
						core_item_id_list, starting_item_id_list, item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list, \
						statperk_id_list, skill_name_list, cs_num, team_champion_id_list, is_pro, is_craft, pro_team_id, tier_id, is_academy, nickname \
			FROM probuild WHERE region_id = %d and version_id = %d and champion_id = %d and lane_id = %d LIMIT 300;'%(region, version, champion_id, lane_id))

		sql_real.set_cursor()
		sql_real.delete('probuild', condition='version_id = %d and region_id = %d and champion_id = %d and lane_id = %d'%(version, region, champion_id, lane_id))
		#sql_real.query("SELECT setval('_id_seq', (SELECT MAX(id) FROM duo_synergy_winrate)+1)")

		for item in sql_sync.cur.fetchall():
			sql_real.insert_ignore('probuild',
					'(region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id, \
						level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list, \
						core_item_id_list, starting_item_id_list, item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list, \
						statperk_id_list, skill_name_list, cs_num, team_champion_id_list, is_pro, is_craft, pro_team_id, tier_id, is_academy, nickname, updated_at)',
						'(champion_id, opponent_champion_id, game_id)',
						(*item, get_now()))
	sql_real.commit()
	sql_sync.cur.close()
	sql_real.cur.close()


