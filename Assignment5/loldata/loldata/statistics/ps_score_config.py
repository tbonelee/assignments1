from typing import List, Tuple, NamedTuple, TypedDict
from enum import IntEnum

NumOfGMMClusters = 15

class Tier(IntEnum):
	BSG = 1
	OVERPLATINUM = 2
	OVERMASTER = 3
	OVERDIAMOND = 13


class Lane(IntEnum):
	TOP = 0
	JUNGLE = 1
	MIDDLE = 2
	ADCARRY = 3
	SUPPORT = 4


class PSScoreWeight(TypedDict):
	winrate:float
	pickrate:float
	banrate:float
	count:int
	counter:int


class ReviseWeight(TypedDict):
	pickbanrate:float
	winrate:float
	pickrate:float
	
class ScoreConfig(NamedTuple):
	weights: float
	revise: bool
	revised_weight: float
	cut: bool


class PSScoreConfig:
	tier_weights_dict = {
		Tier.BSG: PSScoreWeight(
			winrate = 1, 
			pickrate = 1, 
			banrate = 0.15, 
			count = 0, 
			counter = 1), 
		Tier.OVERPLATINUM: PSScoreWeight(
			winrate = 1.3, 
			pickrate = 0.8, 
			banrate = 1.0, 
			count = 0, 
			counter = 1),
		Tier.OVERMASTER: PSScoreWeight(
			winrate = 0.7, 
			pickrate = 1, 
			banrate = 1, 
			count = 0, 
			counter = 1), 
		Tier.OVERDIAMOND: PSScoreWeight(
			winrate = 0.7, 
			pickrate = 1, 
			banrate = 1, 
			count = 0, 
			counter = 1),
	}
	tier_revise_dict = {
		Tier.BSG: False,
		Tier.OVERPLATINUM: True,
		Tier.OVERMASTER: False,
		Tier.OVERDIAMOND: True
	}
	
	tier_for_diff_dict = {
		Tier.BSG: None,
		Tier.OVERPLATINUM: Tier.OVERDIAMOND,
		Tier.OVERDIAMOND: Tier.OVERMASTER,
		Tier.OVERMASTER: None
	}

	tier_revise_weight_dict = {
		Tier.BSG: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
		Tier.OVERPLATINUM: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
		Tier.OVERMASTER: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
		Tier.OVERDIAMOND: ReviseWeight(pickbanrate=0.7, winrate=0, pickrate=0)
	}
	tier_cut_dict = {
		Tier.BSG: False,
		Tier.OVERPLATINUM: True,
		Tier.OVERMASTER: False,
		Tier.OVERDIAMOND: True
	}
	op_criterion_dict = {
		Lane.TOP: 5, 
		Lane.JUNGLE: 5.5, 
		Lane.MIDDLE: 6, 
		Lane.ADCARRY: 5, 
		Lane.SUPPORT: 5
	}

	@classmethod
	def get_configs(cls, tier_id) -> ScoreConfig:
		score_config = ScoreConfig(
			weights = cls.tier_weights_dict[tier_id],
			revise = cls.tier_revise_dict[tier_id],
			revise_weight = cls.tier_revise_weight_dict[tier_id],
			cut = cls.tier_cut_dict[tier_id])
		return score_config


class HoneyScoreConfig:
	tier_threshold_dict = {
		Tier.BSG: 15,
		Tier.OVERPLATINUM: 15,
		Tier.OVERMASTER: 20,
		Tier.OVERDIAMOND: 18
	}
	


# class HoneyScoreConfig:
# 	tier_weights_dict = {
# 		Tier.BSG: PSScoreWeight(
# 			winrate = 3, 
# 			pickrate = 0.2, 
# 			banrate = -0.2, 
# 			count = 0, 
# 			counter = 3), 
# 		Tier.OVERPLATINUM: PSScoreWeight(
# 			winrate = 3, 
# 			pickrate = 0.2, 
# 			banrate = -0.2, 
# 			count = 0, 
# 			counter = 3),
# 		Tier.OVERMASTER: PSScoreWeight(
# 			winrate = 3, 
# 			pickrate = 0.2, 
# 			banrate = -0.2, 
# 			count = 0, 
# 			counter = 3), 
# 		Tier.OVERDIAMOND: PSScoreWeight(
# 			winrate = 3, 
# 			pickrate = 0.2, 
# 			banrate = -0.2, 
# 			count = 0, 
# 			counter = 3),
# 	}
# 	tier_revise_dict = {
# 		Tier.BSG: True,
# 		Tier.OVERPLATINUM: True,
# 		Tier.OVERMASTER: False,
# 		Tier.OVERDIAMOND: True
# 	}
# 	tier_revise_weight_dict = {
# 		Tier.BSG: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
# 		Tier.OVERPLATINUM: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
# 		Tier.OVERMASTER: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
# 		Tier.OVERDIAMOND: ReviseWeight(pickbanrate=1, winrate=0, pickrate=0),
# 	}
# 	tier_cut_dict = {
# 		Tier.BSG: False,
# 		Tier.OVERPLATINUM: False,
# 		Tier.OVERMASTER: False,
# 		Tier.OVERDIAMOND: False,
# 	}
	
# 	@classmethod
# 	def get_configs(cls, tier_id) -> ScoreConfig:
# 		score_config = ScoreConfig(
# 			weights = cls.tier_weights_dict[tier_id],
# 			revise = cls.tier_revise_dict[tier_id],
# 			revise_weight = cls.tier_revise_weight_dict[tier_id],
# 			cut = cls.tier_cut_dict[tier_id])
# 		return score_config

