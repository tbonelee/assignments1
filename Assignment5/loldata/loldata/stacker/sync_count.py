import logging
import logging.config
import time
import pickle
import json
from collections import defaultdict
from datetime import datetime

import pytz
import numpy as np
from ..dto.summoner_dto import SummonerDTO
from ..db_handler.psql_handler import PSQLHandler
from ..algorithm.pitagonian import get_win_timeline_items, get_lose_timeline_items, get_timeline_arrs, pitagonian_one
from ..config import LogConfig

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("stacker")
summoner_dto = SummonerDTO()

def get_core_item_id_list():
    sql.set_cursor()
    sql.query('SELECT item_id FROM item_info WHERE is_core = True')
    items = sql.cur.fetchall()
    item_id_list = []
    for item in items:
        item_id_list.append(item[0])

    return item_id_list


def get_shoes_item_id_list():
    sql.set_cursor()
    sql.query('SELECT item_id FROM item_info WHERE is_shoes = True')
    items = sql.cur.fetchall()
    item_id_list = []
    for item in items:
        item_id_list.append(item[0])

    return item_id_list


def get_info_dict_from_sql(table, key_index=1, fn=lambda x:x, id_name=False):
    sql.set_cursor()
    sql.query('SELECT * FROM %s'%table)
    items = sql.cur.fetchall()
    ret_dict = dict()
    for item in items:
        key = fn(item[key_index])
        if id_name:
            ret_dict[item[0]] = key
        else:
            ret_dict[key] = item[0]
    return ret_dict


def date_str_to_datetime(d):
    return datetime.date(*[int(k) for k in (d.split('-'))])

def get_now():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

# lane_date_count
def update_lane_date_win_count(sql, region_id, version_id, champion_id, lane_id, date_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO lane_date_count (region_id, version_id, champion_id, lane_id, date_id, tier_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, date_id, tier_id) DO UPDATE SET win_count = lane_date_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, date_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

def update_lane_date_lose_count(sql, region_id, version_id, champion_id, lane_id, date_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO lane_date_count (region_id, version_id, champion_id, lane_id, date_id, tier_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, date_id, tier_id) DO UPDATE SET lose_count = lane_date_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, date_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

def update_lane_date_ban_count(sql, region_id, version_id, champion_id, date_id, tier_id):
    sql.set_cursor()
    for lane_id in range(5):
        query = 'INSERT INTO lane_date_count (region_id, version_id, champion_id, lane_id, date_id, tier_id, ban_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, date_id, tier_id) DO UPDATE SET ban_count = lane_date_count.ban_count + 1'
        params =  tuple([region_id, version_id, champion_id, lane_id, date_id, tier_id, 1])
        sql.query(query, params)
    sql.commit()

# counter_count
def update_counter_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id) DO UPDATE SET win_count = counter_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id) DO UPDATE SET lose_count = counter_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

# duo_count
def update_duo_win_count(sql, region_id, version_id, champion1_id, champion2_id, duo_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO duo_count (region_id, version_id, champion1_id, champion2_id, duo_id, tier_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, duo_id, tier_id) DO UPDATE SET win_count = duo_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, duo_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

def update_duo_lose_count(sql, region_id, version_id, champion1_id, champion2_id, duo_id, tier_id):
    sql.set_cursor()
    query = 'INSERT INTO duo_count (region_id, version_id, champion1_id, champion2_id, duo_id, tier_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, duo_id, tier_id) DO UPDATE SET lose_count = duo_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, duo_id, tier_id, 1])
    sql.query(query, params)
    sql.commit()

# joint_build_count
def update_joint_build_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1, first_skill_master_id, shoes_id, starting_item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO joint_build_count (region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1,  first_skill_master_id, shoes_id, starting_item_id_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1,  first_skill_master_id, shoes_id, starting_item_id_list) DO UPDATE SET win_count = joint_build_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1, first_skill_master_id, shoes_id, starting_item_id_list, 1])
    sql.query(query, params)
    sql.commit()

def update_joint_build_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1, first_skill_master_id, shoes_id, starting_item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO joint_build_count (region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1,  first_skill_master_id, shoes_id, starting_item_id_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1,  first_skill_master_id, shoes_id, starting_item_id_list) DO UPDATE SET lose_count = joint_build_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, core_id, rune_main1, first_skill_master_id, shoes_id, starting_item_id_list, 1])
    sql.query(query, params)
    sql.commit()

# item_core_count
#def update_item_core_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list):
#    sql.set_cursor()
#    query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
#    query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET win_count = item_core_count.win_count + 1'
#    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, core_id_list, 1])
#    sql.query(query, params)
#    sql.commit()


def update_item_core_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list):
    sql.set_cursor()
    query = 'SELECT win_count, lose_count, coretime_list FROM item_core_count WHERE region_id=%s and version_id=%s and champion_id=%s and lane_id=%s and tier_id=%s and core_id_list=%s'
    params = (region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list))
    sql.query(query, params)
    item = sql.cur.fetchone()
    if item is None:
        sql.set_cursor()
        query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET win_count = item_core_count.win_count + 1, coretime_list = %s'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list), str(coretime_list), 1, str(coretime_list)])
        sql.query(query, params)
        sql.commit()

    else:
        win_count, lose_count, before_coretime_list = item
        if before_coretime_list is None:
            coretime_list = list(coretime_list)
        else:
            before_coretime_list = np.array(eval(before_coretime_list))
            coretime_list = (before_coretime_list * (win_count + lose_count) + np.array(coretime_list)) / (win_count + lose_count + 1)
            coretime_list = [round(v) for v in coretime_list]

        sql.set_cursor()
        query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET coretime_list=%s, win_count = item_core_count.win_count + 1'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list), str(coretime_list), 1, str(coretime_list)])
        sql.query(query, params)
        sql.commit()


#def update_item_core_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list):
#    sql.set_cursor()
#    query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
#    query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET lose_count = item_core_count.lose_count + 1'
#    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, core_id_list, 1])
#    sql.query(query, params)
#    sql.commit()

def update_item_core_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list):
    sql.set_cursor()
    query = 'SELECT win_count, lose_count, coretime_list FROM item_core_count WHERE region_id=%s and version_id=%s and champion_id=%s and lane_id=%s and tier_id=%s and core_id_list=%s'
    params = (region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list))
    sql.query(query, params)
    item = sql.cur.fetchone()
    if item is None:
        sql.set_cursor()
        query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET lose_count = item_core_count.lose_count + 1, coretime_list=%s'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list), str(coretime_list), 1, str(coretime_list)])
        sql.query(query, params)
        sql.commit()
    else:
        win_count, lose_count, before_coretime_list = item
        if before_coretime_list is None:
            coretime_list = list(coretime_list)
        else:
            before_coretime_list = np.array(eval(before_coretime_list))
            coretime_list = (before_coretime_list * (win_count + lose_count) + np.array(coretime_list)) / (win_count + lose_count + 1)
            coretime_list = [round(v) for v in coretime_list]

        sql.set_cursor()
        query = 'INSERT INTO item_core_count (region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id, core_id_list) DO UPDATE SET coretime_list=%s, lose_count = item_core_count.lose_count + 1'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list), str(coretime_list), 1, str(coretime_list)])
        sql.query(query, params)
        sql.commit()

# item_starting_count
def update_item_starting_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO item_starting_count (region_id, version_id, champion_id, lane_id, tier_id, item_id_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, item_id_list) DO UPDATE SET win_count = item_starting_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, item_id_list, 1])
    sql.query(query, params)
    sql.commit()

def update_item_starting_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO item_starting_count (region_id, version_id, champion_id, lane_id, tier_id, item_id_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, item_id_list) DO UPDATE SET lose_count = item_starting_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, item_id_list, 1])
    sql.query(query, params)
    sql.commit()

# item_shoes_count
def update_item_shoes_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, item_id):
    sql.set_cursor()
    query = 'INSERT INTO item_shoes_count (region_id, version_id, champion_id, lane_id, tier_id, item_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, item_id) DO UPDATE SET win_count = item_shoes_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, item_id, 1])
    sql.query(query, params)
    sql.commit()

def update_item_shoes_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, item_id):
    sql.set_cursor()
    query = 'INSERT INTO item_shoes_count (region_id, version_id, champion_id, lane_id, tier_id, item_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, item_id) DO UPDATE SET lose_count = item_shoes_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, item_id, 1])
    sql.query(query, params)
    sql.commit()

# rune_count
def update_rune_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2):
    sql.set_cursor()
    query = 'INSERT INTO rune_count (region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2) DO UPDATE SET win_count = rune_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, 1])
    sql.query(query, params)
    sql.commit()

def update_rune_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2):
    sql.set_cursor()
    query = 'INSERT INTO rune_count (region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2) DO UPDATE SET lose_count = rune_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, 1])
    sql.query(query, params)
    sql.commit()

# skill_count
def update_skill_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, skill_list):
    sql.set_cursor()
    query = 'INSERT INTO skill_count (region_id, verison_id, champion_id, lane_id, tier_id, skill_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, verison_id, champion_id, lane_id, tier_id, skill_list) DO UPDATE SET win_count = skill_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, skill_list, 1])
    sql.query(query, params)
    sql.commit()

def update_skill_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, skill_list):
    sql.set_cursor()
    query = 'INSERT INTO skill_count (region_id, verison_id, champion_id, lane_id, tier_id, skill_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, verison_id, champion_id, lane_id, tier_id, skill_list) DO UPDATE SET lose_count = skill_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, skill_list, 1])
    sql.query(query, params)
    sql.commit()

# spell_count
def update_spell_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id):
    sql.set_cursor()
    query = 'INSERT INTO spell_count (region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id) DO UPDATE SET win_count = spell_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id, 1])
    sql.query(query, params)
    sql.commit()

def update_spell_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id):
    sql.set_cursor()
    query = 'INSERT INTO spell_count (region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id) DO UPDATE SET lose_count = spell_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id, 1])
    sql.query(query, params)
    sql.commit()

# statperk_count
def update_statperk_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id):
    sql.set_cursor()
    query = 'INSERT INTO statperk_count (region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id) DO UPDATE SET win_count = statperk_count.win_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, 1])
    sql.query(query, params)
    sql.commit()

def update_statperk_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id):
    sql.set_cursor()
    query = 'INSERT INTO statperk_count (region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id) DO UPDATE SET lose_count = statperk_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, 1])
    sql.query(query, params)
    sql.commit()

def update_duo_count(sql, region_id, version_id, tier_id, tmp_lane_pairs):
    top_win_champion_id = tmp_lane_pairs[0][0][0]
    jg_win_champion_id = tmp_lane_pairs[1][0][0]
    mid_win_champion_id = tmp_lane_pairs[2][0][0]
    ad_win_champion_id = tmp_lane_pairs[3][0][0]
    sup_win_champion_id = tmp_lane_pairs[4][0][0]
    top_lose_champion_id = tmp_lane_pairs[0][1][0]
    jg_lose_champion_id = tmp_lane_pairs[1][1][0]
    mid_lose_champion_id = tmp_lane_pairs[2][1][0]
    ad_lose_champion_id = tmp_lane_pairs[3][1][0]
    sup_lose_champion_id = tmp_lane_pairs[4][1][0]

    update_duo_win_count(sql, region_id, version_id, ad_win_champion_id, sup_win_champion_id, 0, tier_id) # ad -sup
    update_duo_lose_count(sql, region_id, version_id, ad_lose_champion_id, sup_lose_champion_id, 0, tier_id) # ad -sup
    update_duo_win_count(sql, region_id, version_id, mid_win_champion_id, jg_win_champion_id, 1, tier_id) # mid-jg
    update_duo_lose_count(sql, region_id, version_id, mid_lose_champion_id, jg_lose_champion_id, 1, tier_id) # mid-jg
    update_duo_win_count(sql, region_id, version_id, top_win_champion_id, jg_win_champion_id, 2, tier_id) # top-jg
    update_duo_lose_count(sql, region_id, version_id, top_lose_champion_id, jg_lose_champion_id, 2, tier_id) # top-jg
    update_duo_win_count(sql, region_id, version_id, jg_win_champion_id, sup_win_champion_id, 3, tier_id) # jg-sup
    update_duo_lose_count(sql, region_id, version_id, jg_lose_champion_id, sup_lose_champion_id, 3, tier_id) # jg-sup


# counter_item_core_count
def update_counter_item_core_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_core_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list) DO UPDATE SET win_count = counter_item_core_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, str(item_id_list), 1])
    sql.query(query, params)
    sql.commit()

def update_counter_item_core_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_core_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list) DO UPDATE SET lose_count = counter_item_core_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, str(item_id_list), 1])
    sql.query(query, params)
    sql.commit()

# counter_item_shoes_count
def update_counter_item_shoes_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_shoes_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id) DO UPDATE SET win_count = counter_item_shoes_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_item_shoes_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_shoes_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id) DO UPDATE SET lose_count = counter_item_shoes_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id, 1])
    sql.query(query, params)
    sql.commit()

# counter_item_starting_count
def update_counter_item_starting_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_starting_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list) DO UPDATE SET win_count = counter_item_starting_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_item_starting_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_item_starting_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list) DO UPDATE SET lose_count = counter_item_starting_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, item_id_list, 1])
    sql.query(query, params)
    sql.commit()

# counter_rune_count
def update_counter_rune_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2):
    sql.set_cursor()
    query = 'INSERT INTO counter_rune_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2) DO UPDATE SET win_count = counter_rune_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_rune_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2):
    sql.set_cursor()
    query = 'INSERT INTO counter_rune_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2) DO UPDATE SET lose_count = counter_rune_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, rune_main1, rune_main2, rune_main3, rune_main4, rune_sub1, rune_sub2, rune_category1, rune_category2, 1])
    sql.query(query, params)
    sql.commit()

# counter_spell_count
def update_counter_spell_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_spell_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id) DO UPDATE SET win_count = counter_spell_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_spell_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_spell_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id) DO UPDATE SET lose_count = counter_spell_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id, spell2_id, 1])
    sql.query(query, params)
    sql.commit()

# counter_statperk_count
def update_counter_statperk_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_statperk_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id,statperk3_id, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id) DO UPDATE SET win_count = counter_statperk_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_statperk_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id):
    sql.set_cursor()
    query = 'INSERT INTO counter_statperk_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id,statperk3_id, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id) DO UPDATE SET lose_count = counter_statperk_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id, 1])
    sql.query(query, params)
    sql.commit()

# counter_skill_count
def update_counter_skill_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_skill_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list, win_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list) DO UPDATE SET win_count = counter_skill_count.win_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list, 1])
    sql.query(query, params)
    sql.commit()

def update_counter_skill_lose_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list):
    sql.set_cursor()
    query = 'INSERT INTO counter_skill_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list, lose_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
    query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list) DO UPDATE SET lose_count = counter_skill_count.lose_count + 1'
    params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list, 1])
    sql.query(query, params)
    sql.commit()

def get_str_for_psql(arr):
    arr = [round(v, 4) for v in arr]
    return str(arr).replace('[','{').replace(']','}').replace(' ','')

# timeline_count
def update_timeline_count(sql, region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin):
    sql.set_cursor()
    query = 'SELECT timeline_pita, timeline_count, timeline_origin FROM timeline_count WHERE region_id=%s and version_id=%s and champion_id=%s and lane_id=%s and tier_id=%s'
    params = (region_id, version_id, champion_id, lane_id, tier_id)
    sql.query(query, params)
    item = sql.cur.fetchone()
    if item is None:
        timeline_pita = get_str_for_psql(timeline_pita[0])
        timeline_count = get_str_for_psql(timeline_count[0])
        timeline_origin = get_str_for_psql(timeline_origin[0])
        sql.set_cursor()
        query = 'INSERT INTO timeline_count (region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id) DO UPDATE SET timeline_pita=%s, timeline_count=%s, timeline_origin=%s'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin, timeline_pita, timeline_count, timeline_origin])
        sql.query(query, params)
        sql.commit()

    else:
        pita, count, origin = item
        pita = np.array(pita)
        count = np.array(count)
        origin = np.array(origin)
        timeline_pita = get_str_for_psql(pita + timeline_pita[0])
        timeline_count = get_str_for_psql(count + timeline_count[0])
        timeline_origin = get_str_for_psql(origin + timeline_origin[0])

        sql.set_cursor()
        query = 'INSERT INTO timeline_count (region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion_id, lane_id, tier_id) DO UPDATE SET timeline_pita=%s, timeline_count=%s, timeline_origin=%s'
        params =  tuple([region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin, timeline_pita, timeline_count, timeline_origin])
        sql.query(query, params)
        sql.commit()


# counter_timeline_count
def update_counter_timeline_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin):
    sql.set_cursor()
    query = 'SELECT timeline_pita, timeline_count, timeline_origin FROM counter_timeline_count WHERE region_id=%s and version_id=%s and champion1_id=%s and champion2_id=%s and lane_id=%s and tier_id=%s'
    params = (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id)
    sql.query(query, params)
    item = sql.cur.fetchone()
    if item is None:
        timeline_pita = get_str_for_psql(timeline_pita[0])
        timeline_count = get_str_for_psql(timeline_count[0])
        timeline_origin = get_str_for_psql(timeline_origin[0])
        sql.set_cursor()
        query = 'INSERT INTO counter_timeline_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id) DO UPDATE SET timeline_pita=%s, timeline_count=%s, timeline_origin=%s'
        params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin, timeline_pita, timeline_count, timeline_origin])
        sql.query(query, params)
        sql.commit()

    else:
        pita, count, origin = item
        pita = np.array(pita)
        count = np.array(count)
        origin = np.array(origin)
        timeline_pita = get_str_for_psql(pita + timeline_pita[0])
        timeline_count = get_str_for_psql(count + timeline_count[0])
        timeline_origin = get_str_for_psql(origin + timeline_origin[0])

        sql.set_cursor()
        query = 'INSERT INTO counter_timeline_count (region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT '
        query += '(region_id, version_id, champion1_id, champion2_id, lane_id, tier_id) DO UPDATE SET timeline_pita=%s, timeline_count=%s, timeline_origin=%s'
        params =  tuple([region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin, timeline_pita, timeline_count, timeline_origin])
        sql.query(query, params)
        sql.commit()


def update_counter_timeline_count_from_item(sql, item, lane_pred, region_id, version_id, tier_id, gold_rat=5, object_check=1):

    def _get_pid_cid_dict(item, lane_pred):
        ret_dict = {'win':{}, 'lose':{}}
        for tt in item['teams']['win']['participants']:
            pid = tt['stats']['participantId']
            cid = tt['championId']
            lane_id = lane_dict[lane_pred[str(pid)]]
            ret_dict['win'][lane_id] = (pid, cid)
        for tt in item['teams']['lose']['participants']:
            pid = tt['stats']['participantId']
            cid = tt['championId']
            lane_id = lane_dict[lane_pred[str(pid)]]
            ret_dict['lose'][lane_id] = (pid, cid)
        return ret_dict

    pid_cid_dict = _get_pid_cid_dict(item, lane_pred)
    for lane_id in range(5):
        win_participant_id, win_champion_id = pid_cid_dict['win'][lane_id]
        lose_participant_id, lose_champion_id = pid_cid_dict['lose'][lane_id]

        win_team_array, lose_team_array = pitagonian_one(item, win_participant_id, lose_participant_id,
                                                         gold_rat=gold_rat,object_check=object_check)
        period_array=np.ones((1,len(win_team_array[0])), dtype=object)
        timeline_pita_win, timeline_count_win, timeline_origin_win = get_win_timeline_items(win_team_array, period_array)
        timeline_pita_lose, timeline_count_lose, timeline_origin_lose = get_lose_timeline_items(lose_team_array, period_array)

        update_counter_timeline_count(sql, region_id, version_id, win_champion_id, lose_champion_id, lane_id, tier_id,
                                      timeline_pita_win, timeline_count_win, timeline_origin_win)
        update_counter_timeline_count(sql, region_id, version_id, lose_champion_id, win_champion_id, lane_id, tier_id,
                                      timeline_pita_lose, timeline_count_lose, timeline_origin_lose)


def timestamp_to_str(timestamp, locale='Asia/Seoul'):
    timezone = pytz.timezone(locale)
    ts = datetime.fromtimestamp(timestamp // 1000)
    ts = ts.astimezone(timezone)
    return ts.strftime('%Y-%m-%d %H:%M')

def str_to_timestamp(time_str, locale='Asia/Seoul'):

    timezone = pytz.timezone(locale)
    date_time_obj = datetime.strptime(time_str, '%Y-%m-%d %H:%M')
    timezone_date_time_obj = timezone.localize(date_time_obj)
    return int(timezone_date_time_obj.timestamp() * 1000)

def update_version_id(sql, version_id, version):
    sql.set_cursor()
    query = 'INSERT INTO version_info (version_id, description, patch_date, updated_at) VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING'
    params =  tuple([version_id, version, get_now(), get_now()])
    sql.query(query, params)
    sql.commit()

def get_version_id(sql, version):
    sql.set_cursor()
    sql.query('SELECT version_id from version_info')
    for item in sql.cur.fetchall():
        version_id = item[0]
    return version_id + 1

def get_date_id(timestamp):
    start = str_to_timestamp('2020-10-01 10:00')
    start_date_id = 64
    date_diff = (timestamp - start) // (1000 * 60 * 60 * 24)
    date_id = start_date_id + date_diff
    return date_id

def update_date_id(sql, date_id, date):
    sql.set_cursor()
    query = 'INSERT INTO date_info (date_id, date) VALUES (%s, %s) ON CONFLICT DO NOTHING'
    params =  tuple([date_id, date])
    sql.query(query, params)
    sql.commit()

def get_date_str(date_id):
    start = str_to_timestamp('2020-10-01 10:00')
    start_date_id = 64
    date_diff = date_id - start_date_id
    return timestamp_to_str(start + date_diff * 24 * 60 * 60 * 1000).split(' ')[0]


def is_core_item(item_id):
    if item_id in core_item_id_list:
        return True
    else:
        return False

def is_shoes_item(item_id):
    if item_id in shoes_id_list:
        return True
    else:
        return False


def get_shoes_id(item_logs, item_timestamp_logs):
    shoes = None
    for item, timestamp in zip(item_logs, item_timestamp_logs):
        if is_shoes_item(item) and item > 0:
            shoes = item
            break
    return shoes

def get_core_id_list(item_logs, item_timestamp_logs):
    core = []
    coretime = []
    for item, timestamp in zip(item_logs, item_timestamp_logs):
        if is_core_item(item) and item > 0:
            length = len(core)
            if (length > 0 and core[length-1] != item) or length == 0:
                core.append(item)
                coretime.append(timestamp // 1000)
                if len(core) >= 5:
                    break
    return core, coretime

def get_starting_id_list(item_logs, item_timestamp_logs):
    starting = []
    for item, timestamp in zip(item_logs, item_timestamp_logs):
        if timestamp < 60000 and item > 0 and item not in [3340,3363,3364]:
            starting.append(item)
    return sorted(starting)


def get_rune_id_list(stats):
    runes=[]
    rune_category1 = stats['perkPrimaryStyle']
    rune_category2 = stats['perkSubStyle']
    for i in range(4):
        idd=str('perk'+str(i))
        runes.append(stats[str(idd)])

    runes.append(min(stats['perk4'], stats['perk5']))
    runes.append(max(stats['perk4'], stats['perk5']))

    return runes, rune_category1, rune_category2


def get_statperk_id_list(stats):
    statperk1_id = stats['statPerk0']
    statperk2_id = stats['statPerk1']
    statperk3_id = stats['statPerk2']
    return statperk1_id, statperk2_id, statperk3_id


def get_skill_list(skill_logs):
    skill_list = skill_logs[:18]
    return skill_list


def get_first_master_skill_id(skill_list):
    master_arr = np.zeros(5)
    for i in skill_list:
        master_arr[i] += 1
        if master_arr[i] >= 5:
            return i
    return None


def get_time(timestamp):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timestamp))


def update_probuild(sql, region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                         level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                         core_item_id_list, starting_item_id_list, item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                         statperk_id_list, skill_name_list, cs_num, team_champion_id_list, is_pro, is_craft, pro_team_id, tier_id, is_academy, nickname):
    sql.set_cursor()
    query = 'INSERT INTO probuild (region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id, \
                         level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list, \
                         core_item_id_list, starting_item_id_list, item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list, \
                         statperk_id_list, skill_name_list, cs_num, team_champion_id_list, is_pro, is_craft, pro_team_id, tier_id, is_academy, nickname, updated_at) \
                         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
    params =  tuple([region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                         level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                         core_item_id_list, starting_item_id_list, item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                         statperk_id_list, skill_name_list, cs_num, team_champion_id_list, is_pro, is_craft, pro_team_id, tier_id, is_academy, nickname, get_now()])
    sql.query(query, params)
    sql.commit()


def get_champion_id(item, lane_pred, result='win', target_lane_id=0):
    for p in item['teams'][result]['participants']:
        lane_id = lane_dict[str(lane_pred[str(p['stats']['participantId'])])]
        champion_id = p['championId']
        if lane_id == target_lane_id:
            return champion_id


def get_item_timeline(item_logs, item_timestamp_logs):
    item_timeline = defaultdict(list)
    for item_id, item_timestamp in zip(item_logs, item_timestamp_logs):
        if item_id > 0:
            item_timeline[round(item_timestamp/60000)].append(item_id)
    return json.dumps(dict(item_timeline))


def get_skill_name(arr):
    d = {
        1:'Q',
        2:'W',
        3:'E',
        4:'R',
    }
    return [d[k] for k in arr]


async def update_tables(sql, mds, item, lane_pred):
    region_id = 0
    max_len = 31
    tier_id = tier_dict[item['tier']]
    if item['version'] in version_dict:
        version_id = version_dict[item['version']]
    else:
        version_id = get_version_id(sql, item['version'])
        update_version_id(sql, version_id, item['version'])

    date_id = get_date_id(item['timestamp'])
    update_date_id(sql, date_id, get_date_str(date_id))
    game_id = item['game_id']
    timestamp = get_time(int(item['timestamp'])//1000)
    duration = item['game_duration']

    win_team_array, lose_team_array, period_array = get_timeline_arrs(item=item, gold_rat=5,object_check=1)
    update_counter_timeline_count_from_item(sql, item, lane_pred, region_id, version_id, tier_id, gold_rat=5, object_check=1)

    team_champion_id_list = [{'win':True, 'champion_id_list':[0,0,0,0,0]},
                             {'win':False, 'champion_id_list':[0,0,0,0,0]}]
    for participant in item['teams']['win']['participants']:
        champion_id = int(participant['championId'])
        participant_id = str(participant['stats']['participantId'])
        lane_id = lane_dict[lane_pred[participant_id]]
        team_champion_id_list[0]['champion_id_list'][lane_id] = champion_id

    for participant in item['teams']['lose']['participants']:
        champion_id = int(participant['championId'])
        participant_id = str(participant['stats']['participantId'])
        lane_id = lane_dict[lane_pred[participant_id]]
        team_champion_id_list[1]['champion_id_list'][lane_id] = champion_id


    tmp_lane_pairs = dict()
    for participant in item['teams']['win']['participants']:
        core_id_list = []
        shoes_id = None
        starting_id_list = []
        skill_list = []
        champion_id = int(participant['championId'])
        summoner_id = participant['summonerId']
        participant_id = str(participant['stats']['participantId'])
        lane_id = lane_dict[lane_pred[participant_id]]
        update_lane_date_win_count(sql, region_id, version_id, champion_id, lane_id, date_id, tier_id) # ok
        runes, rune_category1, rune_category2 = get_rune_id_list(participant['stats'])
        update_rune_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, runes[0], runes[1], runes[2], runes[3], runes[4], runes[5], rune_category1, rune_category2) # ok
        if participant_id in item['item_logs']:
            item_logs = item['item_logs'][participant_id]
            item_timestamp_logs = item['item_timestamp_logs'][participant_id]
            core_id_list, coretime_list = get_core_id_list(item_logs, item_timestamp_logs)
            if len(core_id_list) > 0:
                #update_item_core_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list)) # ok
                update_item_core_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list) # ok
            shoes_id = get_shoes_id(item_logs, item_timestamp_logs)
            if shoes_id is not None:
                update_item_shoes_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, shoes_id) # ok
            starting_id_list = get_starting_id_list(item_logs, item_timestamp_logs)
            if len(starting_id_list) > 0:
                update_item_starting_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(starting_id_list)) # ok
            item_timeline = get_item_timeline(item_logs, item_timestamp_logs)

        if participant_id in item['skill_logs']:
            skill_logs = item['skill_logs'][participant_id]
            skill_list = get_skill_list(skill_logs)
            update_skill_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(skill_list))
            if len(core_id_list) > 0 and shoes_id is not None and len(starting_id_list) > 0:
                first_skill_master_id = get_first_master_skill_id(skill_list)
                if first_skill_master_id is not None:
                    update_joint_build_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list[0], runes[0], first_skill_master_id, shoes_id, str(starting_id_list)) # ok

        spell1_id, spell2_id = sorted(participant['spells'])
        update_spell_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id)
        statperk1_id, statperk2_id, statperk3_id = get_statperk_id_list(participant['stats'])
        update_statperk_win_count(sql, region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id)
        tmp_lane_pairs[lane_id] = [(champion_id, str(core_id_list), str(starting_id_list), shoes_id, (runes, rune_category1, rune_category2), str(skill_list), (spell1_id, spell2_id), (statperk1_id, statperk2_id, statperk3_id))]
        timeline_pita, timeline_count, timeline_origin = get_win_timeline_items(win_team_array, period_array, max_len)
        update_timeline_count(sql, region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin)

        if summoner_id in pro_summoner_id_dict and participant_id in item['item_logs'] and participant_id in item['skill_logs']:
            logger.info('pro data')
            nickname, team_name, summoner_name, pro_team_id, pro_or_academy = pro_summoner_id_dict[summoner_id]
            logger.warning("pro, win, %s"%summoner_name)
            if pro_or_academy == 'pro':
                is_pro = 1
                is_academy = 0
                is_craft = 0
            elif pro_or_academy == 'academy':
                is_pro = 0
                is_academy = 1
                is_craft = 0
            result = 1 # win
            summoner_name = participant['summonerName']
            opponent_champion_id = get_champion_id(item, lane_pred, result='lose' if result==1 else 'win', target_lane_id=lane_id)
            level = participant['stats']['champLevel']
            kills = participant['stats']['kills']
            deaths = participant['stats']['deaths']
            assists = participant['stats']['assists']
            gold = participant['stats']['goldEarned']
            damage_dealt = participant['stats']['totalDamageDealt']
            damage_taken = participant['stats']['totalDamageTaken']
            final_item_id_list = str(list(participant['items']))
            cs_num = participant['stats']['totalMinionsKilled']
            main_rune_id_list = str(runes[:4])
            sub_rune_id_list = str(runes[4:])
            statperk_id_list = str([statperk1_id, statperk2_id, statperk3_id])
            spell1_id, spell2_id = participant['spells'] # not sorted
            tier_name, rank_name = await summoner_dto.get_tier_name_from_summoner_summary(mds, summoner_id)
            real_tier_id = tier_dict[tier_name]
            update_probuild(sql, region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                 level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                 str(core_id_list), str(starting_id_list), item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                 statperk_id_list, str(get_skill_name(skill_list)), cs_num, str(team_champion_id_list).replace("'", '"'), is_pro, is_craft, pro_team_id, real_tier_id, is_academy, nickname)
            logger.warning("pro, win, update")


        elif summoner_id in craft_dict[(champion_id, lane_id)] and participant_id in item['item_logs'] and participant_id in item['skill_logs']:
            result = 1 # win
            summoner_name = participant['summonerName']
            logger.warning("craft, win, %s"%summoner_name)
            opponent_champion_id = get_champion_id(item, lane_pred, result='lose' if result==1 else 'win', target_lane_id=lane_id)
            level = participant['stats']['champLevel']
            kills = participant['stats']['kills']
            deaths = participant['stats']['deaths']
            assists = participant['stats']['assists']
            gold = participant['stats']['goldEarned']
            damage_dealt = participant['stats']['totalDamageDealt']
            damage_taken = participant['stats']['totalDamageTaken']
            final_item_id_list = str(list(participant['items']))
            cs_num = participant['stats']['totalMinionsKilled']
            main_rune_id_list = str(runes[:4])
            sub_rune_id_list = str(runes[4:])
            statperk_id_list = str([statperk1_id, statperk2_id, statperk3_id])
            spell1_id, spell2_id = participant['spells'] # not sorted
            is_pro = 0
            is_craft = 1
            is_academy = 0
            pro_team_id = -1
            tier_name, rank_name = await summoner_dto.get_tier_name_from_summoner_summary(mds, summoner_id)
            real_tier_id = tier_dict[tier_name]
            if tier_name in ['MASTER', 'GRANDMASTER', 'CHALLENGER']:
                nickname = tier_name
            else:
                nickname = tier_name +' '+ rank_name
            update_probuild(sql, region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                 level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                 str(core_id_list), str(starting_id_list), item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                 statperk_id_list, str(get_skill_name(skill_list)), cs_num, str(team_champion_id_list).replace("'", '"'), is_pro, is_craft, pro_team_id, real_tier_id, is_academy, nickname)
            logger.warning("craft, win, update")


    for participant in item['teams']['lose']['participants']:
        core_id_list = []
        shoes_id = None
        starting_id_list = []
        skill_list = []
        champion_id = int(participant['championId'])
        summoner_id = participant['summonerId']
        participant_id = str(participant['stats']['participantId'])
        lane_id = lane_dict[lane_pred[participant_id]]
        update_lane_date_lose_count(sql, region_id, version_id, champion_id, lane_id, date_id, tier_id) # ok
        runes, rune_category1, rune_category2 = get_rune_id_list(participant['stats'])
        update_rune_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, runes[0], runes[1], runes[2], runes[3], runes[4], runes[5], rune_category1, rune_category2) # ok
        if participant_id in item['item_logs']:
            item_logs = item['item_logs'][participant_id]
            item_timestamp_logs = item['item_timestamp_logs'][participant_id]
            core_id_list, coretime_list = get_core_id_list(item_logs, item_timestamp_logs)
            if len(core_id_list) > 0:
                #update_item_core_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(core_id_list)) # ok
                update_item_core_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list, coretime_list) # ok
            shoes_id = get_shoes_id(item_logs, item_timestamp_logs)
            if shoes_id is not None:
                update_item_shoes_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, shoes_id) # ok
            starting_id_list = get_starting_id_list(item_logs, item_timestamp_logs)
            if len(starting_id_list) > 0:
                update_item_starting_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(starting_id_list))
            item_timeline = get_item_timeline(item_logs, item_timestamp_logs)

        if participant_id in item['skill_logs']:
            skill_logs = item['skill_logs'][participant_id]
            skill_list = get_skill_list(skill_logs)
            update_skill_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, str(skill_list))
            if len(core_id_list) > 0 and shoes_id is not None and len(starting_id_list) > 0:
                first_skill_master_id = get_first_master_skill_id(skill_list)
                if first_skill_master_id is not None:
                    update_joint_build_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, core_id_list[0], runes[0], first_skill_master_id, shoes_id, str(starting_id_list)) # ok

        spell1_id, spell2_id = sorted(participant['spells'])
        update_spell_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, spell1_id, spell2_id)
        statperk1_id, statperk2_id, statperk3_id = get_statperk_id_list(participant['stats'])
        update_statperk_lose_count(sql, region_id, version_id, champion_id, lane_id, tier_id, statperk1_id, statperk2_id, statperk3_id)
        tmp_lane_pairs[lane_id].append((champion_id, str(core_id_list), str(starting_id_list), shoes_id, (runes, rune_category1, rune_category2), str(skill_list), (spell1_id, spell2_id), (statperk1_id, statperk2_id, statperk3_id)))
        timeline_pita, timeline_count, timeline_origin = get_lose_timeline_items(lose_team_array, period_array, max_len)
        update_timeline_count(sql, region_id, version_id, champion_id, lane_id, tier_id, timeline_pita, timeline_count, timeline_origin)

        if summoner_id in pro_summoner_id_dict and participant_id in item['item_logs'] and participant_id in item['skill_logs']:
            logger.info('pro data')
            nickname, team_name, summoner_name, pro_team_id, pro_or_academy = pro_summoner_id_dict[summoner_id]
            if pro_or_academy == 'pro':
                is_pro = 1
                is_academy = 0
                is_craft = 0
            elif pro_or_academy == 'academy':
                is_pro = 0
                is_academy = 1
                is_craft = 0
            result = 0 # lose
            summoner_name = participant['summonerName']
            opponent_champion_id = get_champion_id(item, lane_pred, result='lose' if result==1 else 'win', target_lane_id=lane_id)
            level = participant['stats']['champLevel']
            kills = participant['stats']['kills']
            deaths = participant['stats']['deaths']
            assists = participant['stats']['assists']
            gold = participant['stats']['goldEarned']
            damage_dealt = participant['stats']['totalDamageDealt']
            damage_taken = participant['stats']['totalDamageTaken']
            final_item_id_list = str(list(participant['items']))
            cs_num = participant['stats']['totalMinionsKilled']
            main_rune_id_list = str(runes[:4])
            sub_rune_id_list = str(runes[4:])
            statperk_id_list = str([statperk1_id, statperk2_id, statperk3_id])
            spell1_id, spell2_id = participant['spells'] # not sorted
            tier_name, rank_name = await summoner_dto.get_tier_name_from_summoner_summary(mds, summoner_id)
            real_tier_id = tier_dict[tier_name]
            update_probuild(sql, region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                 level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                 str(core_id_list), str(starting_id_list), item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                 statperk_id_list, str(get_skill_name(skill_list)), cs_num, str(team_champion_id_list[::-1]).replace("'", '"'), is_pro, is_craft, pro_team_id, real_tier_id, is_academy, nickname)

        elif summoner_id in craft_dict[(champion_id, lane_id)] and participant_id in item['item_logs'] and participant_id in item['skill_logs']:
            result = 0 # lose
            summoner_name = participant['summonerName']
            opponent_champion_id = get_champion_id(item, lane_pred, result='lose' if result==1 else 'win', target_lane_id=lane_id)
            level = participant['stats']['champLevel']
            kills = participant['stats']['kills']
            deaths = participant['stats']['deaths']
            assists = participant['stats']['assists']
            gold = participant['stats']['goldEarned']
            damage_dealt = participant['stats']['totalDamageDealt']
            damage_taken = participant['stats']['totalDamageTaken']
            final_item_id_list = str(list(participant['items']))
            cs_num = participant['stats']['totalMinionsKilled']
            main_rune_id_list = str(runes[:4])
            sub_rune_id_list = str(runes[4:])
            statperk_id_list = str([statperk1_id, statperk2_id, statperk3_id])
            spell1_id, spell2_id = participant['spells'] # not sorted
            is_pro = 0
            is_craft = 1
            is_academy = 0
            pro_team_id = -1
            tier_name, rank_name = await summoner_dto.get_tier_name_from_summoner_summary(mds, summoner_id)
            real_tier_id = tier_dict[tier_name]
            if tier_name in ['MASTER', 'GRANDMASTER', 'CHALLENGER']:
                nickname = tier_name
            else:
                nickname = tier_name +' '+ rank_name
            update_probuild(sql, region_id, version_id, champion_id, lane_id, game_id, result, summoner_name, opponent_champion_id,
                 level, kills, deaths, assists, gold, damage_dealt, damage_taken, spell1_id, spell2_id, final_item_id_list,
                 str(core_id_list), str(starting_id_list), item_timeline, timestamp, duration, main_rune_id_list, sub_rune_id_list,
                 statperk_id_list, str(get_skill_name(skill_list)), cs_num, str(team_champion_id_list[::-1]).replace("'", '"'), is_pro, is_craft, pro_team_id, real_tier_id, is_academy, nickname)



    for lane_id, pair in tmp_lane_pairs.items():
        (champion1_id, core_id_list1, starting_id_list1, shoes_id1, (runes1, rune_category11, rune_category21), skill_list1, (spell1_id1, spell2_id1), (statperk1_id1, statperk2_id1, statperk3_id1)) = pair[0]
        (champion2_id, core_id_list2, starting_id_list2, shoes_id2, (runes2, rune_category12, rune_category22), skill_list2, (spell1_id2, spell2_id2), (statperk1_id2, statperk2_id2, statperk3_id2)) = pair[1]

        update_counter_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id) # ok
        update_counter_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id) # ok
        if len(core_id_list1) > 0:
            update_counter_item_core_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, str(core_id_list1))
        if len(core_id_list2) > 0:
            update_counter_item_core_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, str(core_id_list2))
        if len(starting_id_list1) > 0:
            update_counter_item_starting_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, starting_id_list1)
        if len(starting_id_list2) > 0:
            update_counter_item_starting_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, starting_id_list2)
        if shoes_id1 is not None:
            update_counter_item_shoes_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, shoes_id1)
        if shoes_id2 is not None:
            update_counter_item_shoes_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, shoes_id2)
        update_counter_rune_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, runes1[0], runes1[1], runes1[2], runes1[3], runes1[4], runes1[5], rune_category11, rune_category21)
        update_counter_rune_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, runes2[0], runes2[1], runes2[2], runes2[3], runes2[4], runes2[5], rune_category12, rune_category22)
        if len(skill_list1) > 0:
            update_counter_skill_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, skill_list1)
        if len(skill_list2) > 0:
            update_counter_skill_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, skill_list2)
        update_counter_spell_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, spell1_id1, spell2_id1)
        update_counter_spell_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, spell1_id2, spell2_id2)
        update_counter_statperk_win_count(sql, region_id, version_id, champion1_id, champion2_id, lane_id, tier_id, statperk1_id1, statperk2_id1, statperk3_id1)
        update_counter_statperk_lose_count(sql, region_id, version_id, champion2_id, champion1_id, lane_id, tier_id, statperk1_id2, statperk2_id2, statperk3_id2)

    update_duo_count(sql, region_id, version_id, tier_id, tmp_lane_pairs)

    tmp_ban = list()
    for ban in item['teams']['win']['bans']:
        if ban == -1:
            continue
        else:
            tmp_ban.append(ban)

    for ban in item['teams']['lose']['bans']:
        if ban == -1:
            continue
        else:
            tmp_ban.append(ban)

    for ban_id in set(tmp_ban):
        update_lane_date_ban_count(sql, region_id, version_id, ban_id, date_id, tier_id)


def get_craft_dict(version_id):
    sql.set_cursor()
    sql.query('SELECT champion_id, lane_id, summoner_id FROM craftsman_info where version_id=%d'%version_id)
    items = sql.cur.fetchall()
    ret_dict = defaultdict(list)
    for champion_id, lane_id, summoner_id in items:
        ret_dict[(champion_id, lane_id)].append(summoner_id)
    return ret_dict


def get_pro_dict():
    sql.set_cursor()
    sql.query('SELECT summoner_id, nickname, team_name, summoner_name, pro_team_id, pro_or_academy FROM progamer_info')
    items = sql.cur.fetchall()
    ret_dict = dict()
    for summoner_id, nickname, team_name, summoner_name, pro_team_id, pro_or_academy in items:
        ret_dict[summoner_id] = nickname, team_name, summoner_name, pro_team_id, pro_or_academy
    return ret_dict


sql = PSQLHandler(db_name='lolps_sync', connect=False)
sql.connect()
version_dict = get_info_dict_from_sql('version_info')
region_dict = get_info_dict_from_sql('region_info')
lane_dict = get_info_dict_from_sql('lane_info', key_index=2)
tier_dict = get_info_dict_from_sql('tier_info')
date_dict = get_info_dict_from_sql('date_info')

core_item_id_list = get_core_item_id_list()
shoes_id_list = get_shoes_item_id_list()
craft_dict = get_craft_dict(list(version_dict.values())[-1])
pro_summoner_id_dict = get_pro_dict()


#with open('loldata/stacker/pro_summoner_id_dict.pkl', 'rb') as f:
#    pro_summoner_id_dict = pickle.load(f)
#champion_dict = get_info_dict_from_sql('champion_info')

