import logging
import logging.config
import time
import numpy as np
import asyncio
import re

from .sync_count import *

from ..db_handler.async_mongodb_handler import AsyncMongoDbHandler
from ..db_handler.psql_handler import PSQLHandler
from ..algorithm.lane_predictor import predict_lane
from ..riotapi.riot_watcher_wrapper_async import league_entries, masters_by_queue, close_session
from ..riotapi.riot_watcher_wrapper_async import challenger_by_queue, grandmaster_by_queue
from ..riotapi.riot_watcher_wrapper_async import summoner_by_id, matchlist_by_account, matchlist_by_puuid, match_by_id_v5
from ..dto.summoner_dto import SummonerDTO
from ..dto.match_dto import MatchDTO as MatchDTO4
from ..dto.match_v5_dto import MatchDTO as MatchDTO5
from ..dto.match_summary_dto import MatchSummaryDTO
from ..error import InvalidMatchDataError
from ..config import RiotConfig, LogConfig

log_config = LogConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("stacker")
config = RiotConfig()

my_region = config.my_region
my_region_v5 = 'asia' if config.my_region == 'kr' else ''
MAXLEN = 1000
SLEEP = 0.1
SOLO_RANKED_GAME_5x5_queueid = 420
SOLO_RANKED_GAME_TYPE = 'ranked'
MATCH_ID_RE = re.compile('[A-Z]+_[0-9]+')

target_version = '11.18'

match_dto = MatchDTO4(my_region, target_version)
match_dto_v5 = MatchDTO5('asia', target_version)
match_summary_dto = MatchSummaryDTO()
summoner_dto = SummonerDTO()

db_name = 'riot'
collection_name = 'game_data_v11.18'
collection_name_doc_game = 'game_summary'
collection_name_doc_summoner = 'summoner_summary'


def get_threshold_of_tier_correction(tier):
    th = 10
    if tier in ['IRON', 'BRONZE', 'SILVER', 'GOLD']:
        th = 10
    elif tier in ['PLATINUM']:
        th = 10
    elif tier in ['DIAMOND']:
        th = 10
    elif tier in ['MASTER', 'GRANDMASTER', 'CHALLENGER']:
        th = 10
    return th


async def get_high_elo_summoner_list(fn, tier, my_region, queue):
    summoner_queue = []
    try:
        if np.random.randint(10) == 0:
            summoner_list = (await fn(my_region, queue))['entries']
            for summoner_item in summoner_list:
                summoner_item['tier'] = tier
                summoner_queue.append((summoner_item['summonerId'], summoner_item))
    except Exception as e:
        logger.error(e)
        pass

    except KeyboardInterrupt as e:
        logger.error(f'keyboard_interrupt : {e}')
        raise
    finally:
        return summoner_queue


async def get_low_elo_summoner_list(my_region, tier, division):
    summoner_queue = []
    try:
        summoner_list = (await league_entries(my_region, tier, division))
        for summoner_item in summoner_list:
            summoner_item['tier'] = tier
            summoner_queue.append((summoner_item['summonerId'], summoner_item))
    except Exception as e:
        logger.error(e)
        pass

    except KeyboardInterrupt as e:
        logger.error(f'keyboard_interrupt : {e}')
        raise
    finally:
        return summoner_queue


async def get_summoner_queue(queue):

    if np.random.randint(10) == 0:
        coroutines = [
            get_high_elo_summoner_list(masters_by_queue, 'MASTER', my_region, queue),
            get_high_elo_summoner_list(challenger_by_queue, 'CHALLENGER', my_region, queue),
            get_high_elo_summoner_list(grandmaster_by_queue, 'GRANDMASTER', my_region, queue),
        ]
    else:
        coroutines = []

    tiers = {'DIAMOND', 'PLATINUM', 'GOLD', 'SILVER', 'BRONZE', 'IRON'}
    divisions = {'I', 'II', 'III', 'IV'}

    for tier in tiers:
        for division in divisions:
            if np.random.randint(3) == 0:
                coroutines.append(get_low_elo_summoner_list(my_region, tier, division))

    result = await asyncio.gather(*coroutines)
    return result


def get_now_timestamp():
    return int(time.time() * 1000)


def prune_match_list(match_list):
    pruned_match_list = []
    SOLO_RANKED_GAME_5x5_queueid = 420
    ONE_DAY_TIME = 1000 * 60 * 60 * 24
    TIME_THRESHOLD = ONE_DAY_TIME * 5
    for m in match_list:
        if m['queue'] == SOLO_RANKED_GAME_5x5_queueid:
            time_diff = get_now_timestamp() - m['timestamp']
            if time_diff < TIME_THRESHOLD:
                pruned_match_list.append(m)
    return pruned_match_list


def has_summoner_info(summoner_item):
    if 'wins' in summoner_item:
        return True
    else:
        return False


async def collect_game_data(mdg, docg, docs, sql, summoner_queue_data, target_version, depth=0):
    summoner_id, summoner_item = summoner_queue_data
    tier = summoner_item['tier']
    summoner_name = summoner_item['summonerName']
    if has_summoner_info(summoner_item):
        last_called_by_stacker = await summoner_dto.update_summoner_tier_info(docs, summoner_id, summoner_item)
        if last_called_by_stacker is not None:
            time_diff = (time.time() - last_called_by_stacker // 1000)
            if time_diff < 60 * 20:  # 20 min
                logger.info(f'time_diff : {int(time_diff)}')
                logger.info(f'the info of the summoner {summoner_name} was updated within the last 20 minutes.')
                return

    # seed = np.random.randint(10000)
    # await asyncio.sleep(seed / 1000)
    try:
        tmp_summoner_data = await docs.find_one_item({'summoner_id': summoner_id})
        if tmp_summoner_data is not None and 'puuid' in tmp_summoner_data:
            puuid = tmp_summoner_data['puuid']
        else:
            puuid = (await summoner_by_id(my_region, summoner_id))['puuid']
            if tmp_summoner_data is not None:
                set_item = {'puuid': puuid}
                await docs.update_item({'summoner_id': summoner_id}, {'$set': set_item})
        match_list = (await matchlist_by_puuid(my_region_v5, puuid, queue=SOLO_RANKED_GAME_5x5_queueid, type=SOLO_RANKED_GAME_TYPE, begin_time=int(get_now_timestamp() / 1000 - 1 * 15 * 60 * 60)))
    except Exception as e:
        logger.error(e)
        return

    coroutines = []
    for match_id in match_list:
        if MATCH_ID_RE.match(match_id):
            coroutines.append(insert_gamedata_to_db(mdg, docg, docs, sql, match_id, tier, target_version, depth=depth))
    await asyncio.gather(*coroutines)


async def insert_gamedata_to_db(mdg, docg, docs, sql, match_id, tier, target_version, depth=0):
    try:
        game_id = int(match_id.split('_')[1])  # KR_5441725643 -> 5441725643
        if await mdg.find_one_item({'game_id': game_id}) is None:
            logger.info(f'없음 : {game_id}')
            game_data = await match_dto_v5(match_id)
            game_data['tier'] = tier

            # insert to db
            if await mdg.find_one_item({'game_id': game_id}) is None:  # check once more because of the asynchronous behaviors
                await mdg.insert_item(game_data)
                lane_pred = predict_lane(game_data)
                logger.info(f'game_id : {game_id:d}, depth : {depth:d}')
                if lane_pred is not None and game_data['teams']['win']['participants'][0]['stats']['gameEndedInEarlySurrender'] == False:
                    await update_tables(sql, docs, game_data, lane_pred)
                    summary = match_summary_dto(game_data, lane_pred)
                    await docg.insert_item(summary)

                    coroutines = []
                    for sid, svalue in summary['participants'].items():
                        coroutines.append(collect_game_data_for_each_participant(mdg, docg, docs, summary, sid, svalue, depth))

                    await asyncio.gather(*coroutines)

    except InvalidMatchDataError as e:
        logger.error(e)
        return


async def collect_game_data_for_each_participant(mdg, docg, docs, summary, sid, svalue, depth):
    await summoner_dto.update_match_info_of_summoner_summary(docs, sid, summary)
    if depth <= 2:
        summoner_name = svalue['summoner_name']
        new_summoner_item = await docs.find_one_item(condition={'summoner_id': sid}, projection={'tier': 1})
        if new_summoner_item is not None:
            tier = new_summoner_item['tier']
            if tier != '':
                summoner_queue_data = (sid, {'tier': tier, 'summonerName': summoner_name})
                await collect_game_data(mdg, docg, docs, sql, summoner_queue_data, target_version, depth=depth + 1)


async def main(summoner_queue):

    mdg = AsyncMongoDbHandler(db_name=db_name, collection_name=collection_name)
    docg = AsyncMongoDbHandler(db_name=db_name, collection_name=collection_name_doc_game)
    docs = AsyncMongoDbHandler(db_name=db_name, collection_name=collection_name_doc_summoner)
    sql = PSQLHandler(db_name='lolps_sync', connect=False)
    sql.connect()

    coroutines = []
    for summoner_queue_data in summoner_queue:
        coroutines.append(collect_game_data(mdg, docg, docs, sql, summoner_queue_data, target_version))
    await asyncio.gather(*coroutines)


if __name__ == '__main__':

    queue = 'RANKED_SOLO_5x5'
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(get_summoner_queue(queue))
    summoner_queue = []
    for summoner_list in result:
        for summoner_info in summoner_list:
            summoner_queue.append(summoner_info)

    np.random.shuffle(summoner_queue)
    logger.info('len of summoner_queue : %d' % (len(summoner_queue)))
    loop.run_until_complete(main(summoner_queue))
    logger.info('finish')
    loop.run_until_complete(close_session())
