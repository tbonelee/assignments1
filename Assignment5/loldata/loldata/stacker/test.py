import logging
import logging.config
import time
import asyncio
import numpy as np

from ..db_handler.mongodb_handler import MongoDbHandler
from ..riotapi.riot_watcher_wrapper_async import summoner_by_account, summoner_by_id, close_session, summoner_by_name
from ..error import InvalidSummonerDataError
from ..utils import convert_to_dict_mongodb
from ..config import LogConfig, RiotConfig

log_config = LogConfig()
riot_config = RiotConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("puuid_updater")
db_name = 'riot'
collection_name_doc_summoner = 'summoner_summary'
my_region = riot_config.my_region


async def send_request():
    # for i in range(1000):
    summoner_id = 'TQx8k739Xc0Rhmkjwyhtg9T61KPMA-pvIW1HpK-uNSQlHg'
    summoner_item = (await summoner_by_id(my_region, summoner_id))
    print(summoner_item)
    # account_id = 'zbkWhQeeduBjBOjVlY334wpa1iZisAoX5aKXxaKQM1UG'
    # summoner_item = (await summoner_by_account(my_region, account_id))
    # print(summoner_item)

    # summoner_name = 'Hide on bush'
    # summoner_item = (await summoner_by_name(my_region, summoner_name))
    # print(summoner_item)

    # if 'puuid' in summoner_item:
    # 	puuid = summoner_item['puuid']
    # 	summoner_name = summoner_item['name']
    # 	profile_icon_id = summoner_item['profileIconId']
    # 	summoner_level = summoner_item['summonerLevel']
    # 	logger.info(f'{account_id}')
    # else:
    # 	raise InvalidSummonerDataError('puuid is not in summoner_item')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(send_request())
    loop.run_until_complete(close_session())
