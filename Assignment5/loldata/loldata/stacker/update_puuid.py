import logging
import logging.config
import time
import asyncio
import numpy as np

from ..db_handler.mongodb_handler import MongoDbHandler
from ..riotapi.riot_watcher_wrapper_async import summoner_by_account, close_session
from ..error import InvalidSummonerDataError
from ..utils import convert_to_dict_mongodb
from ..config import LogConfig, RiotConfig

log_config = LogConfig()
riot_config = RiotConfig()
logging.config.dictConfig(log_config.config)
logger = logging.getLogger("puuid_updater")
db_name = 'riot'
collection_name_doc_summoner = 'summoner_summary'
my_region = riot_config.my_region


async def update_puuid(mds, account_id):
    summoner_item = (await summoner_by_account(my_region, account_id))
    if 'puuid' in summoner_item:
        puuid = summoner_item['puuid']
        summoner_name = summoner_item['name']
        profile_icon_id = summoner_item['profileIconId']
        summoner_level = summoner_item['summonerLevel']
        logger.info(f'{account_id}')
    else:
        raise InvalidSummonerDataError('puuid is not in summoner_item')

    set_item = {'puuid': puuid,
                'summoner_name': summoner_name,
                'profile_icon_id': profile_icon_id,
                'summoner_level': summoner_level}
    converted_dict = convert_to_dict_mongodb(set_item)
    mds.update_item({'account_id': account_id},
                    {'$set': converted_dict})
    logger.info(f'update puuid of {summoner_name} complete')


def get_random_tier():
    tier = np.random.choice(['GRANDMASTER', 'MASTER', 'DIAMOND', 'PLATINUM',
                             'GOLD', 'SILVER', 'BRONZE', 'IRON'])
    if tier in ['CHALLENGER', 'GRANDMASTER', 'MASTER']:
        rank = 'I'
    else:
        rank = np.random.choice(['I', 'II', 'III', 'IV'])
    return tier, rank


async def main():
    mds = MongoDbHandler(db_name, collection_name_doc_summoner)
    tier, rank = get_random_tier()
    items = mds.find_item(condition={'tier': tier, 'rank': rank},
                          projection={'account_id': 1, 'puuid': 1})
    for item in items:
        if 'puuid' in item:
            logger.info('pass')
            continue
        account_id = item['account_id']
        try:
            await asyncio.sleep(0.001)
            await update_puuid(mds, account_id)
        except InvalidSummonerDataError as e:
            logger.error(e)

if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.run_until_complete(close_session())
