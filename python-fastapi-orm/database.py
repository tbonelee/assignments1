
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import *
import databases

DATABASE_URL = 'mysql+pymysql://root:qwer1234@mysql:3306/match_info'

database = databases.Database(DATABASE_URL)

metadata_obj = MetaData()
engine = create_engine(
    DATABASE_URL, echo=True
)
champion_rate = Table('champion_rate', metadata_obj,
                      Column('id', Integer, primary_key=True),
                      Column('lane', VARCHAR, primary_key=True),
                      Column('pick rate', DECIMAL),
                      Column('ban rate', DECIMAL),
                      Column('win rate', DECIMAL),
                      autoload_with=engine)


# metadata_obj.create_all(engine)

# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
