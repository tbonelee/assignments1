from re import match
from riotwatcher import LolWatcher, ApiError
import json

# API 키를 넣어서 객체 생성
# lol_watcher = LolWatcher('RGAPI-d94501c2-8cfe-4c96-bd45-07e9c78e2e92', default_match_v5=True)
lol_watcher = LolWatcher('RGAPI-e31220dd-ee0b-43dd-889f-bf06313038a8')

# 소환사를 찾을 지역
regionList = ['', 'br1', 'eun1', 'euw1', 'jp1', 'kr', 'la1', 'la2', 'na1', 'oc1', 'ru', 'tr1']

# 지역 입력 받기
temp = input("""
해당하는 지역 번호를 입력하세요
(1) BR1 / (2) EUN1 / (3) EUW1 / (4) JP1 / (5) KR / (6) LA1 / (7) LA2 / (8) NA1 / (9) OC1 / (10) RU / (11) TR1
""")
# 숫자 아닌 입력 예외
if not temp.isdigit():
	print("not a valid region number")
	exit()

# 선택지에 없는 입력 예외
if int(temp) >= 1 and int(temp) <= 11:
	my_region = regionList[int(temp)]
else:
	print("not a valid region number")
	exit()

# 소환사명 입력
summonerName = input("소환사명을 입력하세요 : ")

# 소환사 정보는 user에 딕셔너리로 반환
try:
	user = lol_watcher.summoner.by_name(my_region, summonerName)
except ApiError as e:
	if e.response.status_code == 429:
		print('We should retry in {} seconds.'.format(e.headers['Retry-After']))
		print('this retry-after is handled by default by the RiotWatcher library')
		print('future requests wait until the retry-after time passes')
		exit()
	elif e.response.status_code == 404:
		print("Summoner name not found")
		exit()
	else:
		print("")
		# exit()

		raise
print(user)
print("=" * 20)
# 해당 소환사의 가장 최근 경기를 최대 20 개까지 matchlist에 받아온다
matchList = lol_watcher.match_v4.matchlist_by_account(my_region, user['accountId'], begin_index=0, end_index=20)
print(matchList)
# gameId만 gameIdList에 리스트로 할당
matchList = matchList['matches']
gameIdList = []
for _ in matchList:
	gameIdList.append(_['gameId'])

# 결과 출력
print("=" * 20)
print("Number of matches : " + str(len(gameIdList)))
print("gameId list =", gameIdList)
